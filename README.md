# SatNOGS COMMS Software MCU

SatNOGS-COMMS is an open-source project that aims to develop a versatile telecommunications subsystem suitable for nano-satellites and Cubesats.
It is designed to be flexible and adaptable, allowing for a wide range of missions.

![SatNOGS-COMMS](docs/assets/pcb-top.png)![SatNOGS-COMMS](docs/assets/pcb-buttom.png)

The firmware is based on the Zephyr-RTOS.
However, SatNOGS-COMMS architecture follows a modular approach.
All hardware control is implemented by the abstract platform-agnostic library [libsatnogs-comms](https://gitlab.com/librespacefoundation/satnogs-comms/libsatnogs-comms) allowing users to implement custom firmware on the RTOS of choice, quite easily.

## Resources

### Getting Started

* [Firmware documentation](https://librespacefoundation.gitlab.io/satnogs-comms/satnogs-comms-software-mcu)
* [Building and flashing the firmware](https://librespacefoundation.gitlab.io/satnogs-comms/satnogs-comms-software-mcu/group__build)

### Development

* [Development guide](https://librespacefoundation.gitlab.io/satnogs-comms/satnogs-comms-software-mcu/group__development.html)
* [Configuration & Customization](https://librespacefoundation.gitlab.io/satnogs-comms/satnogs-comms-software-mcu/group__customization.html)

## Flight Heritage & Missions

* Curium-1 on Ariane 6 maiden flight (July 2024)
* PHASMA (Q3 2025)
* HELEO (Q1 2026)

## Website and Contact

For more information about the project and Libre Space Foundation please visit our [site](https://libre.space/)
and our [community forums](https://community.libre.space).
You can also chat with the SatNOGS-COMMS development team at
[#satnogs-comms:matrix.org](https://riot.im/app/#/room/#satnogs-comms:matrix.org)

## License

![Libre Space Foundation](docs/assets/LSF_HD_Horizontal_Color1-300x66.png)
&copy; 2019-2020 [Libre Space Foundation](https://libre.space).
