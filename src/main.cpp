/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2022-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "mission.hpp"
#include "startup.hpp"
#include <satnogs-comms/board.hpp>
#include <zephyr/kernel.h>

/**
 * @brief The main sequence to initialize the Satnogs-COMMS board
 *
 * @mermaid{main_sequence}
 */
int
main(void)
{

  auto &startup = startup::get_instance();
  auto &err     = error_handler::get_instance();

  startup.prepare();
  startup.start();

  auto &leds = satnogs::comms::board::get_instance().leds();

  while (1) {
    try {
      task_wdt_feed(startup.wdg_id());
      k_msleep(500);
      leds.toggle(sc::leds::led::led0);
      leds.toggle(sc::leds::led::led1);
    } catch (const sc::exception &e) {
      err.handle(e);
    }
    /*
     * Exceptions that are not of the base type satnogs::comms::exception are
     * handled directly as critical
     */
    catch (std::exception &e) {
      err.handle(e);
    }
  }
  return 0;
}
