#include "startup.hpp"

namespace sc = satnogs::comms;

#if DT_HAS_COMPAT_STATUS_OKAY(st_stm32_watchdog)
#define WDT_NODE DT_COMPAT_GET_ANY_STATUS_OKAY(st_stm32_watchdog)
#else
#error "IWDG is not available. Check the device tree configuration"
#endif

const struct device *v_bat_dev = DEVICE_DT_GET(DT_ALIAS(vbat));

const struct gpio_dt_spec led0 = GPIO_DT_SPEC_GET(DT_ALIAS(led0), gpios);
const struct gpio_dt_spec led1 = GPIO_DT_SPEC_GET(DT_ALIAS(led1), gpios);

const struct spi_config radio_spi_cfg = {
    .frequency = 5000000,
    .operation = SPI_OP_MODE_MASTER | SPI_TRANSFER_MSB | SPI_WORD_SET(8) |
                 SPI_LINES_SINGLE | SPI_FULL_DUPLEX | SPI_FRAME_FORMAT_MOTOROLA,
    .cs = {.gpio = {.port = nullptr, .pin = 0, .dt_flags = 0}, .delay = 0}};
const struct device      *radio_spi_dev = DEVICE_DT_GET(DT_NODELABEL(spi4));
const struct gpio_dt_spec radio_cs =
    GPIO_DT_SPEC_GET(DT_NODELABEL(radio_cs), gpios);

const struct gpio_dt_spec radio_rst =
    GPIO_DT_SPEC_GET(DT_NODELABEL(radio_rst), gpios);

const struct spi_config fpga_spi_cfg = {
    .frequency = 5000000,
    .operation = SPI_OP_MODE_MASTER | SPI_TRANSFER_MSB | SPI_WORD_SET(8) |
                 SPI_LINES_SINGLE | SPI_FULL_DUPLEX | SPI_FRAME_FORMAT_MOTOROLA,
    .cs = {.gpio  = SPI_CS_GPIOS_DT_SPEC_GET(DT_NODELABEL(fpga_spi)),
           .delay = 4}};
const struct device *fpga_spi_dev = DEVICE_DT_GET(DT_NODELABEL(spi2));

const struct gpio_dt_spec fpga_done =
    GPIO_DT_SPEC_GET(DT_NODELABEL(fpga_done), gpios);

const struct gpio_dt_spec p5v_rf_en =
    GPIO_DT_SPEC_GET(DT_NODELABEL(p5v_rf_en), gpios);
const struct gpio_dt_spec p5v_fpga_en =
    GPIO_DT_SPEC_GET(DT_NODELABEL(p5v_fpga_en), gpios);
const struct device *sens_i2c = DEVICE_DT_GET(DT_ALIAS(sensorsi2c));

const struct gpio_dt_spec can1_en =
    GPIO_DT_SPEC_GET(DT_NODELABEL(can1_en), gpios);
const struct gpio_dt_spec can1_low_pwr =
    GPIO_DT_SPEC_GET(DT_NODELABEL(can1_low_pwr), gpios);
const struct gpio_dt_spec can2_en =
    GPIO_DT_SPEC_GET(DT_NODELABEL(can2_en), gpios);
const struct gpio_dt_spec can2_low_pwr =
    GPIO_DT_SPEC_GET(DT_NODELABEL(can2_low_pwr), gpios);

const struct gpio_dt_spec p5v_rf_pg =
    GPIO_DT_SPEC_GET(DT_NODELABEL(p5v_rf_pg), gpios);
const struct gpio_dt_spec p5v_fpga_pg =
    GPIO_DT_SPEC_GET(DT_NODELABEL(p5v_fpga_pg), gpios);

#if DT_NODE_EXISTS(DT_NODELABEL(p3v3_rf_pg))
const struct gpio_dt_spec p3v3_rf_pg =
    GPIO_DT_SPEC_GET(DT_NODELABEL(p3v3_rf_pg), gpios);
#else
const struct gpio_dt_spec p3v3_rf_pg = {.port = nullptr, 0, 0};
#endif

const struct gpio_dt_spec en_uhf =
    GPIO_DT_SPEC_GET(DT_NODELABEL(en_uhf), gpios);
const struct gpio_dt_spec uhf_pgood =
    GPIO_DT_SPEC_GET(DT_NODELABEL(uhf_pgood), gpios);
const struct gpio_dt_spec en_sband =
    GPIO_DT_SPEC_GET(DT_NODELABEL(en_sband), gpios);
const struct gpio_dt_spec sband_pgood =
    GPIO_DT_SPEC_GET(DT_NODELABEL(sband_pgood), gpios);

const struct gpio_dt_spec en_agc_uhf =
    GPIO_DT_SPEC_GET(DT_NODELABEL(en_agc_uhf), gpios);
const struct gpio_dt_spec flt_sel_uhf =
    GPIO_DT_SPEC_GET(DT_NODELABEL(flt_sel_uhf), gpios);
const struct gpio_dt_spec alert_uhf_pa_temp =
    GPIO_DT_SPEC_GET(DT_NODELABEL(alert_uhf_pa_temp), gpios);

const struct gpio_dt_spec mixer_rst =
    GPIO_DT_SPEC_GET(DT_NODELABEL(mixer_rst), gpios);
const struct gpio_dt_spec mixer_enx =
    GPIO_DT_SPEC_GET(DT_NODELABEL(mixer_enx), gpios);
const struct gpio_dt_spec mixer_scl =
    GPIO_DT_SPEC_GET(DT_NODELABEL(mixer_scl), gpios);
const struct gpio_dt_spec mixer_sda =
    GPIO_DT_SPEC_GET(DT_NODELABEL(mixer_sda), gpios);

const struct gpio_dt_spec en_agc_sband =
    GPIO_DT_SPEC_GET(DT_NODELABEL(en_agc_sband), gpios);
const struct gpio_dt_spec flt_sel_sband =
    GPIO_DT_SPEC_GET(DT_NODELABEL(flt_sel_sband), gpios);
const struct gpio_dt_spec alert_sband_pa_temp =
    GPIO_DT_SPEC_GET(DT_NODELABEL(alert_sband_pa_temp), gpios);

const struct gpio_dt_spec emmc_en =
    GPIO_DT_SPEC_GET(DT_NODELABEL(emmc_en), gpios);
const struct gpio_dt_spec emmc_sel =
    GPIO_DT_SPEC_GET(DT_NODELABEL(emmc_sel), gpios);
const struct gpio_dt_spec emmc_rst =
    GPIO_DT_SPEC_GET(DT_NODELABEL(emmc_rst), gpios);

static const struct adc_dt_spec adc_1_16 =
    ADC_DT_SPEC_GET_BY_NAME(DT_PATH(zephyr_user), imon_3v3_d);
static const struct adc_dt_spec adc_3_0 =
    ADC_DT_SPEC_GET_BY_NAME(DT_PATH(zephyr_user), imon_5v_rf);
static const struct adc_dt_spec adc_3_1 =
    ADC_DT_SPEC_GET_BY_NAME(DT_PATH(zephyr_user), imon_5v_fpga);

const struct device *dac1 = DEVICE_DT_GET(DT_NODELABEL(dac1));

K_THREAD_STACK_DEFINE(workqueue_thread_stack, CONFIG_MAIN_WORKQUEUE_STACK_SIZE);
struct k_work_q main_work_q;

const struct gpio_dt_spec radio_trx_irq =
    GPIO_DT_SPEC_GET(DT_NODELABEL(radio_trx_irq), gpios);

static void
radio_trx_irq_handler(struct k_work *item)
{
  sc::radio::trx_irq_handler();
}
struct k_work        radio_trx_irq_work;
struct gpio_callback radio_trx_irq_clbk_h;

static void
radio_trx_irq_callback(const struct device *dev, struct gpio_callback *cb,
                       uint32_t pins)
{
  k_work_submit(&radio_trx_irq_work);
}

/* Misc IO */
gpio_bsp l0(&led0);
gpio_bsp l1(&led1);

/* FPGA IO*/
spi_bsp  fpga_spi(fpga_spi_dev, fpga_spi_cfg);
gpio_bsp gpio_fpga_done(&fpga_done, false);

/* Power IO */
i2c_bsp  sensors_i2c(sens_i2c);
gpio_bsp gpio_p5v_rf(&p5v_rf_en);
gpio_bsp gpio_p5v_fpga(&p5v_fpga_en);
gpio_bsp gpio_can1_en(&can1_en);
gpio_bsp gpio_can1_low_pwr(&can1_low_pwr);
gpio_bsp gpio_can2_en(&can2_en);
gpio_bsp gpio_can2_low_pwr(&can2_low_pwr);
gpio_bsp gpio_rf_5v_pgood(&p5v_rf_pg, false);
gpio_bsp gpio_fpga_5v_pgood(&p5v_fpga_pg, false);
gpio_bsp gpio_en_uhf(&en_uhf);
gpio_bsp gpio_uhf_pgood(&uhf_pgood, false);
gpio_bsp gpio_en_sband(&en_sband);
gpio_bsp gpio_sband_pgood(&sband_pgood, false);

/* Radio IO */
gpio_bsp gpio_en_agc_uhf(&en_agc_uhf);
gpio_bsp gpio_flt_sel_uhf(&flt_sel_uhf);
gpio_bsp gpio_alert_uhf_pa_temp(&alert_uhf_pa_temp, false);
gpio_bsp gpio_en_agc_sband(&en_agc_sband);
gpio_bsp gpio_flt_sel_sband(&flt_sel_sband);
gpio_bsp gpio_alert_sband_pa_temp(&alert_sband_pa_temp, false);

spi_manual_cs_bsp radio_spi(radio_spi_dev, radio_spi_cfg, &radio_cs);
gpio_bsp          radio_nrst(&radio_rst);
gpio_bsp          gpio_mixer_rst(&mixer_rst);
gpio_bsp          gpio_mixer_enx(&mixer_enx);
gpio_bsp          gpio_mixer_scl(&mixer_scl);
gpio_bsp          gpio_mixer_sda(&mixer_sda);

dac_bsp agc_vout_uhf(dac1, sc::radio::UHF_AGC_VOUT_DAC_CH, 12);
dac_bsp agc_vout_sband(dac1, sc::radio::SBAND_AGC_VOUT_DAC_CH, 12);

/* EMMC IO */
gpio_bsp gpio_emmc_en(&emmc_en);
gpio_bsp gpio_emmc_sel(&emmc_sel);
gpio_bsp gpio_emmc_rst(&emmc_rst);

chrono_bsp chr;

static adc_bsp adc_imon_3v3_d(adc_1_16);
static adc_bsp adc_imon_5v_rf(adc_3_0);
static adc_bsp adc_imon_5v_fpga(adc_3_1);

sensor_bsp v_bat(v_bat_dev);

sc::power::r_lim adc_rlim(CONFIG_R_LIM_3V3_D, CONFIG_R_LIM_5V_RF,
                          CONFIG_R_LIM_5V_FPGA);

sc::power::io_conf pwr_cnf = {.mon_i2c       = sensors_i2c,
                              .rf_5v_en      = gpio_p5v_rf,
                              .fpga_5v_en    = gpio_p5v_fpga,
                              .can1_en       = gpio_can1_en,
                              .can1_low_pwr  = gpio_can1_low_pwr,
                              .can2_en       = gpio_can2_en,
                              .can2_low_pwr  = gpio_can2_low_pwr,
                              .rf_5v_pgood   = gpio_rf_5v_pgood,
                              .fpga_5v_pgood = gpio_fpga_5v_pgood,
                              .uhf_en        = gpio_en_uhf,
                              .uhf_pgood     = gpio_uhf_pgood,
                              .sband_en      = gpio_en_sband,
                              .sband_pgood   = gpio_sband_pgood,
                              .imon_3v3_d    = adc_imon_3v3_d,
                              .imon_5v_rf    = adc_imon_5v_rf,
                              .imon_fpga     = adc_imon_5v_fpga,
                              .v_mon         = v_bat,
                              .rlim          = adc_rlim,
                              .efuse_adc_current_gain =
                                  CONFIG_EFUSE_ADC_CURRENT_GAIN};

/* Handle the available antenna configurations */
auto uhf_antenna = []() -> auto {
/* The GPIO antenna overlay has been set */
#if DT_NODE_HAS_PROP(DT_ALIAS(uhf_antenna), deploy_gpios)
  const struct gpio_dt_spec deploy_gpio =
      GPIO_DT_SPEC_GET(DT_ALIAS(uhf_antenna), deploy_gpios);
  const struct gpio_dt_spec sense_gpio =
      GPIO_DT_SPEC_GET(DT_ALIAS(uhf_antenna), sense_gpios);

  static gpio_bsp deploy0(&deploy_gpio);
  static gpio_bsp sense0(&sense_gpio);

  auto ant = sc::antenna_gpio(etl::make_string_with_capacity<32>("uhf-gpio"),
                              {{.deploy = deploy0, .sense = sense0}});

  return ant;
/* The ISIS i2c antenna overlay has been set */
#elif DT_NODE_HAS_PROP(DT_ALIAS(uhf_antenna), i2c_dev)
// TODO
/*
 * If no antenna configuration have been set from the overlays, just create a
 * dummy antenna definition. Any operation on this dummy object should not
 * have any effect
 */
#else
  return sc::antenna(etl::make_string_with_capacity<32>("uhf-dummy"), 0);
#endif
}();

auto sband_antenna = []() -> auto {
/* The GPIO antenna overlay has been set */
#if DT_NODE_HAS_PROP(DT_ALIAS(sband_antenna), deploy_gpios)
  const struct gpio_dt_spec deploy_gpio =
      GPIO_DT_SPEC_GET(DT_ALIAS(sband_antenna), deploy_gpios);
  const struct gpio_dt_spec sense_gpio =
      GPIO_DT_SPEC_GET(DT_ALIAS(sband_antenna), sense_gpios);

  static gpio_bsp deploy0(&deploy_gpio);
  static gpio_bsp sense0(&sense_gpio);

  auto ant = sc::antenna_gpio(etl::make_string_with_capacity<32>("sband-gpio"),
                              {{.deploy = deploy0, .sense = sense0}});

  return ant;
/* The ISIS i2c antenna overlay has been set */
#elif DT_NODE_HAS_PROP(DT_ALIAS(sband_antenna), i2c_dev)
// TODO
/*
 * If no antenna configuration have been set from the overlays, just create a
 * dummy antenna definition. Any operation on this dummy object should not
 * have any effect
 */
#else
  return sc::antenna(etl::make_string_with_capacity<32>("sband-dummy"), 0);
#endif
}();

msgq<sc::radio::rx_msg, CONFIG_RADIO_RX_MSGQ_SIZE> rx_msgq;

sc::rf_frontend09::io_conf rffe09_io = {
    .en_agc = gpio_en_agc_uhf,
    /* We re-purpose the ENBL pin, which is controlled by the HW with the gain
       selection pin */
    .agc_vset = agc_vout_uhf,
    .flt_sel  = gpio_flt_sel_uhf};

sc::rf_frontend24::io_conf rffe24_io = {.en_agc    = gpio_en_agc_sband,
                                        .agc_vset  = agc_vout_sband,
                                        .flt_sel   = gpio_flt_sel_sband,
                                        .mixer_clk = gpio_mixer_scl,
                                        .mixer_rst = gpio_mixer_rst,
                                        .mixer_enx = gpio_mixer_enx,
                                        .mixer_sda = gpio_mixer_sda,
                                        .chrono    = chr};

sc::radio::io_conf radio_cnf = {.spi_ctrl  = radio_spi,
                                .nreset    = radio_nrst,
                                .chrono    = chr,
                                .rffe09_io = rffe09_io,
                                .rffe24_io = rffe24_io};

sc::board::io_conf board_io = {.emmc_en          = gpio_emmc_en,
                               .emmc_sel         = gpio_emmc_sel,
                               .emmc_rst         = gpio_emmc_rst,
                               .fpga_spi         = fpga_spi,
                               .led0             = l0,
                               .led1             = l1,
                               .pwr_io           = pwr_cnf,
                               .sensors_i2c      = sensors_i2c,
                               .alert_t_pa_uhf   = gpio_alert_uhf_pa_temp,
                               .alert_t_pa_sband = gpio_alert_sband_pa_temp,
                               .radio_io         = radio_cnf,
                               .chrono           = chr,
                               .fpga_done        = gpio_fpga_done,
                               .uhf_antenna      = uhf_antenna,
                               .sband_antenna    = sband_antenna};

// FIXME some of the settings here should be fetched from the settings subsystem
static sc::radio::rx_conf rx_conf_uhf = {
    .freq = 0.0f,
    .fsk = {.rate = AT86RF215_FSK_SRATE_50, .mod_idx = 1.0f, .excess_bw = 1.0f},
    .gain = {.gain0_mode = sc::rf_frontend::gain_mode::AUTO,
             .gain0      = {.tgt = -40.0f},
             .gain1_mode = sc::rf_frontend::gain_mode::AUTO,
             .gain1      = {.agc = {.enable = 1,
                                    .freeze = 0,
                                    .reset  = 0,
                                    .avgs   = AT86RF215_AVGS_8,
                                    .input  = 0,
                                    .gcw    = 23,
                                    .tgt    = AT86RF215_TGT_M21}}}};

static sc::radio::rx_conf rx_conf_sband = {
    .freq = 0.0f,
    .fsk  = {.rate      = AT86RF215_FSK_SRATE_400,
             .mod_idx   = 0.5f,
             .excess_bw = 1.0f},
    .gain = {.gain0_mode = sc::rf_frontend::gain_mode::AUTO,
             .gain0      = {.tgt = -40.0f},
             .gain1_mode = sc::rf_frontend::gain_mode::AUTO,
             .gain1      = {.agc = {.enable = 1,
                                    .freeze = 0,
                                    .reset  = 0,
                                    .avgs   = AT86RF215_AVGS_8,
                                    .input  = 0,
                                    .gcw    = 23,
                                    .tgt    = AT86RF215_TGT_M21}}}};

/**
 * @brief Upon creation of the object, the hardware watchdog is activated and
 * the Task Watchdog is initialized
 *
 */
startup::startup() : m_wdgid(0)
{
  /*
   * IMPORTANT: Before do anything enable the hardware watchdog!
   * Ideally, it should be already enabled by the bootloader!
   */
  const struct device *hw_wdt_dev = DEVICE_DT_GET_OR_NULL(WDT_NODE);
  task_wdt_init(hw_wdt_dev);
  m_wdgid = task_wdt_add(CONFIG_WATCHDOG_PERIOD_SYS, task_wdt_callback,
                         (void *)k_current_get());
  /*
   * task_wdt_add() does activates the IWDG internally. The IWDG is enabled at
   * the first call of task_wdt_feed(). So it is essential for the
   * task_wdt_feed() to be called before any code that may hang the MCU
   */
  task_wdt_feed(m_wdgid);
}

/**
 * @brief Prepares and initializes the transceiver
 *
 */
void
startup::prepare()
{

  /*
   * Initialize the main work queue that can accept workers. This a
   * good practice instead of executing directly stuff into the timer.
   *
   * Timers on Zephyr operate in interrupt context.
   */
  k_work_queue_start(&main_work_q, workqueue_thread_stack,
                     K_THREAD_STACK_SIZEOF(workqueue_thread_stack),
                     CONFIG_MAIN_WORKQUEUE_PRIO, NULL);

  /*
   * The board initialization access the settings storage. If we want to protect
   * this procedure for any possible exception, settings retrieval should be
   * performed after the watchdog and logging subsystem initialization
   */
  auto &settings = settings::get_instance();
  sc::board::params
      board_params =
          {
              .radio_params =
                  {.rffe09_params =
                       {.rx_range =
                            {settings.get<settings::param::UHF_RX_RANGE_MIN>(),
                             settings.get<settings::param::UHF_RX_RANGE_MAX>()},
                        .tx_range   = {settings.get<
                                         settings::param::UHF_TX_RANGE_MIN>(),
                                       settings.get<
                                         settings::param::UHF_TX_RANGE_MAX>()},
                        .agc0_range = {settings.get<
                                           settings::param::UHF_AGC0_RANGE_MIN>(),
                                       settings.get<
                                           settings::param::UHF_AGC0_RANGE_MAX>()},
                        .gain0_range =
                            {settings
                                 .get<settings::param::UHF_GAIN0_RANGE_MIN>(),
                             settings
                                 .get<settings::param::UHF_GAIN0_RANGE_MAX>()},
                        .agc0_calib = {settings
                                           .get<settings::
                                                    param::UHF_AGC0_CALIB_SLOPE>(),
                                       settings
                                           .get<settings::
                                                    param::UHF_AGC0_CALIB_INTRCPT>()},
                        .gain0_calib =
                            {settings
                                 .get<settings::param::UHF_GAIN0_CALIB_SLOPE>(),
                             settings
                                 .get<settings::param::UHF_GAIN0_CALIB_INTRCPT>()}},
                   .rffe24_params =
                       {.rx_range =
                            {settings
                                 .get<settings::param::SBAND_RX_RANGE_MIN>(),
                             settings
                                 .get<settings::param::SBAND_RX_RANGE_MAX>()},
                        .tx_range =
                            {settings
                                 .get<settings::param::SBAND_TX_RANGE_MIN>(),
                             settings
                                 .get<settings::param::SBAND_TX_RANGE_MAX>()},
                        .agc0_range =
                            {settings
                                 .get<settings::param::SBAND_AGC0_RANGE_MIN>(),
                             settings
                                 .get<settings::param::SBAND_AGC0_RANGE_MAX>()},
                        .gain0_range =
                            {settings
                                 .get<settings::param::SBAND_GAIN0_RANGE_MIN>(),
                             settings.get<
                                 settings::param::SBAND_GAIN0_RANGE_MAX>()},
                        .agc0_calib =
                            {settings.get<
                                 settings::param::SBAND_AGC0_CALIB_SLOPE>(),
                             settings.get<
                                 settings::param::SBAND_AGC0_CALIB_INTRCPT>()},
                        .gain0_calib =
                            {settings.get<
                                 settings::param::SBAND_GAIN0_CALIB_SLOPE>(),
                             settings.get<settings::param::
                                              SBAND_GAIN0_CALIB_INTRCPT>()}},
                   .uhf_len   = {.tx = CONFIG_UHF_TX_FRAME_LEN,
                                 .rx = CONFIG_UHF_RX_FRAME_LEN},
                   .sband_len = {.tx = CONFIG_SBAND_TX_FRAME_LEN,
                                 .rx = CONFIG_SBAND_RX_FRAME_LEN},
                   .tx_mixer_params =
                       {{{.freq = {2270e6f, 2290e6f}, .lo = 182e6f},
                         {.freq = {2200e6f, 2270e6f}, .lo = 216e6f}}},
                   .rx_mixer_params =
                       {{{.freq = {2025e6f, 2070e6f}, .lo = 390e6f},
                         {.freq = {2070e6f, 2110e6f}, .lo = 389e6f}}}},
              .fpga_hw = sc::fpga::hw::ALINX_AC7Z020,
              .rx_msgq = rx_msgq};

  auto &mission = mission::get_instance();
  mission.pre_init();
  sc::board::init(board_io, board_params);
  mission.post_init();
}

/**
 * @brief Starts the execution of all the subsystems and their coressponding
 * tasks
 *
 * @note This method internally calls the mission::start() as the last step
 */
void
startup::start()
{
  auto &board    = sc::board::get_instance();
  auto &settings = settings::get_instance();
  auto &radio    = board.radio();
  auto &fpga     = board.fpga();

  auto &msg_arb = msg_arbiter::get_instance();
  auto &io_ctrl = io::get_instance();
  auto &io_wdg  = io_wdg::get_instance();
  auto &log     = logger::get_instance();
  auto &mission = mission::get_instance();

  log.boot();

  radio.enable();
  radio.enable(sc::radio::interface::UHF, false);
  radio.enable(sc::radio::interface::SBAND, false);
  fpga.enable(false);

  task_wdt_feed(m_wdgid);

  /*
   * If this the first time the board is active, wait for the post deployment
   * time before doing anything
   */
  if (settings.deployed() == false) {
    log.log({logger::target::UART, logger::target::RTT},
            "First boot. Waiting the hold off period...");
    for (size_t i = 0; i < CONFIG_DEPLOY_HOLD_OFF_SECS; i++) {
      task_wdt_feed(m_wdgid);
      k_sleep(K_SECONDS(1));
    }
  } else {
    log.log({logger::target::UART, logger::target::RTT},
            "Already deployed. Continue...");
  }
  settings.incr_boot_cnt();

  /* Spawn the threads */
  msg_arb.start();
  io_ctrl.start();
  io_wdg.start();
  task_wdt_feed(m_wdgid);

  /* Now that the main work queue is up and the radio has been initialized
   * activate the IRQ */
  gpio_pin_configure_dt(&radio_trx_irq, GPIO_INPUT | radio_trx_irq.dt_flags);

  gpio_init_callback(&radio_trx_irq_clbk_h, radio_trx_irq_callback,
                     BIT(radio_trx_irq.pin));
  gpio_add_callback(radio_trx_irq.port, &radio_trx_irq_clbk_h);
  k_work_init(&radio_trx_irq_work, radio_trx_irq_handler);
  gpio_pin_interrupt_configure_dt(&radio_trx_irq, GPIO_INT_EDGE_TO_ACTIVE);

  // FIXME
  radio.uhf().set_filter(sc::rf_frontend::filter::NARROW);
  rx_conf_uhf.freq = settings.get<settings::param::UHF_RX_FREQ>();

  radio.rx_async(sc::radio::interface::UHF, rx_conf_uhf);

  radio.sband().set_filter(sc::rf_frontend::filter::NARROW);
  rx_conf_sband.freq = settings.get<settings::param::SBAND_RX_FREQ>();
  radio.rx_async(sc::radio::interface::SBAND, rx_conf_sband);

  mission.start();
  log.log({logger::target::UART, logger::target::RTT},
          "All threads started succesfully!");
}

/**
 * @brief Returns the Task Watchdog ID registered at the contructor
 *
 * @note The ID returned from this method can be used inside the calling thread.
 * There is no need to register for another one.
 *
 * @return int the Task Watchdog ID
 */
int
startup::wdg_id() const
{
  return m_wdgid;
}
