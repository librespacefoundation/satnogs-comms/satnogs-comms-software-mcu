/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "callbacks.hpp"
#include <zephyr/device.h>
#include <zephyr/drivers/watchdog.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
#include <zephyr/sys/reboot.h>
#include <zephyr/task_wdt/task_wdt.h>

/* Declare the file as part of the original module in main.cpp */
LOG_MODULE_DECLARE(satnogscomms);

void
task_wdt_callback(int channel_id, void *user_data)
{
  k_msleep(1000);
  LOG_ERR("Task watchdog channel %d callback, thread: %s\n", channel_id,
          k_thread_name_get((k_tid_t)user_data));
  k_msleep(1000);
  /*
   * If the issue could be resolved, call task_wdt_feed(channel_id) here
   * to continue operation.
   *
   * Otherwise we can perform some cleanup and reset the device.
   */
  LOG_ERR("Resetting device...\n");
  k_msleep(1000);
  sys_reboot(SYS_REBOOT_COLD);
}