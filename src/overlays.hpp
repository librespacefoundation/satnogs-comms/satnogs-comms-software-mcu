/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <zephyr/devicetree.h>

/**
 * @file overlays.hpp
 * @brief This file contains several helper macros that produce a compile-time
 * error through the preprocessor, when some of the IO criteria are violated
 * due to misuse of the available overlays.
 *
 * An example could be, the usage of a specific UART (e.g UART1) for both
 * logging and IO. A second example of a misuse, could be the usage of a UART
 * and an SPI interface sharing the same pins on the PC104 connector.
 *
 * Such checks seems that are not currently possible, through the Zephyr build
 * system. The downside of this approach is that there is not way to enforce the
 * checks below, rather than including this header file at the project.
 *
 */

#if DT_NODE_EXISTS(DT_ALIAS(uartlog3)) && DT_NODE_EXISTS(DT_ALIAS(pc104_usart3))
#error UART3 cannot be used for logging and as IO interface at the same time. \
       Consider selecting a proper overlay configuration
#endif

#if DT_NODE_EXISTS(DT_ALIAS(uartlog1)) && DT_NODE_EXISTS(DT_ALIAS(uartlog3))
#error Only one UART can be used as logging interface. \
       Consider selecting a proper overlay configuration.
#endif

#if DT_NODE_EXISTS(DT_ALIAS(uartlog1)) && DT_NODE_EXISTS(DT_ALIAS(uartlog4))
#error Only one UART can be used as logging interface. \
       Consider selecting a proper overlay configuration.
#endif

#if DT_NODE_EXISTS(DT_ALIAS(uartlog3)) && DT_NODE_EXISTS(DT_ALIAS(uartlog4))
#error Only one UART can be used as logging interface. \
       Consider selecting a proper overlay configuration.
#endif

#if DT_NODE_EXISTS(DT_ALIAS(uartlog4)) && DT_NODE_EXISTS(DT_ALIAS(uartgnss4))
#error UART4 can be user either for GNSS or for logging. \
       Consider selecting a proper overlay configuration.
#endif

#if DT_NODE_EXISTS(DT_ALIAS(uartlog3)) && DT_NODE_EXISTS(DT_ALIAS(uartgnss3))
#error UART3 can be user either for GNSS or for logging. \
       Consider selecting a proper overlay configuration.
#endif

#if DT_NODE_EXISTS(DT_ALIAS(uartlog1)) && DT_NODE_EXISTS(DT_ALIAS(uartgnss1))
#error UART1 can be user either for GNSS or for logging. \
       Consider selecting a proper overlay configuration.
#endif

#if DT_NODE_EXISTS(DT_ALIAS(uartlog1)) && DT_NODE_EXISTS(DT_ALIAS(uartgnss1))
#error UART1 can be user either for GNSS or for logging. \
       Consider selecting a proper overlay configuration.
#endif

#if DT_NODE_EXISTS(DT_ALIAS(spi3)) && DT_NODE_EXISTS(DT_ALIAS(uartlog3))
#error SPI3 cannot be used along with UART3. \
       Consider selecting another UART port for logging.
#endif

#if DT_NODE_EXISTS(DT_ALIAS(spi3)) && DT_NODE_EXISTS(DT_ALIAS(uartgnss3))
#error SPI3 cannot be used along with UART3. \
       Consider selecting another UART port for GNSS.
#endif

#if DT_NODE_EXISTS(DT_ALIAS(spi3)) && DT_NODE_EXISTS(DT_ALIAS(pc104_usart3))
#error SPI3 cannot be used along with UART3. \
       Consider selecting another UART port for IO interface.
#endif
