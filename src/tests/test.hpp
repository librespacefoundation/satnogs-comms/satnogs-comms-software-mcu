/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <satnogs-comms/board.hpp>
#include <zephyr/kernel.h>

class test
{
public:
  struct params_work_container
  {
    struct k_work work;
    int           test_id;
    uint32_t      param0;
    uint32_t      param1;
    uint32_t      param2;
  };

  static test &
  get_instance()
  {
    static test inst;
    return inst;
  }

  enum class test_id : int
  {
    TX_UHF_SIMPLE   = 0,
    TX_SBAND_SIMPLE = 1,
    EMMC_SIMPLE     = 2,
    EMC_ROUTINE     = 3
  };

  /* Singleton */
  test(test const &) = delete;

  void
  operator=(test const &) = delete;

  void
  exec_test(test_id id, std::initializer_list<uint32_t> params);

  void
  stop();

  bool
  running() const;

  void
  emc_routine();

  bool
  emmc_success() const;

private:
  test();

  static void
  exec(struct k_work *item);

  void
  uhf_tx_simple(uint32_t nframes, uint32_t delay_us, uint32_t len);

  void
  sband_tx_simple(uint32_t nframes, uint32_t delay_us, uint32_t len);

  void
  emmc_simple(uint32_t nbytes);

  void
  init_workq();

  void
       tx_simple(satnogs::comms::radio::interface iface, uint32_t nframes,
                 uint32_t delay_us, uint32_t len);
  bool m_emmc_success;
  bool m_running;
  bool m_stop;
  struct params_work_container m_test_internal_params;
};
