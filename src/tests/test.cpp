/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "test.hpp"
#include "logger.hpp"
#include "msg_arbiter.hpp"
#include "settings.hpp"
#include <zephyr/drivers/disk.h>
#include <zephyr/kernel.h>
#include <zephyr/random/random.h>
#include <zephyr/storage/disk_access.h>
#include <zephyr/task_wdt/task_wdt.h>

// FIXME: This has to be defined in a more elegant way
#define EMMC_VOLUME_NAME "SD"

namespace sc = satnogs::comms;

static uint8_t buffer[512];

#ifdef CONFIG_TEST_ENABLED
static uint8_t         read_buff[512];
static struct k_work_q asynq_workq;
K_THREAD_STACK_DEFINE(async_workq_stack, CONFIG_ASYNC_WORKQUEUE_STACK_SIZE);
#endif

void
test::init_workq()
{
#ifdef CONFIG_TEST_ENABLED
  const k_work_queue_config cfg = {.name = "asynq_workq", .no_yield = 0};
  k_work_queue_start(&asynq_workq, async_workq_stack,
                     K_THREAD_STACK_SIZEOF(async_workq_stack),
                     CONFIG_ASYNC_WORKQUEUE_PRIO, &cfg);

  k_work_init(&m_test_internal_params.work, &test::exec);
  return;
#else
  return;
#endif
}

void
test::exec_test(test_id id, std::initializer_list<uint32_t> params)
{
#ifdef CONFIG_TEST_ENABLED
  switch (id) {
  case test_id::TX_UHF_SIMPLE:
    m_test_internal_params.test_id = static_cast<int>(id);
    m_test_internal_params.param0  = params.begin()[0];
    m_test_internal_params.param1  = params.begin()[1];
    m_test_internal_params.param2  = params.begin()[2];
    break;
  case test_id::TX_SBAND_SIMPLE:
    m_test_internal_params.test_id = static_cast<int>(id);
    m_test_internal_params.param0  = params.begin()[0];
    m_test_internal_params.param1  = params.begin()[1];
    m_test_internal_params.param2  = params.begin()[2];
    break;
  case test_id::EMMC_SIMPLE:
    m_test_internal_params.test_id = static_cast<int>(id);
    m_test_internal_params.param0  = params.begin()[0];
    break;
  default:
    return;
  }
  k_work_submit_to_queue(&asynq_workq, &m_test_internal_params.work);
#endif
}

void
test::uhf_tx_simple(uint32_t nframes, uint32_t delay_us, uint32_t len)
{
  tx_simple(sc::radio::interface::UHF, nframes, delay_us, len);
}

void
test::sband_tx_simple(uint32_t nframes, uint32_t delay_us, uint32_t len)
{
  tx_simple(sc::radio::interface::SBAND, nframes, delay_us, len);
}

static auto radio_msg = msg_arbiter::msg();

void
test::tx_simple(sc::radio::interface iface, uint32_t nframes, uint32_t delay_us,
                uint32_t len)
{
  auto &arb = msg_arbiter::get_instance();

  switch (iface) {
  case sc::radio::interface::UHF:
    radio_msg.iface = msg_arbiter::subsys::RADIO_UHF;
    break;
  case sc::radio::interface::SBAND:
    radio_msg.iface = msg_arbiter::subsys::RADIO_SBAND;
    break;
  default:
    return;
  }

  len = etl::clamp<uint32_t>(len, 1, msg_arbiter::mtu);

  radio_msg.len = len;
  for (uint32_t i = 0; i < nframes && m_stop == false; i++) {
    for (size_t j = 0; j < len; j++) {
      radio_msg.data[j] ^= sys_rand32_get() * j;
    }

    arb.fwd(&radio_msg, radio_msg.iface, K_FOREVER);
    k_usleep(std::max(1U, delay_us));
  }
}

bool
test::emmc_success() const
{
  return m_emmc_success;
}

void
test::emmc_simple(uint32_t nbytes)
{
#ifdef CONFIG_TEST_ENABLED
  auto &emmc = sc::board::get_instance().emmc();
  emmc.enable(true);
  emmc.reset(true);
  k_msleep(10);
  emmc.reset(false);
  emmc.set_dir(sc::emmc::dir::MCU);

  int ret = disk_access_ioctl(EMMC_VOLUME_NAME, DISK_IOCTL_CTRL_INIT, NULL);
  uint32_t sec_size = 512;
  // for now we only accept multiples of `sec_size`
  if (nbytes % sec_size != 0) {
    return;
  }

  int num_of_sectors = nbytes / sec_size;

  int k = 0;
  for (uint32_t i = 0; i < sec_size; i++) {
    if (i % 4 == 0)
      k++;
    buffer[i] = k;
  }

  int i;
  for (i = 0; i < num_of_sectors; i++) {
    ret = disk_access_write(EMMC_VOLUME_NAME, buffer, i, 1);

    if (ret) {
      return;
    }
    ret = disk_access_read(EMMC_VOLUME_NAME, read_buff, i, 1);
    k_yield();
    if (ret) {
      return;
    }

    if (memcmp(read_buff, buffer, sec_size)) {
      return;
    }
    memset(read_buff, 0xff, sec_size);
  }
  m_emmc_success = true;
  emmc.reset(true);
  k_msleep(50);
  emmc.enable(false);
  return;
#endif
}

/**
 * @brief A simple routine for EMC testing
 *
 * @deprecated This is deprecated and will be removed in upcoming release
 *
 */
void
test::emc_routine()
{
  auto &emmc = sc::board::get_instance().emmc();
  auto  fpga = sc::board::get_instance().fpga();
  emmc.enable(true);
  fpga.enable();
  emmc.set_dir(sc::emmc::dir::MCU);
  emmc.reset(true);
  k_msleep(1);
  emmc.reset(false);
  while (1) {
    uhf_tx_simple(4, 100000, 512);
    k_sleep(K_SECONDS(30));
    sband_tx_simple(10, 100000, 512);
    k_sleep(K_SECONDS(10));
    emmc_simple(512 * 1000);
  }
}

void
test::exec(k_work *item)
{
#ifdef CONFIG_TEST_ENABLED

  auto &t = test::get_instance();
  while (t.m_running) {
    k_sleep(K_MSEC(10));
  }

  t.m_running = true;
  t.m_stop    = false;

  struct params_work_container *params =
      CONTAINER_OF(item, struct params_work_container, work);
  switch (static_cast<test_id>(params->test_id)) {
  case test_id::TX_UHF_SIMPLE: {
    t.uhf_tx_simple(params->param0, params->param1, params->param2);
    break;
  }
  case test_id::TX_SBAND_SIMPLE: {
    t.sband_tx_simple(params->param0, params->param1, params->param2);
    break;
  }
  case test_id::EMMC_SIMPLE: {
    t.emmc_simple(params->param0);
    break;
  }
  case test_id::EMC_ROUTINE: {
    t.emc_routine();
  }
  default:
    break;
  }

  t.m_running = false;
  t.m_stop    = false;
#endif
}

void
test::stop()
{
  m_stop = true;
}

bool
test::running() const
{
  return m_running;
}

test::test() : m_emmc_success(false), m_running(false), m_stop(false)
{
  init_workq();
}
