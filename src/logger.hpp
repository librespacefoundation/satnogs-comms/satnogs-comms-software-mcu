/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */
#pragma once

#include <etl/circular_buffer.h>
#include <etl/string.h>
#include <initializer_list>
#include <satnogs-comms/exception.hpp>
#include <string>
#include <zephyr/devicetree.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>

/**
 * @class logger
 * @brief A centralized, thread-safe logging utility for the SatNOGS-COMMS MCU
 * software.
 *
 * The logger class is designed to facilitate a consistent approach to logging
 * within the SatNOGS-COMMS system. It supports multiple log targets:
 * - RTT (Real-Time Transfer)
 * - UART output
 * - Ring buffer storage for historical retrieval
 * - EMMC for persistent storage
 *
 * @todo Currently EMMC logging is not yet implemented in this class.
 *
 * The @ref logger class is a singleton, ensuring that all system components
 * share the same logging interface. It can log messages and exceptions,
 * including those derived from the `satnogs::comms::exception` class and
 * standard exceptions.
 *
 * Usage involves obtaining the singleton instance via
 * `logger::get_instance()` and calling the appropriate `log()` method for the
 * data type at hand.
 */
class logger
{
public:
  /**
   * @brief Enumeration for targets to send logs to.
   */
  enum class target
  {
    RTT  = 0, /**< Real-Time Transfer logging (Segger RTT) */
    UART = 1, /**< UART logging output */
    RING_BUFFER =
        2,   /**< In-memory ring buffer for storing recent log messages */
    EMMC = 3 /**< eMMC logging for persistent storage */
  };

  logger(logger const &) =
      delete; /**< Disabled copy constructor to enforce singleton. */

  void
  operator=(logger const &) =
      delete; /**< Disabled assignment operator to enforce singleton. */

  /**
   * @brief Singleton access to the \ref logger subsystem
   *
   * @return logger&
   */
  static logger &
  get_instance()
  {
    static logger instance;
    return instance;
  }

  void
  boot();

  void
  log(std::initializer_list<target> list, const satnogs::comms::exception &e);

  void
  log(std::initializer_list<target> list, const std::exception &e);

  void
  log(std::initializer_list<target> list, const char *msg);

  void
  log(const satnogs::comms::exception &e);

  void
  log(const std::exception &e);

  void
  log(const char *msg);

  void
  log(const etl::istring &msg);

  etl::string<CONFIG_LOG_MAX_MSG_LEN>
  get_latest_exception() const;

private:
  /**
   * @brief Internal helper function to log a message without acquiring a lock.
   *
   * Iterates over the provided logging targets and forwards the message to
   * the appropriate push function.
   *
   * @tparam T The type of the message object (exception or string).
   * @param list A list of logging targets.
   * @param m The message or exception object to log.
   */
  template <typename T>
  void
  log_unlocked(std::initializer_list<target> list, const T &m)
  {
    for (auto i : list) {
      switch (i) {
      case target::RTT:
        rtt_push(m);
        break;
      case target::UART:
        uart_push(m);
        break;
      case target::RING_BUFFER:
        ring_buffer_push(m);
        break;
      case target::EMMC:
        break;
      default:
        break;
      }
    }
  }

  void
  rtt_push(const satnogs::comms::exception &e);

  void
  rtt_push(const std::exception &e);

  void
  rtt_push(const char *msg);

  void
  ring_buffer_push(const satnogs::comms::exception &e);

  void
  ring_buffer_push(const std::exception &e);

  void
  ring_buffer_push(const char *msg);

  void
  uart_push(const satnogs::comms::exception &e);

  void
  uart_push(const std::exception &e);

  void
  uart_push(const char *msg);

  int
  backup_sram_push(const satnogs::comms::exception &e);

  int
  backup_sram_push(const std::exception &e);

  logger();

  /**
   * @brief In-memory ring buffer for recent log messages.
   *
   * The size and capacity are defined by `CONFIG_LOG_MAX_MSG_LEN` and
   * `CONFIG_LOG_RING_BUFFER_MSGS`.
   */
  etl::circular_buffer<etl::string<CONFIG_LOG_MAX_MSG_LEN>,
                       CONFIG_LOG_RING_BUFFER_MSGS>
      m_ring_buffer;
  /**
   * @brief Mutex for thread-safe logging operations.
   */
  mutable struct k_mutex m_mtx;
};
