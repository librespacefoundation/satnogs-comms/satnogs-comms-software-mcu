/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include "bsp/bsp.hpp"
#include "error_handler.hpp"
#include "msg_arbiter.hpp"
#include "time.hpp"
#include <etl/bit_stream.h>
#include <satnogs-comms/version.hpp>
#include <version.hpp>
#include <zephyr/kernel.h>
#ifdef CONFIG_RETENTION_BOOTLOADER_INFO
#include <bootutil/boot_status.h>
#include <bootutil/image.h>
#include <zephyr/settings/settings.h>
#endif

namespace sc = satnogs::comms;

class telemetry
{
public:
  static constexpr uint32_t min_period_ms = 100;

  enum class telemetry_apid : uint16_t
  {
    PING_RESP         = 100,
    CMD_ACK           = 101,
    BASIC             = 102,
    POWER             = 103,
    HEALTH            = 104,
    CONFIG            = 105,
    FPGA              = 106,
    RADIO             = 107,
    OTA               = 108,
    GNSS              = 109,
    EMMC_TEST_RESULT  = 110,
    RFFE_PARAMS_UHF   = 111,
    RFFE_PARAMS_SBAND = 112,
    BOOTLOADER_INFO   = 113
  };

  class sensors
  {
  public:
    static void
    serialize(etl::bit_stream_writer &writer)
    {
      auto &board = sc::board::get_instance();
      telemetry::serialize(writer,
                           board.temperature(sc::temperature_sensor::PCB));
      telemetry::serialize(writer,
                           board.temperature(sc::temperature_sensor::UHF_PA));
      telemetry::serialize(writer,
                           board.temperature(sc::temperature_sensor::SBAND_PA));
      writer.write<bool>(board.alert(sc::temperature_sensor::UHF_PA), 1);
      writer.write<bool>(board.alert(sc::temperature_sensor::SBAND_PA), 1);
    }
  };

  class power
  {
  public:
    static void
    serialize(etl::bit_stream_writer &writer)
    {
      const auto &pwr = sc::board::get_instance().power();

      // Serialize power_enable
      writer.write<bool>(pwr.enabled(sc::power::subsys::RF_5V), 1);
      writer.write<bool>(pwr.enabled(sc::power::subsys::FPGA_5V), 1);
      writer.write<bool>(pwr.enabled(sc::power::subsys::CAN1), 1);
      writer.write<bool>(pwr.enabled(sc::power::subsys::CAN1_LPWR), 1);
      writer.write<bool>(pwr.enabled(sc::power::subsys::CAN2), 1);
      writer.write<bool>(pwr.enabled(sc::power::subsys::CAN2_LPWR), 1);
      writer.write<bool>(pwr.enabled(sc::power::subsys::UHF), 1);
      writer.write<bool>(pwr.enabled(sc::power::subsys::SBAND), 1);

      telemetry::serialize(writer, pwr.voltage(sc::power::channel::VIN));
      telemetry::serialize(writer, pwr.current(sc::power::channel::VIN));
      telemetry::serialize(writer, pwr.current(sc::power::channel::FPGA));
      telemetry::serialize(writer, pwr.current(sc::power::channel::DIG_3V3));
      telemetry::serialize(writer, pwr.current(sc::power::channel::RF_5V));
      telemetry::serialize(writer, pwr.get_power(sc::power::sensor::EMC1702));
      telemetry::serialize(writer, pwr.get_power(sc::power::sensor::EFUSES));
      telemetry::serialize(writer, pwr.voltage(sc::power::channel::V_BAT));

      writer.write<bool>(pwr.pgood(sc::power::pgood_tp::RAIL_5V), 1);
      writer.write<bool>(pwr.pgood(sc::power::pgood_tp::RAIL_FPGA), 1);
      writer.write<bool>(pwr.pgood(sc::power::pgood_tp::RAIL_UHF), 1);
      writer.write<bool>(pwr.pgood(sc::power::pgood_tp::RAIL_SBAND), 1);
    }
  };

  class uhf_radio
  {
  public:
    uhf_radio(uhf_radio const &) = delete;

    void
    operator=(uhf_radio const &) = delete;

    uhf_radio() = delete;

    static void
    serialize(etl::bit_stream_writer &writer)
    {
      using scr = sc::radio;

      auto &b     = sc::board::get_instance();
      auto &radio = b.radio();

      writer.write(radio.enabled(scr::interface::UHF), 1);
      writer.write(static_cast<bool>(radio.direction(scr::interface::UHF)), 1);
      writer.write(static_cast<uint8_t>(radio.uhf().get_filter()), 1);
      telemetry::serialize(writer, radio.frequency(scr::interface::UHF,
                                                   sc::rf_frontend::dir::TX));
      telemetry::serialize(writer, radio.frequency(scr::interface::UHF,
                                                   sc::rf_frontend::dir::RX));

      const auto &stats = radio.get_stats(scr::interface::UHF);
      writer.write(stats.tx_frames, 32);
      writer.write(stats.tx_frames_fail, 32);
      writer.write(stats.tx_frames_drop, 32);
      writer.write(stats.rx_frames, 32);
      writer.write(stats.rx_frames_inval, 32);
      writer.write(stats.rx_frames_drop, 32);
    }
  };

  class sband_radio
  {
  public:
    sband_radio(sband_radio const &) = delete;

    void
    operator=(sband_radio const &) = delete;

    sband_radio() = delete;

    static void
    serialize(etl::bit_stream_writer &writer)
    {
      using scr = sc::radio;

      auto &b     = sc::board::get_instance();
      auto &radio = b.radio();

      writer.write(radio.enabled(scr::interface::SBAND), 1);
      writer.write(static_cast<bool>(radio.direction(scr::interface::SBAND)),
                   1);
      writer.write(radio.sband().mixer_lock(), 1);
      writer.write(static_cast<uint8_t>(radio.sband().get_filter()), 1);
      telemetry::serialize(writer, radio.frequency(scr::interface::SBAND,
                                                   sc::rf_frontend::dir::TX));
      telemetry::serialize(writer, radio.frequency(scr::interface::SBAND,
                                                   sc::rf_frontend::dir::RX));

      const auto &stats = radio.get_stats(scr::interface::SBAND);
      writer.write(stats.tx_frames, 32);
      writer.write(stats.tx_frames_fail, 32);
      writer.write(stats.tx_frames_drop, 32);
      writer.write(stats.rx_frames, 32);
      writer.write(stats.rx_frames_inval, 32);
      writer.write(stats.rx_frames_drop, 32);
    }
  };

  class ccsds_tm_header
  {
  public:
    static constexpr size_t size = 6U;
    uint8_t                 version;
    uint8_t                 type;
    uint8_t                 sec_hdr;
    telemetry_apid          apid;
    uint8_t                 group_flags;
    uint16_t                count;
    uint16_t                length;

    ccsds_tm_header()
        : version(0),
          type(0),
          sec_hdr(0),
          apid(telemetry_apid::PING_RESP),
          group_flags(0b11),
          count(0),
          length(0)
    {
    }

    void
    serialize(etl::bit_stream_writer &writer) const
    {
      writer.write<uint8_t>(version, 3);
      writer.write<uint8_t>(type, 1);
      writer.write<uint8_t>(sec_hdr, 1);
      writer.write<uint16_t>(static_cast<uint16_t>(apid), 11);
      writer.write<uint8_t>(group_flags, 2);
      writer.write<uint16_t>(count, 14);
      writer.write<uint16_t>(length, 16);
    }
  };

  class telemetry_basic
  {
  public:
    static constexpr size_t size = 119U;

    telemetry_basic(telemetry_basic const &) = delete;

    void
    operator=(telemetry_basic const &) = delete;

    telemetry_basic() = delete;

    static void
    serialize(etl::bit_stream_writer &writer)
    {
      auto &s = settings::get_instance();
      writer.write<uint64_t>(k_uptime_get(), 64);
      writer.write<uint32_t>(s.get<settings::param::BOOT_COUNT>(), 32);
      // Serialize uhf_radio
      uhf_radio::serialize(writer);
      // Serialize sband_radio
      sband_radio::serialize(writer);
      // Serialize power
      power::serialize(writer);
    }
  };

  class telemetry_health
  {
  public:
    static constexpr size_t size = 62U + CONFIG_LOG_MAX_MSG_LEN;

    telemetry_health(telemetry_health const &) = delete;

    void
    operator=(telemetry_health const &) = delete;

    telemetry_health() = delete;

    static void
    serialize(etl::bit_stream_writer &writer)
    {
      auto &s = settings::get_instance();
      writer.write<uint64_t>(k_uptime_get(), 64);
      writer.write<uint32_t>(s.get<settings::param::BOOT_COUNT>(), 32);
      // Serialize sensors
      sensors::serialize(writer);
      // Serialize rst_cause
      writer.write<uint16_t>(error_handler::get_instance().hwinfo_reset_cause(),
                             15);
      // Serialize power
      power::serialize(writer);
      // Add padding
      for (size_t i = 0; i < 3; ++i) {
        writer.write<uint8_t>(0U, 1);
      }
      // Serialize latest exception message
      auto &log              = logger::get_instance();
      auto  retained_mem_str = log.get_latest_exception();
      for (size_t i = 0; i < retained_mem_str.size(); ++i) {
        writer.write<uint8_t>(static_cast<uint8_t>(retained_mem_str[i]), 8);
      }
    }
  };

  class telemetry_radio
  {
  public:
    static constexpr size_t size = 74U;

    telemetry_radio(telemetry_radio const &) = delete;

    void
    operator=(telemetry_radio const &) = delete;

    telemetry_radio() = delete;

    static void
    serialize(etl::bit_stream_writer &writer)
    {
      // Serialize uhf_radio
      uhf_radio::serialize(writer);
      // Serialize sband_radio
      sband_radio::serialize(writer);
    }
  };

  class telemetry_power
  {
  public:
    static constexpr size_t size = 34U;

    telemetry_power(telemetry_power const &) = delete;

    void
    operator=(telemetry_power const &) = delete;

    telemetry_power() = delete;

    static void
    serialize(etl::bit_stream_writer &writer)
    {
      power::serialize(writer);
    }
  };

  class telemetry_ack
  {
  public:
    static constexpr size_t size = 1U;

    telemetry_ack(telemetry_ack const &) = delete;

    void
    operator=(telemetry_ack const &) = delete;

    telemetry_ack() = delete;

    static void
    serialize(etl::bit_stream_writer &writer, bool success)
    {
      writer.write<bool>(success, 1);
    }
  };

  class telemetry_config
  {
  public:
    static constexpr size_t hash_max_len = 256 / 8;
    static constexpr size_t size         = 37U + 2 * hash_max_len;

    telemetry_config(telemetry_config const &) = delete;

    void
    operator=(telemetry_config const &) = delete;

    telemetry_config() = delete;

    static void
    append_git_hash(etl::bit_stream_writer &writer, const char *hash)
    {
      size_t hash_len = strnlen(hash, hash_max_len);
      for (size_t i = 0; i < hash_len; i++) {
        writer.write<uint8_t>(hash[i], 8);
      }
      for (size_t i = hash_len; i < hash_max_len; i++) {
        writer.write<uint8_t>(0x0, 8);
      }
    }

    static void
    serialize(etl::bit_stream_writer &writer)
    {
      auto &s = settings::get_instance();
      /* harware version */
      writer.write<uint32_t>(sc::version::hw_major, 32);
      writer.write<uint32_t>(sc::version::hw_minor, 32);
      writer.write<uint32_t>(sc::version::hw_patch, 32);

      /* libsatnogs-comms version */
      writer.write<uint32_t>(sc::version::lib_major, 32);
      writer.write<uint32_t>(sc::version::lib_minor, 32);
      writer.write<uint32_t>(sc::version::lib_patch, 32);
      append_git_hash(writer, sc::version::lib_git_hash);

      /* firmware version */
      writer.write<uint32_t>(satnogs::version::fw_major, 32);
      writer.write<uint32_t>(satnogs::version::fw_minor, 32);
      writer.write<uint32_t>(satnogs::version::fw_patch, 32);
      append_git_hash(writer, satnogs::version::fw_git_hash);

      writer.write<bool>(!s.get<settings::param::UHF_TX_EN>(), 1);
      writer.write<bool>(!s.get<settings::param::SBAND_TX_EN>(), 1);
    }
  };

  class telemetry_fpga
  {
  public:
    static constexpr size_t size = 1U;

    telemetry_fpga(telemetry_fpga const &) = delete;

    void
    operator=(telemetry_fpga const &) = delete;

    telemetry_fpga() = delete;

    static void
    serialize(etl::bit_stream_writer &writer)
    {
      auto &fpga = sc::board::get_instance().fpga();
      writer.write<bool>(fpga.enabled(), 1);
      writer.write<bool>(fpga.get_fpga_done(), 1);
      // TODO: Add correct methods when implemented in libsatnogs-comms
      writer.write<bool>(false, 1);
      writer.write<bool>(false, 1);
      writer.write<bool>(false, 1);
    }
  };

  class telemetry_gnss
  {
  public:
    static constexpr size_t size = 54U;

    telemetry_gnss(telemetry_gnss const &) = delete;

    void
    operator=(telemetry_gnss const &) = delete;

    telemetry_gnss() = delete;

    static void
    serialize(etl::bit_stream_writer &writer)
    {
      auto            &time = sc::time::get_instance();
      struct gnss_data data;
      time.gnss(data);
      telemetry::serialize(writer, data.nav_data.latitude * 1e-9f);
      telemetry::serialize(writer, data.nav_data.longitude * 1e-9f);
      telemetry::serialize(writer, data.nav_data.bearing * 1e-3f);
      telemetry::serialize(writer, data.nav_data.speed * 1e-3f);
      telemetry::serialize(writer, data.nav_data.altitude * 1e-3f);
      writer.write<uint32_t>(data.info.satellites_cnt, 32);
      writer.write<uint32_t>(data.info.hdop, 32);
      writer.write<uint8_t>(data.info.fix_status, 8);
      writer.write<uint8_t>(data.info.fix_quality, 8);
      writer.write<uint32_t>(data.utc.hour, 32);
      writer.write<uint32_t>(data.utc.minute, 32);
      writer.write<uint32_t>(data.utc.millisecond, 32);
      writer.write<uint32_t>(data.utc.month_day, 32);
      writer.write<uint32_t>(data.utc.month, 32);
      writer.write<uint32_t>(data.utc.century_year, 32);
    }
  };

  class telemetry_rffe_params
  {
  public:
    static constexpr size_t size = 49U;

    telemetry_rffe_params(telemetry_rffe_params const &) = delete;

    void
    operator=(telemetry_rffe_params const &) = delete;

    telemetry_rffe_params() = delete;

    static void
    serialize(etl::bit_stream_writer          &writer,
              satnogs::comms::radio::interface iface)
    {
      auto &s = settings::get_instance();
      writer.write<uint8_t>(static_cast<uint8_t>(iface), 1);

      switch (iface) {
      case sc::radio::interface::UHF:
        telemetry::serialize(writer,
                             s.get<settings::param::UHF_RX_RANGE_MIN>());
        telemetry::serialize(writer,
                             s.get<settings::param::UHF_RX_RANGE_MAX>());
        telemetry::serialize(writer,
                             s.get<settings::param::UHF_TX_RANGE_MIN>());
        telemetry::serialize(writer,
                             s.get<settings::param::UHF_TX_RANGE_MAX>());
        telemetry::serialize(writer,
                             s.get<settings::param::UHF_AGC0_RANGE_MIN>());
        telemetry::serialize(writer,
                             s.get<settings::param::UHF_AGC0_RANGE_MAX>());
        telemetry::serialize(writer,
                             s.get<settings::param::UHF_GAIN0_RANGE_MIN>());
        telemetry::serialize(writer,
                             s.get<settings::param::UHF_GAIN0_RANGE_MAX>());
        telemetry::serialize(writer,
                             s.get<settings::param::UHF_AGC0_CALIB_SLOPE>());
        telemetry::serialize(writer,
                             s.get<settings::param::UHF_AGC0_CALIB_INTRCPT>());
        telemetry::serialize(writer,
                             s.get<settings::param::UHF_GAIN0_CALIB_SLOPE>());
        telemetry::serialize(writer,
                             s.get<settings::param::UHF_GAIN0_CALIB_INTRCPT>());
        break;
      case sc::radio::interface::SBAND:
        telemetry::serialize(writer,
                             s.get<settings::param::SBAND_RX_RANGE_MIN>());
        telemetry::serialize(writer,
                             s.get<settings::param::SBAND_RX_RANGE_MAX>());
        telemetry::serialize(writer,
                             s.get<settings::param::SBAND_TX_RANGE_MIN>());
        telemetry::serialize(writer,
                             s.get<settings::param::SBAND_TX_RANGE_MAX>());
        telemetry::serialize(writer,
                             s.get<settings::param::SBAND_AGC0_RANGE_MIN>());
        telemetry::serialize(writer,
                             s.get<settings::param::SBAND_AGC0_RANGE_MAX>());
        telemetry::serialize(writer,
                             s.get<settings::param::SBAND_GAIN0_RANGE_MIN>());
        telemetry::serialize(writer,
                             s.get<settings::param::SBAND_GAIN0_RANGE_MAX>());
        telemetry::serialize(writer,
                             s.get<settings::param::SBAND_AGC0_CALIB_SLOPE>());
        telemetry::serialize(
            writer, s.get<settings::param::SBAND_AGC0_CALIB_INTRCPT>());
        telemetry::serialize(writer,
                             s.get<settings::param::SBAND_GAIN0_CALIB_SLOPE>());
        telemetry::serialize(
            writer, s.get<settings::param::SBAND_GAIN0_CALIB_INTRCPT>());
        break;
      default:
        break;
      }
    }
  };

  class telemetry_bootloader_info
  {
  public:
    static constexpr size_t size = 20U;

    telemetry_bootloader_info(telemetry_bootloader_info const &) = delete;

    void
    operator=(telemetry_bootloader_info const &) = delete;

    telemetry_bootloader_info() = delete;

    static void
    serialize(etl::bit_stream_writer &writer)
    {
#ifdef CONFIG_RETENTION_BOOTLOADER_INFO
      uint8_t var[8];
      auto    get_blinfo = [&](const char *path, size_t size, auto callback) {
        memset(var, 0xFF, sizeof(var));
        settings_runtime_get(path, var, size);
        callback(var);
      };
      auto single_byte = [&](const uint8_t *data) {
        writer.write<uint8_t>(static_cast<uint8_t>(data[0]), 8);
      };
      auto version = [&](const uint8_t *data) {
        struct image_version *version = (struct image_version *)data;
        writer.write<uint32_t>(version->iv_major, 32);
        writer.write<uint32_t>(version->iv_minor, 32);
        writer.write<uint32_t>(version->iv_revision, 32);
      };
      auto max_app_size = [&](const uint8_t *data) {
        uint32_t max_app_size;
        memcpy(&max_app_size, data, sizeof(max_app_size));
        max_app_size /= FLASH_SECTOR_SIZE;
        writer.write<uint32_t>(max_app_size, 32);
      };
      get_blinfo("blinfo/mode", 1, single_byte);
      get_blinfo("blinfo/signature_type", 1, single_byte);
      get_blinfo("blinfo/recovery", 1, single_byte);
      get_blinfo("blinfo/running_slot", 1, single_byte);
      get_blinfo("blinfo/bootloader_version", sizeof(var), version);
      get_blinfo("blinfo/max_application_size", 4, max_app_size);
#endif
    }
  };

  static telemetry &
  get_instance()
  {
    static telemetry tlm;
    return tlm;
  }

  static void
  tlm(msg_arbiter::msg &msg, telemetry_apid type);

  /* Singleton */
  telemetry(telemetry const &) = delete;

  void
  operator=(telemetry const &) = delete;

private:
  telemetry();

  static void
  serialize(etl::bit_stream_writer &writer, float x);
};
