/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <zephyr/kernel.h>

/**
 * @brief IO and GS watchdog
 *
 * This class implements an IO and GS watchdog with configurable via TC
 * expiration period.
 *
 * To disable the \ref io_wdg set the `CONFIG_IO_WDG_PERIOD_MINS` KConfig option
 * to 0.
 *
 * @todo Currently the \ref io_wdg guards only the two RF interfaces. The goal
 * is to enhance it an monitor all configured IO interfaces (UART, CAN, etc)
 */
class io_wdg
{
public:
  /**
   * The minimum allowed period in minutes
   */
  static constexpr uint32_t min_period_mins = 240;

  /**
   * The maximum allowed period in minutes
   */
  static constexpr uint32_t max_period_mins = 20160;

  static constexpr uint32_t task_wdg_interval = 10000;

  /**
   * @brief Singleton access to the \ref io_wdg subsystem
   *
   * @return io_wdg&
   */
  static io_wdg &
  get_instance()
  {
    static io_wdg instance;
    return instance;
  }

  io_wdg(io_wdg const &) = delete;

  void
  operator=(io_wdg const &) = delete;

  void
  start();

  void
  set_period(uint32_t period_mins);

  uint32_t
  get_period();

  uint32_t
  get_ellapsed();

private:
  static void
  monitor_thread(void *arg1, void *arg2, void *arg3);

  void
  update_ellapsed(uint32_t ellapsed_mins);

  io_wdg();

  struct k_thread m_thread_data;
  k_tid_t         m_tid;

  uint32_t m_period_mins;
  uint32_t m_ellapsed_mins;
};
