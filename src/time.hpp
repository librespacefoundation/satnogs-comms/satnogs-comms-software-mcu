/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include "bsp/bsp.hpp"
#include <time.h>
#include <zephyr/device.h>
#include <zephyr/drivers/gnss.h>
#include <zephyr/drivers/rtc.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
#include <zephyr/sys/timeutil.h>

namespace satnogs::comms
{

/**
 * @brief Time and position information
 *
 * This class provides time and position (if available) information.
 * The SatNOGS-COMMS supports diffent sources of time depending on the
 * configuration and the available peripherals. If the RTC battery is present
 * and has enough voltage, the RTC time is retrieved, if the RTC clock has been
 * set at least once. This can be done either by a telecommand or by the second
 * available time source which is the GNSS. This class uses internally the GNSS
 * modem of the Zephyr-RTOS and parses the NMEA messages that are published on
 * one of the available UART ports. If the GNSS declares a valid fix, the
 * announced time is automatically set to the RTC too.
 *
 * If the RTC battery is not present or failed mid-flight but the GNSS time is
 * available, the time is updated based on the last known time information from
 * the GNSS and the uptime.
 *
 * If all the above time sources failed to provide time information, the uptime
 * is used as a last resort.
 *
 */
class time
{
public:
  static time &
  get_instance()
  {
    static time instance;
    return instance;
  }

  /**
   * @brief Source of the reported time
   *
   */
  enum class time_src : uint8_t
  {
    GNSS_ASSISTED_RTC = 0, //!< RTC that may assisted by GNSS. This time source
                           //!< is valid, if the RTC is present and has been set
                           //!< either by a telecommand or by the GNSS
    GNSS_ONLY =
        1, //!< No RTC installed, but there is time information from a GNSS fix.
    UPTIME =
        3, //!< No RTC or GNSS time source. The uptime is used to track time
  };

  /* Singleton */
  time(time const &) = delete;

  void
  operator=(time const &) = delete;

  void
  gnss_data_cb(const struct device *dev, const struct gnss_data *data);

  time_src
  get(uint64_t &t);

  time_src
  get(struct tm &t);

  void
  set(const struct tm &t);

  void
  gnss(struct gnss_data &data);

private:
  /* Milliseconds Since Epoch (January 1, 1970, UTC.) */
  uint64_t               m_millis_since_epoch_gnss;
  bool                   m_gnss_received_fix;
  int64_t                m_last_gnss_update_uptime;
  const struct device   *m_rtc;
  bool                   m_init;
  mutable struct k_mutex m_mtx;
  struct gnss_data       m_gnss_data;

  time();
};

} // namespace satnogs::comms
