/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */
#include "telemetry.hpp"
#include "callbacks.hpp"
#include "error_handler.hpp"
#include "etl/map.h"
#include "settings.hpp"
#include "telecommand.hpp"
#include "time.hpp"
#include <satnogs-comms/power.hpp>
#include <satnogs-comms/radio.hpp>
#include <satnogs-comms/version.hpp>
#include <version.hpp>
#include <zephyr/sys/byteorder.h>
#include <zephyr/task_wdt/task_wdt.h>

namespace sc = satnogs::comms;

void
telemetry::serialize(etl::bit_stream_writer &writer, float x)
{
  static_assert(sizeof(x) == sizeof(uint32_t), "Unsupported float size");
  uint32_t u;
  std::memcpy(&u, &x, 4);
  writer.write<uint32_t>(u, 32);
}

void
telemetry::tlm(msg_arbiter::msg &m, telemetry_apid tlm_type)
{
  telemetry::ccsds_tm_header tm_header;
  auto                      &tlc = telecommand::get_instance();
  tm_header.apid                 = tlm_type;
  tm_header.count                = tlc.get_frame_count();

  switch (tlm_type) {
  case telemetry_apid::PING_RESP: {
    etl::bit_stream_writer bit_stream(m.data, m.len - 2, etl::endian::big);
    tm_header.length = m.len - 8;
    tm_header.serialize(bit_stream);
    m.len = m.len - 2;
    for (size_t i = 0; i < m.len - 6; ++i) {
      bit_stream.write<uint8_t>(*(m.data + 8 + i));
    }
  } break;
  case telemetry_apid::CMD_ACK: {
  } break;
  case telemetry_apid::BASIC: {
    m.len            = ccsds_tm_header::size + telemetry_basic::size;
    tm_header.length = telemetry_basic::size;
    etl::bit_stream_writer bit_stream(
        m.data, ccsds_tm_header::size + telemetry_basic::size,
        etl::endian::big);
    tm_header.serialize(bit_stream);
    telemetry::telemetry_basic::serialize(bit_stream);
  } break;
  case telemetry_apid::POWER: {
    m.len            = ccsds_tm_header::size + telemetry_power::size;
    tm_header.length = telemetry_power::size;
    etl::bit_stream_writer bit_stream(
        m.data, ccsds_tm_header::size + telemetry_power::size,
        etl::endian::big);
    tm_header.serialize(bit_stream);
    telemetry::telemetry_power::serialize(bit_stream);
  } break;
  case telemetry_apid::HEALTH: {
    m.len            = ccsds_tm_header::size + telemetry_health::size;
    tm_header.length = telemetry_health::size;
    etl::bit_stream_writer bit_stream(
        m.data, ccsds_tm_header::size + telemetry_health::size,
        etl::endian::big);
    tm_header.serialize(bit_stream);
    telemetry::telemetry_health::serialize(bit_stream);
  } break;
  case telemetry_apid::CONFIG: {
    m.len            = ccsds_tm_header::size + telemetry_config::size;
    tm_header.length = telemetry_config::size;
    etl::bit_stream_writer bit_stream(
        m.data, ccsds_tm_header::size + telemetry_config::size,
        etl::endian::big);
    tm_header.serialize(bit_stream);
    telemetry::telemetry_config::serialize(bit_stream);
  } break;
  case telemetry_apid::FPGA: {
    m.len            = ccsds_tm_header::size + telemetry_fpga::size;
    tm_header.length = telemetry_fpga::size;
    etl::bit_stream_writer bit_stream(
        m.data, ccsds_tm_header::size + telemetry_fpga::size, etl::endian::big);
    tm_header.serialize(bit_stream);
    telemetry::telemetry_fpga::serialize(bit_stream);
  } break;
  case telemetry_apid::RADIO: {
    m.len            = ccsds_tm_header::size + telemetry_radio::size;
    tm_header.length = telemetry_radio::size;
    etl::bit_stream_writer bit_stream(
        m.data, ccsds_tm_header::size + telemetry_radio::size,
        etl::endian::big);
    tm_header.serialize(bit_stream);
    telemetry::telemetry_radio::serialize(bit_stream);
  } break;
  case telemetry_apid::OTA: {
  } break;
  case telemetry_apid::GNSS: {
    m.len            = ccsds_tm_header::size + telemetry_gnss::size;
    tm_header.length = telemetry_gnss::size;
    etl::bit_stream_writer bit_stream(
        m.data, ccsds_tm_header::size + telemetry_gnss::size, etl::endian::big);
    tm_header.serialize(bit_stream);
    telemetry::telemetry_gnss::serialize(bit_stream);
  } break;
  case telemetry_apid::EMMC_TEST_RESULT: {
    auto &t          = test::get_instance();
    m.len            = ccsds_tm_header::size + telemetry_ack::size;
    tm_header.length = telemetry_ack::size;
    etl::bit_stream_writer bit_stream(
        m.data, ccsds_tm_header::size + telemetry_ack::size, etl::endian::big);
    tm_header.serialize(bit_stream);
    telemetry::telemetry_ack::serialize(bit_stream, t.emmc_success());
  } break;
  case telemetry_apid::RFFE_PARAMS_UHF: {
    m.len            = ccsds_tm_header::size + telemetry_rffe_params::size;
    tm_header.length = telemetry_rffe_params::size;
    etl::bit_stream_writer bit_stream(
        m.data, ccsds_tm_header::size + telemetry_rffe_params::size,
        etl::endian::big);
    tm_header.serialize(bit_stream);
    telemetry::telemetry_rffe_params::serialize(bit_stream,
                                                sc::radio::interface::UHF);
  } break;
  case telemetry_apid::RFFE_PARAMS_SBAND: {
    m.len            = ccsds_tm_header::size + telemetry_rffe_params::size;
    tm_header.length = telemetry_rffe_params::size;
    etl::bit_stream_writer bit_stream(
        m.data, ccsds_tm_header::size + telemetry_rffe_params::size,
        etl::endian::big);
    tm_header.serialize(bit_stream);
    telemetry::telemetry_rffe_params::serialize(bit_stream,
                                                sc::radio::interface::SBAND);
  } break;
  case telemetry_apid::BOOTLOADER_INFO: {
    m.len            = ccsds_tm_header::size + telemetry_bootloader_info::size;
    tm_header.length = telemetry_bootloader_info::size;
    etl::bit_stream_writer bit_stream(
        m.data, ccsds_tm_header::size + telemetry_bootloader_info::size,
        etl::endian::big);
    tm_header.serialize(bit_stream);
    telemetry::telemetry_bootloader_info::serialize(bit_stream);
  } break;
  default:
    break;
  }
}
