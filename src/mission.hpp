/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2025, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <etl/delegate.h>
#include <etl/delegate_service.h>

/**
 * @ingroup mission-specific
 * @brief Mission specific generic callback service
 *
 * This class provides a convenient way to register mission specific
 * functionality executed in strategically selected places. This minimizes the
 * effort of the mission integration and avoid mission implementation to
 * significantly divert from the original codebase, thus simplifying the
 * maintenance of the code.
 *
 * If there is no need for any of some of the available hooks, users may just
 * ignore them and the underlying implementation will ensure that nothing will
 * be executed.
 *
 * Each hook is defines as an
 * [etl::delegate](https://www.etlcpp.com/delegate.html) allowing any type of
 * C++ method to be passed (e.g method of an object, static method, C function
 * etc).
 *
 *
 * @note During your mission development, if a special need for a mission
 * specific hook comes up, please raise an issue and contribute back with the
 * appropriate merge request.
 */
class mission
{
public:
  void
  pre_init() const;

  void
  register_pre_init(etl::delegate<void(void)> callback);

  void
  post_init() const;

  void
  register_post_init(etl::delegate<void(void)> callback);

  void
  start() const;

  void
  register_start(etl::delegate<void(void)> callback);

  /**
   * @brief Get a singleton access to the mission subsystem
   *
   * @return mission& singleton access to the mission subsystem
   */
  static mission &
  get_instance()
  {
    static mission instance;
    return instance;
  }
  /* Singleton */
  mission(mission const &) = delete;
  void
  operator=(mission const &) = delete;

private:
  etl::delegate<void(void)> m_pre_init;
  etl::delegate<void(void)> m_post_init;
  etl::delegate<void(void)> m_start;

  mission() = default;
};

/** @} */