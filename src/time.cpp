/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "time.hpp"
#include "error_handler.hpp"
#include "scoped_lock.hpp"
#include <satnogs-comms/board.hpp>

static const struct device *rtc = DEVICE_DT_GET(DT_ALIAS(rtc));

static satnogs::comms::time *time_inst = nullptr;

#if DT_NODE_EXISTS(DT_ALIAS(gnss))
static const struct device *gnss_uart = DEVICE_DT_GET(DT_ALIAS(uartgnss));

static void
gnss_data_callback_wrapper(const struct device    *dev,
                           const struct gnss_data *data)
{
  if (time_inst) {
    time_inst->gnss_data_cb(dev, data);
  }
}

GNSS_DATA_CALLBACK_DEFINE(DEVICE_DT_GET(DT_ALIAS(gnss)),
                          gnss_data_callback_wrapper);
#endif

namespace satnogs::comms
{

void
time::gnss_data_cb(const struct device *dev, const struct gnss_data *data)
{
  struct tm       time_info;
  struct rtc_time datetime_set;
  time_t          posix_epoch;

  if (data->info.fix_status != GNSS_FIX_STATUS_NO_FIX) {
    m_gnss_received_fix = true;
    time_info.tm_hour   = data->utc.hour;
    time_info.tm_min    = data->utc.minute;
    time_info.tm_sec    = data->utc.millisecond / 1000;
    time_info.tm_mday   = data->utc.month_day;
    time_info.tm_mon    = data->utc.month - 1; // Months are 0-11 in struct tm
    time_info.tm_year   = data->utc.century_year + 100; // Years since 1900
    time_info.tm_isdst  = -1; // Disables daylight saving time (DST)

    posix_epoch = timeutil_timegm(&time_info);

    m_millis_since_epoch_gnss =
        posix_epoch * 1000 + data->utc.millisecond % 1000;
    m_last_gnss_update_uptime = k_uptime_get();

    // Set RTC time
    gmtime_r(&posix_epoch, rtc_time_to_tm(&datetime_set));
    datetime_set.tm_nsec = (data->utc.millisecond % 1000) * 1000000;
    rtc_set_time(m_rtc, &datetime_set);

    scoped_lock lock(&m_mtx);
    m_gnss_data = *data;
  }
}

/**
 * @brief Gets the time in ms
 *
 * The current time depends on the time source and it is equal to:
 *
 * - ms since Linux epoch: In case the time source is
 * time_src::GNSS_ASSISTED_RTC or time_src::GNSS_ONLY
 * - ms since board power up: In case the time source is time_src::UPTIME
 *
 * @param t reference to hold the result
 * @return time::time_src the time source for the time retrieved
 */
time::time_src
time::get(uint64_t &t)
{
  struct rtc_time datetime_get;
  int             rtc_status = rtc_get_time(m_rtc, &datetime_get);

  /*
   * Regardless the GNSS status, we trust the RTC. Every GNSS event updates the
   * RTC, so even with a GNSS downtime, RTC will hold the most accurate time.
   * In addition even with no RTC battery or a failed battery, the RTC
   * peripheral is operational while the board is powered. So if a telecommand
   * issues an update to the RTC, the RTC subsystem will return a valid time
   * information
   */
  if (rtc_status == 0) {
    t = timeutil_timegm(rtc_time_to_tm(&datetime_get)) * 1000 +
        datetime_get.tm_nsec / 1000000;
    return time_src::GNSS_ASSISTED_RTC;
  }

  /* No RTC, but we keep track of time based on the uptime and the last known
   * GNSS fix */
  if (m_gnss_received_fix) {
    t = m_millis_since_epoch_gnss +
        (k_uptime_get() - m_last_gnss_update_uptime);
    return time_src::GNSS_ONLY;
  }

  /* Last resort... */
  t = k_uptime_get() & (~BIT64(63));
  return time_src::UPTIME;
}

/**
 * @brief Gets the current time
 *
 * @see time::get(uint64_t &t) for more details
 *
 * @param t reference to hold the result
 * @return time::time_src the time source for the time retrieved
 */
time::time_src
time::get(struct tm &t)
{
  uint64_t ms_since_epoch;
  auto     ret         = time::get(ms_since_epoch);
  time_t   posix_epoch = ms_since_epoch / 1000;
  gmtime_r(&posix_epoch, &t);
  return ret;
}

/**
 * @brief Sets the RTC time
 *
 * @note Even if the RTC battery is not present, the RTC subsystem is active
 * while the board is powered-up. Therefore its can be used to accurately track
 * the time
 *
 * @param t time in UTC
 */
void
time::set(const struct tm &t)
{

  /* RTC needs the week day. Instead of requesting it from the operator (sic...)
   * we can calculate it
   */
  struct tm tmp = t;
  auto      tt  = mktime(&tmp);
  auto      ret = gmtime_r(&tt, &tmp);
  if (ret == nullptr) {
    return;
  }

  struct rtc_time rtct = {.tm_sec  = ret->tm_sec,
                          .tm_min  = ret->tm_min,
                          .tm_hour = ret->tm_hour,
                          .tm_mday = ret->tm_mday,
                          .tm_mon  = ret->tm_mon,
                          .tm_year = ret->tm_year,
                          .tm_wday = ret->tm_wday,
                          .tm_yday = -1,
                          .tm_nsec = 0};

  rtc_set_time(m_rtc, &rtct);
}

/**
 * @brief Retrieve lates GNSS information
 *
 * @param data reference to store the retrieved information
 */
void
time::gnss(struct gnss_data &data)
{
  scoped_lock lock(&m_mtx);
  data = m_gnss_data;
}

time::time()
    : m_millis_since_epoch_gnss(0),
      m_gnss_received_fix(false),
      m_last_gnss_update_uptime(0),
      m_rtc(rtc)
{

  k_mutex_init(&m_mtx);

#if DT_NODE_EXISTS(DT_ALIAS(gnss))
  auto &err = error_handler::get_instance();
  if (!device_is_ready(gnss_uart)) {
    err.assert_error<device_not_ready_exception>(__FILE__, __LINE__);
  }

  struct uart_config gnss_uart_cfg = {
      .baudrate  = CONFIG_GNSS_UART_BAUDRATE,
      .parity    = UART_CFG_PARITY_NONE,
      .stop_bits = UART_CFG_STOP_BITS_1,
      .data_bits = UART_CFG_DATA_BITS_8,
      .flow_ctrl = UART_CFG_FLOW_CTRL_NONE,
  };

  int rc = uart_configure(gnss_uart, &gnss_uart_cfg);

  if (rc) {
    err.assert_error<device_not_configured_exception>(__FILE__, __LINE__);
  }

#endif

  time_inst = this;
}

} // namespace satnogs::comms
