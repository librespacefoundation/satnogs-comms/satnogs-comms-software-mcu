/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "io_wdg.hpp"
#include "callbacks.hpp"
#include "error_handler.hpp"
#include "settings.hpp"
#include <etl/algorithm.h>
#include <satnogs-comms/board.hpp>
#include <satnogs-comms/radio.hpp>
#include <zephyr/kernel.h>
#include <zephyr/task_wdt/task_wdt.h>

namespace sc = satnogs::comms;

static uint32_t
min_to_ms(uint32_t mins)
{
  return (mins * 60000);
}

/*
converts milliseconds to minutes
we do not factor seconds
*/
static uint32_t
ms_to_min_aprox(uint32_t ms)
{

  return (ms / 60000);
}

#if CONFIG_IO_WDG_PERIOD_MINS > 0
K_THREAD_STACK_DEFINE(io_wdg_thread_stack, CONFIG_IO_WDG_STACK_SIZE);
#endif

/**
 * @brief Starts the \ref io_wdg.
 * This method should be called after the board initialization process
 * (satnogs::comms::board::init())
 */
void
io_wdg::start()
{
#if CONFIG_IO_WDG_PERIOD_MINS > 0
  m_tid = k_thread_create(&m_thread_data, io_wdg_thread_stack,
                          K_THREAD_STACK_SIZEOF(io_wdg_thread_stack),
                          monitor_thread, NULL, NULL, NULL, CONFIG_IO_WDG_PRIO,
                          0, K_NO_WAIT);
  if (!m_tid) {
    k_oops();
  }
  k_thread_name_set(m_tid, "io_wdg");
#endif
}

void
io_wdg::monitor_thread(void *arg1, void *arg2, void *arg3)
{

  auto   &board = sc::board::get_instance();
  auto   &radio = board.radio();
  io_wdg &wdg   = io_wdg::get_instance();
  auto   &s     = settings::get_instance();

  int64_t  ellapsed_time  = 0;
  uint32_t rx_frame_uhf   = 0;
  uint32_t rx_frame_sband = 0;

  int task_wdt_id = task_wdt_add(task_wdg_interval, task_wdt_callback,
                                 (void *)k_current_get());
  task_wdt_feed(task_wdt_id);

  int64_t curr_time = k_uptime_get();
  while (1) {
    try {
      uint32_t period = min_to_ms(s.get<settings::param::IO_WDG_PERIOD_MINS>());
      // dont we need to update the frames every iteration?
      auto &stats_uhf   = radio.get_stats(sc::radio::interface::UHF);
      auto &stats_sband = radio.get_stats(sc::radio::interface::SBAND);

      // we did receive frames - all good, reset timer
      if (rx_frame_uhf != stats_uhf.rx_frames ||
          rx_frame_sband != stats_sband.rx_frames) {
        rx_frame_uhf   = stats_uhf.rx_frames;
        rx_frame_sband = stats_sband.rx_frames;
        ellapsed_time  = 0;
        wdg.update_ellapsed(0);
        task_wdt_feed(task_wdt_id);
        // we did not receive any frames - increment timer
      } else if (ellapsed_time < period) {
        ellapsed_time += (k_uptime_get() - curr_time);
        wdg.update_ellapsed(ms_to_min_aprox(ellapsed_time));
        task_wdt_feed(task_wdt_id);

        // we did not receive any frames and timer is above limit - reset device
      } else if (ellapsed_time >= period) {
        k_sleep(K_FOREVER);
      }
      curr_time = k_uptime_get();
      k_msleep(5000);
    } catch (const sc::exception &e) {
      auto &err = error_handler::get_instance();
      err.handle(e);
    } catch (const std::exception &e) {
      auto &err = error_handler::get_instance();
      err.handle(e);
    }
  }
}

/**
 * @brief Updates the watchdog period in both the currently running instance and
 * at the persistent storage.
 *
 * @param period_mins the period in minutes
 */
void
io_wdg::set_period(uint32_t period_mins)
{
  auto &s = settings::get_instance();
  period_mins =
      etl::clamp(period_mins, io_wdg::min_period_mins, io_wdg::max_period_mins);
  s.set<settings::param::IO_WDG_PERIOD_MINS>(period_mins);
  m_period_mins = period_mins;
}

/**
 * @brief Gets the currently configured period
 *
 * @return uint32_t the period in minutes
 */
uint32_t
io_wdg::get_period()
{
  return m_period_mins;
}

void
io_wdg::update_ellapsed(uint32_t ellapsed_mins)
{
  m_ellapsed_mins = ellapsed_mins;
}

uint32_t
io_wdg::get_ellapsed()
{
  return m_ellapsed_mins;
}

io_wdg::io_wdg()
    : m_period_mins(
          settings::get_instance().get<settings::param::IO_WDG_PERIOD_MINS>()),
      m_ellapsed_mins(0)
{
}
