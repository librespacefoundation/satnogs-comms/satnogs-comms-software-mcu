/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include "logger.hpp"
#include <etl/error_handler.h>
#include <exception>
#include <satnogs-comms/exception.hpp>

/**
 * @class error_handler
 *
 * @brief Centralized Exception and Error Handling Utility
 *
 * The @ref error_handler class serves as the core mechanism for managing
 * exceptions, logging error details, and executing appropriate system-level
 * actions, such as initiating reboots, based on the severity of encountered
 * issues.
 *
 * This class is designed to handle a range of error scenarios, enabling
 * the system to respond gracefully to both minor operational disruptions and
 * critical failures.
 *
 * Key features include:
 * - Logging the error message and file/line information for debugging.
 * - Assertion mechanism to throw exception.
 * - Severity-based error responses, ranging from logging warnings to system
 * reboots.
 */
class error_handler
{
public:
  /**
   * @brief Singleton access to the \ref error_handler subsystem
   *
   * @return error_handler&
   */
  static error_handler &
  get_instance()
  {
    static error_handler instance;
    return instance;
  }
  /* Singleton */
  error_handler(error_handler const &) = delete;
  void
  operator=(error_handler const &) = delete;

  void
  assert_error(const satnogs::comms::exception &e);

  void
  assert_error(bool condition, const satnogs::comms::exception &e);

  uint32_t
  hwinfo_reset_cause() const;

  void
  handle(const satnogs::comms::exception &e);

  void
  handle(const std::exception &e);

  /**
   * @brief Asserts an error with file and line number information.
   *
   * This templated method creates an error object of type `T` using the
   * provided file and line information, then triggers an assertion.
   *
   * @tparam T The type of exception to instantiate.
   * @param file The source file where the error occurred.
   * @param line The line number where the error occurred.
   */
  template <typename T>
  void
  assert_error(const char *file, int line)
  {
    T error(file, line);
    assert_error(error);
  }

  /**
   * @brief Asserts an error based on a condition, with file and line
   * information.
   *
   * This templated method creates an error object of type `T` using the
   * provided file and line information, then triggers an assertion only if
   * the condition is false.
   *
   * @tparam T The type of exception.
   * @param condition The condition to evaluate.
   * @param file The source file where the error occurred.
   * @param line The line number where the error occurred.
   */
  template <typename T>
  void
  assert_error(bool condition, const char *file, int line)
  {
    T error(file, line);
    assert_error(condition, error);
  }

protected:
  error_handler();

  template <typename T>
  void
  log(const T &e) const
  {
    auto &log = logger::get_instance();
    log.log(e);
  }

private:
  uint32_t m_hw_reset_cause;
  uint32_t m_last_errno;
  uint32_t m_errno_cnt;

  void
  get_reset_cause();

  void
  system_reboot() const;
};
