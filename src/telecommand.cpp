/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "telecommand.hpp"
#include "io_wdg.hpp"
#include "telemetry.hpp"

void
telecommand::deserialize(etl::bit_stream_reader &reader, float &x)
{
  uint32_t float_bits = reader.read<uint32_t>(32).value();
  std::memcpy(&x, &float_bits, sizeof(float_bits));
}

bool
telecommand::ccsds_frame_valid(msg_arbiter::msg &m)
{
  etl::bit_stream_reader       bit_stream(m.data, m.len, etl::endian::big);
  telecommand::ccsds_tc_header tc_header;
  tc_header.deserialize(bit_stream);

  /* FIXME: frame checking needs revising */
  if ((tc_header.version != 0) && (tc_header.sec_hdr != 0) &&
      (tc_header.group_flags != 0b11)) {
    return false;
  } else {
    return true;
  }
}

int
telecommand::get_frame_count()
{
  return m_frame_count;
}

/**
 * @brief Tries to decode and process an XTCE format CCSDS space packet
 *
 * @param m the message to place the response
 * @return true if the message was decoded successfully and there is a response
 * @return false if no response has been generated
 */
bool
telecommand::decode_ccsds_xtce(msg_arbiter::msg &m)
{
  etl::bit_stream_reader       bit_stream(m.data, m.len, etl::endian::big);
  telecommand::ccsds_tc_header tc_header;
  tc_header.deserialize(bit_stream);
  m_frame_count++;

  switch (tc_header.packet_id_type) {
  case tc_packet_id::PING: {
    telemetry::tlm(m, telemetry::telemetry_apid::PING_RESP);
    return true;
  }
  case tc_packet_id::TELEMETRY_REQ: {
    return parse_tlm_req(m, bit_stream);
  }
  case tc_packet_id::OTA_REQUEST: {
    return parse_ota_req(m, bit_stream);
  }
  case tc_packet_id::OTA_FINISH: {
    return parse_ota_finish(m, bit_stream);
  }
  case tc_packet_id::OTA_DATA: {
    return parse_ota_data(m, bit_stream);
  }
  case tc_packet_id::TEST_TX_SIMPLE: {
    return parse_test_tx_simple(m, bit_stream);
  }
  case tc_packet_id::TESTS_STOP: {
    return parse_tests_stop(m, bit_stream);
  }
  case tc_packet_id::FPGA_ENABLE: {
    return parse_enable_fpga_req(m, bit_stream);
  }
  case tc_packet_id::RADIO_ENABLE: {
    return parse_enable_radio_req(m, bit_stream);
  }
  case tc_packet_id::SBAND_ENABLE: {
    return parse_enable_sband_req(m, bit_stream);
  }
  case tc_packet_id::UHF_ENABLE: {
    return parse_enable_uhf_req(m, bit_stream);
  }
  case tc_packet_id::EMMC_ENABLE: {
    return parse_enable_emmc_req(m, bit_stream);
  }
  case tc_packet_id::EMMC_DIRECTION: {
    return parse_emmc_dir_req(m, bit_stream);
  }
  case tc_packet_id::STOP_WDT_UPDATE: {
    return parse_stop_wdt_update_req(m, bit_stream);
  }
  case tc_packet_id::FREQ_SET: {
    return parse_set_freq_req(m, bit_stream);
  }
  case tc_packet_id::TX_INHIBIT: {
    return parse_set_tx_inhibit_req(m, bit_stream);
  }
  case tc_packet_id::TX_GAIN: {
    return parse_set_tx_gain_req(m, bit_stream);
  }
  case tc_packet_id::IO_WDT_PERIOD: {
    return parse_set_io_wdg_period_req(m, bit_stream);
  }
  case tc_packet_id::RX_GAIN: {
    return parse_set_rx_gain_req(m, bit_stream);
  }
  case tc_packet_id::EMMC_TEST: {
    return parse_emmc_test(m, bit_stream);
  }
  case tc_packet_id::RESET_RADIO_STATS: {
    return parse_reset_radio_stats(m, bit_stream);
  }
  case tc_packet_id::SET_RFFE_PARAMS: {
    return parse_set_rffe_params(m, bit_stream);
  }
  case tc_packet_id::SET_RTC: {
    return parse_set_rtc(m, bit_stream);
  }
  default: {
    return false;
  }
  }
}

bool
telecommand::parse_set_rffe_params(msg_arbiter::msg       &m,
                                   etl::bit_stream_reader &bit_stream)
{
  telecommand::set_rffe_params req;
  req.deserialize(bit_stream);
  auto &s = settings::get_instance();
  switch (req.iface) {
  case sc::radio::interface::UHF:
    s.set<settings::param::UHF_RX_RANGE_MIN>(req.rx_range_min);
    s.set<settings::param::UHF_RX_RANGE_MAX>(req.rx_range_max);
    s.set<settings::param::UHF_TX_RANGE_MIN>(req.tx_range_min);
    s.set<settings::param::UHF_TX_RANGE_MAX>(req.tx_range_max);
    s.set<settings::param::UHF_AGC0_RANGE_MIN>(req.agc0_range_min);
    s.set<settings::param::UHF_AGC0_RANGE_MAX>(req.agc0_range_max);
    s.set<settings::param::UHF_GAIN0_RANGE_MIN>(req.gain0_range_min);
    s.set<settings::param::UHF_GAIN0_RANGE_MAX>(req.gain0_range_max);
    s.set<settings::param::UHF_AGC0_CALIB_SLOPE>(req.agc0_calib_slope);
    s.set<settings::param::UHF_AGC0_CALIB_INTRCPT>(req.agc0_calib_intrcpt);
    s.set<settings::param::UHF_GAIN0_CALIB_SLOPE>(req.gain0_calib_slope);
    s.set<settings::param::UHF_GAIN0_CALIB_INTRCPT>(req.gain0_calib_intrcpt);
    return false;
  case sc::radio::interface::SBAND:
    s.set<settings::param::SBAND_RX_RANGE_MIN>(req.rx_range_min);
    s.set<settings::param::SBAND_RX_RANGE_MAX>(req.rx_range_max);
    s.set<settings::param::SBAND_TX_RANGE_MIN>(req.tx_range_min);
    s.set<settings::param::SBAND_TX_RANGE_MAX>(req.tx_range_max);
    s.set<settings::param::SBAND_AGC0_RANGE_MIN>(req.agc0_range_min);
    s.set<settings::param::SBAND_AGC0_RANGE_MAX>(req.agc0_range_max);
    s.set<settings::param::SBAND_GAIN0_RANGE_MIN>(req.gain0_range_min);
    s.set<settings::param::SBAND_GAIN0_RANGE_MAX>(req.gain0_range_max);
    s.set<settings::param::SBAND_AGC0_CALIB_SLOPE>(req.agc0_calib_slope);
    s.set<settings::param::SBAND_AGC0_CALIB_INTRCPT>(req.agc0_calib_intrcpt);
    s.set<settings::param::SBAND_GAIN0_CALIB_SLOPE>(req.gain0_calib_slope);
    s.set<settings::param::SBAND_GAIN0_CALIB_INTRCPT>(req.gain0_calib_intrcpt);
    return false;
  default:
    return false;
  }
}

bool
telecommand::parse_set_rtc(msg_arbiter::msg       &m,
                           etl::bit_stream_reader &bit_stream)
{
  telecommand::set_rtc req;
  req.deserialize(bit_stream);
  auto &time = sc::time::get_instance();
  /* Make the appropriate adjustments from humar readable to tm representation
   */
  struct tm t = {.tm_sec  = req.secs,
                 .tm_min  = req.mins,
                 .tm_hour = req.hour,
                 .tm_mday = req.day,
                 .tm_mon  = req.month - 1,
                 .tm_year = req.year - 1900};
  time.set(t);
  return false;
}

bool
telecommand::parse_reset_radio_stats(msg_arbiter::msg       &m,
                                     etl::bit_stream_reader &bit_stream)
{
  telecommand::reset_radio_stats req;
  req.deserialize(bit_stream);
  auto &radio = sc::board::get_instance().radio();
  radio.reset_stats(req.iface);
  return false;
}

bool
telecommand::parse_emmc_test(msg_arbiter::msg       &m,
                             etl::bit_stream_reader &bit_stream)
{
  test_emmc e;
  e.deserialize(bit_stream);
  auto &t = test::get_instance();
  t.exec_test(test::test_id::EMMC_SIMPLE, {e.nbytes});
  return false;
}

bool
telecommand::parse_tlm_req(msg_arbiter::msg       &m,
                           etl::bit_stream_reader &bit_stream)
{
  telemetry::telemetry_apid tlm_type;
  tlm_type = static_cast<telemetry::telemetry_apid>(
      bit_stream.read<uint16_t>(16).value());
  telemetry::tlm(m, tlm_type);
  return true;
}

bool
telecommand::parse_ota_req(msg_arbiter::msg       &m,
                           etl::bit_stream_reader &bit_stream)
{
  return false;
}

bool
telecommand::parse_ota_finish(msg_arbiter::msg       &m,
                              etl::bit_stream_reader &bit_stream)
{
  return false;
}

bool
telecommand::parse_ota_data(msg_arbiter::msg       &m,
                            etl::bit_stream_reader &bit_stream)
{
  return false;
}

bool
telecommand::parse_test_tx_simple(msg_arbiter::msg       &m,
                                  etl::bit_stream_reader &bit_stream)
{
  auto                       &t = test::get_instance();
  telecommand::test_tx_simple req;
  req.deserialize(bit_stream);
  switch (req.iface) {
  case static_cast<uint32_t>(sc::radio::interface::UHF):
    t.exec_test(test::test_id::TX_UHF_SIMPLE,
                {req.nframes, req.delay_us, req.len});
    break;
  case static_cast<uint32_t>(sc::radio::interface::SBAND):
    t.exec_test(test::test_id::TX_SBAND_SIMPLE,
                {req.nframes, req.delay_us, req.len});
    break;
  default:
    break;
  }
  return false;
}

bool
telecommand::parse_tests_stop(msg_arbiter::msg       &m,
                              etl::bit_stream_reader &bit_stream)
{
  auto &t = test::get_instance();
  t.stop();
  return false;
}

bool
telecommand::parse_enable_fpga_req(msg_arbiter::msg       &m,
                                   etl::bit_stream_reader &bit_stream)
{
  telecommand::enable_fpga_req req;
  req.deserialize(bit_stream);
  auto &fpga = sc::board::get_instance().fpga();
  if (req.enable_fpga) {
    fpga.enable(true);
  } else {
    fpga.enable(false);
  }
  telemetry::tlm(m, telemetry::telemetry_apid::CMD_ACK);
  return false;
}

bool
telecommand::parse_enable_radio_req(msg_arbiter::msg       &m,
                                    etl::bit_stream_reader &bit_stream)
{
  telecommand::enable_radio_req req;
  req.deserialize(bit_stream);
  auto &radio = sc::board::get_instance().radio();
  if (req.enable_radio) {
    radio.enable(true);
  } else {
    radio.enable(false);
  }
  telemetry::tlm(m, telemetry::telemetry_apid::CMD_ACK);
  return false;
}

bool
telecommand::parse_enable_sband_req(msg_arbiter::msg       &m,
                                    etl::bit_stream_reader &bit_stream)
{
  telecommand::enable_sband_req req;
  req.deserialize(bit_stream);
  auto &radio = sc::board::get_instance().radio();
  if (req.enable_sband) {
    radio.enable(sc::radio::interface::SBAND, true);
  } else {
    radio.enable(sc::radio::interface::SBAND, false);
  }
  telemetry::tlm(m, telemetry::telemetry_apid::CMD_ACK);
  return false;
}

bool
telecommand::parse_enable_uhf_req(msg_arbiter::msg       &m,
                                  etl::bit_stream_reader &bit_stream)
{
  telecommand::enable_uhf_req req;
  req.deserialize(bit_stream);
  auto &radio = sc::board::get_instance().radio();
  if (req.enable_uhf) {
    radio.enable(sc::radio::interface::UHF, true);
  } else {
    radio.enable(sc::radio::interface::UHF, false);
  }
  telemetry::tlm(m, telemetry::telemetry_apid::CMD_ACK);
  return false;
}

bool
telecommand::parse_enable_emmc_req(msg_arbiter::msg       &m,
                                   etl::bit_stream_reader &bit_stream)
{
  telecommand::enable_emmc_req req;
  req.deserialize(bit_stream);
  auto &emmc = sc::board::get_instance().emmc();
  if (req.enable_emmc) {
    emmc.enable(false);
    emmc.enable(true);
  } else {
    emmc.enable(false);
  }
  telemetry::tlm(m, telemetry::telemetry_apid::CMD_ACK);
  return false;
}

bool
telecommand::parse_emmc_dir_req(msg_arbiter::msg       &m,
                                etl::bit_stream_reader &bit_stream)
{
  telecommand::set_dir_emmc_req req;
  req.deserialize(bit_stream);
  auto &emmc = sc::board::get_instance().emmc();
  switch (req.dir) {
  case telecommand::dir_interface::dir_interface_emmc_to_mcu: {
    emmc.enable(false);
    emmc.enable(true);
    emmc.set_dir(sc::emmc::dir::MCU);
  } break;
  case telecommand::dir_interface::dir_interface_emmc_to_fpga: {
    emmc.enable(false);
    emmc.enable(true);
    emmc.set_dir(sc::emmc::dir::FPGA);
  } break;
  default:
    break;
  }
  telemetry::tlm(m, telemetry::telemetry_apid::CMD_ACK);
  return false;
}

bool
telecommand::parse_stop_wdt_update_req(msg_arbiter::msg       &m,
                                       etl::bit_stream_reader &bit_stream)
{
  telecommand::stop_wdt_update_req req;
  req.deserialize(bit_stream);
  if (req.stop_wdt_update) {
    k_msleep(CONFIG_WATCHDOG_PERIOD_SYS + 1000);
  }
  telemetry::tlm(m, telemetry::telemetry_apid::CMD_ACK);
  return false;
}

bool
telecommand::parse_set_freq_req(msg_arbiter::msg       &m,
                                etl::bit_stream_reader &bit_stream)
{
  telecommand::set_frequency req;
  req.deserialize(bit_stream);
  auto &s = settings::get_instance();
  if (req.iface == sc::radio::interface::UHF &&
      req.direction == sc::rf_frontend::dir::RX) {
    s.set<settings::param::UHF_RX_FREQ>(req.freq);
  } else if (req.iface == sc::radio::interface::UHF &&
             req.direction == sc::rf_frontend::dir::TX) {
    s.set<settings::param::UHF_TX_FREQ>(req.freq);
  } else if (req.iface == sc::radio::interface::SBAND &&
             req.direction == sc::rf_frontend::dir::RX) {
    s.set<settings::param::SBAND_RX_FREQ>(req.freq);
  } else if (req.iface == sc::radio::interface::SBAND &&
             req.direction == sc::rf_frontend::dir::TX) {
    s.set<settings::param::SBAND_TX_FREQ>(req.freq);
  }
  telemetry::tlm(m, telemetry::telemetry_apid::CMD_ACK);
  return false;
}

bool
telecommand::parse_set_tx_inhibit_req(msg_arbiter::msg       &m,
                                      etl::bit_stream_reader &bit_stream)
{
  telecommand::set_tx_inhibit req;
  req.deserialize(bit_stream);
  auto &s = settings::get_instance();
  switch (req.iface) {
  case sc::radio::interface::UHF:
    s.set<settings::param::UHF_TX_EN>(!req.enable);
    break;
  case sc::radio::interface::SBAND:
    s.set<settings::param::SBAND_TX_EN>(!req.enable);
    break;
  default:
    return false;
  }
  telemetry::tlm(m, telemetry::telemetry_apid::CMD_ACK);
  return false;
}

bool
telecommand::parse_set_tx_gain_req(msg_arbiter::msg       &m,
                                   etl::bit_stream_reader &bit_stream)
{
  telecommand::set_tx_gain req;
  req.deserialize(bit_stream);
  auto &s = settings::get_instance();
  switch (req.iface) {
  case satnogs::comms::radio::interface::UHF:
    s.set<settings::param::UHF_TX_GAIN>(req.gain);
    break;
  case satnogs::comms::radio::interface::SBAND:
    s.set<settings::param::SBAND_TX_GAIN>(req.gain);
    break;
  default:
    return false;
  }
  telemetry::tlm(m, telemetry::telemetry_apid::CMD_ACK);
  return false;
}

bool
telecommand::parse_set_io_wdg_period_req(msg_arbiter::msg       &m,
                                         etl::bit_stream_reader &bit_stream)
{
  telecommand::set_io_wdg_period req;
  req.deserialize(bit_stream);
  auto &wdg = io_wdg::get_instance();
  wdg.set_period(req.period_mins);
  telemetry::tlm(m, telemetry::telemetry_apid::CMD_ACK);
  return false;
}

bool
telecommand::parse_set_rx_gain_req(msg_arbiter::msg       &m,
                                   etl::bit_stream_reader &bit_stream)
{
  return false;
}

telecommand::telecommand() : m_frame_count(0) {}
