/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "io.hpp"
#include "callbacks.hpp"
#include "error_handler.hpp"
#include "msg_arbiter.hpp"
#include "settings.hpp"
#include <cstring>
#include <etl/to_string.h>
#include <satnogs-comms/board.hpp>
#include <satnogs-comms/power.hpp>
#include <zephyr/canbus/isotp.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/task_wdt/task_wdt.h>

namespace sc = satnogs::comms;

static constexpr bool pc104_uart_en =
    DT_NODE_HAS_STATUS(DT_NODELABEL(usart3), okay);

static struct k_thread radio_rx_thread_data;
K_THREAD_STACK_DEFINE(radio_rx_thread_stack, CONFIG_RADIO_RX_THREAD_STACK_SIZE);
static struct k_thread uhf_tx_thread_data;
K_THREAD_STACK_DEFINE(uhf_tx_thread_stack, CONFIG_UHF_TX_THREAD_STACK_SIZE);
static struct k_thread sband_tx_thread_data;
K_THREAD_STACK_DEFINE(sband_tx_thread_stack, CONFIG_SBAND_TX_THREAD_STACK_SIZE);

#if CONFIG_IO_CAN1_ENABLE
const struct device *can1_dev = DEVICE_DT_GET(DT_NODELABEL(fdcan1));

static struct k_thread can1_thread_data;
K_THREAD_STACK_DEFINE(can1_thread_stack, CONFIG_CAN1_THREAD_STACK_SIZE);

/* We want to accept messages from everyone */
static const struct isotp_msg_id can1_rx_filter = {
    .std_id   = CONFIG_CAN1_ISOTP_REMOTE_PEER_RX_ADDR,
    .ext_addr = 0,
    .dl       = 8,
    .flags    = 0};

static const struct isotp_msg_id can1_tx_conf = {
    .std_id = CONFIG_CAN1_ISOTP_TX_ADDR, .ext_addr = 0, .dl = 8, .flags = 0};
static const struct isotp_msg_id can1_tx_remote_conf = {
    .std_id   = CONFIG_CAN1_ISOTP_REMOTE_PEER_TX_ADDR,
    .ext_addr = 0,
    .dl       = 8,
    .flags    = 0};

static const struct isotp_msg_id can1_rx_conf = {
    .std_id = CONFIG_CAN1_ISOTP_RX_ADDR, .ext_addr = 0, .dl = 8, .flags = 0};
/*
 * Set the BS to 0 for receiving the ISOTP frame in a single isotp_recv_net()
 * call
 */
const struct isotp_fc_opts can1_fc_opts = {.bs = 8, .stmin = 0};

static struct isotp_recv_ctx can1_recv_ctx;
static struct isotp_send_ctx can1_send_ctx;

#endif

/*
 * Those messages are quite large and we do not want to take the memory from the
 * task stack
 */
static msg_arbiter::msg  can1_msg;
static sc::radio::rx_msg radio_rx_msg;
static msg_arbiter::msg  radio_msg_to_arb;
static msg_arbiter::msg  uhf_msg_from_arb;
static msg_arbiter::msg  sband_msg_from_arb;

#if DT_NODE_HAS_STATUS(DT_ALIAS(pc104_usart3), okay)
static void
uart_handling_callback(const struct device *dev, struct uart_event *evt,
                       void *user_data);
#endif

#if DT_NODE_HAS_STATUS(DT_ALIAS(pc104_usart3), okay)
io::uart usart3(io::uart::identifier::PC104);
#endif

void
io::start()
{
#if CONFIG_IO_CAN1_ENABLE
  m_can1_tid =
      k_thread_create(&can1_thread_data, can1_thread_stack,
                      K_THREAD_STACK_SIZEOF(can1_thread_stack), can1_thread,
                      NULL, NULL, NULL, CONFIG_CAN1_THREAD_PRIO, 0, K_NO_WAIT);
  if (!m_can1_tid) {
    k_oops();
  }
  k_thread_name_set(m_can1_tid, "can1");
#endif

  m_radio_rx_tid = k_thread_create(&radio_rx_thread_data, radio_rx_thread_stack,
                                   K_THREAD_STACK_SIZEOF(radio_rx_thread_stack),
                                   radio_rx_thread, NULL, NULL, NULL,
                                   CONFIG_RADIO_RX_THREAD_PRIO, 0, K_NO_WAIT);

  m_uhf_tx_tid = k_thread_create(&uhf_tx_thread_data, uhf_tx_thread_stack,
                                 K_THREAD_STACK_SIZEOF(uhf_tx_thread_stack),
                                 uhf_tx_thread, NULL, NULL, NULL,
                                 CONFIG_UHF_TX_THREAD_PRIO, 0, K_NO_WAIT);

  m_sband_tx_tid = k_thread_create(&sband_tx_thread_data, sband_tx_thread_stack,
                                   K_THREAD_STACK_SIZEOF(sband_tx_thread_stack),
                                   sband_tx_thread, NULL, NULL, NULL,
                                   CONFIG_SBAND_TX_THREAD_PRIO, 0, K_NO_WAIT);

  if (!m_radio_rx_tid) {
    k_oops();
  }
  if (!m_uhf_tx_tid) {
    k_oops();
  }

  if (!m_sband_tx_tid) {
    k_oops();
  }
  k_thread_name_set(m_radio_rx_tid, "radio_rx");
  k_thread_name_set(m_uhf_tx_tid, "uhf_tx");
  k_thread_name_set(m_sband_tx_tid, "sband_tx");

#if DT_NODE_HAS_STATUS(DT_ALIAS(pc104_usart3), okay)
  const struct device *uart_dev = DEVICE_DT_GET(DT_ALIAS(pc104_usart3));

  const struct uart_config uart_cfg = {.baudrate  = CONFIG_PC104_UART_BAUDRATE,
                                       .parity    = UART_CFG_PARITY_NONE,
                                       .stop_bits = UART_CFG_STOP_BITS_1,
                                       .data_bits = UART_CFG_DATA_BITS_8,
                                       .flow_ctrl = UART_CFG_FLOW_CTRL_NONE};
  uart_rx_disable(uart_dev);
  uart_configure(uart_dev, &uart_cfg);
  uart_callback_set(uart_dev, uart_handling_callback, &usart3);
  uart_rx_enable(uart_dev, usart3.buf[usart3.buf_idx], io::uart::buf_len,
                 CONFIG_PC104_UART_RX_TIMEOUT_MS);
#endif
}

class can_enable_exception : public sc::exception
{
public:
  can_enable_exception(string_type file_name, numeric_type line)
      : sc::exception(file_name, line,
                      error_msg{sc::exception::severity::MAJOR,
                                "Could not enable CAN device", "canenbl",
                                ECANENBL})
  {
  }
};

class isotp_bind_exception : public sc::exception
{
public:
  isotp_bind_exception(string_type file_name, numeric_type line)
      : sc::exception(file_name, line,
                      error_msg{sc::exception::severity::MINOR,
                                "Could not bind the ISOTP device", "isotpbind",
                                EISOTPBIND})
  {
  }
};

class isotp_recv_exception : public sc::exception
{
public:
  isotp_recv_exception(string_type file_name, numeric_type line)
      : sc::exception(file_name, line,
                      error_msg{sc::exception::severity::MINOR,
                                "Could not receive from ISOTP device",
                                "isotprecv", EISOTPRECV})
  {
  }
};

#if CONFIG_IO_CAN1_ENABLE
void
io::can1_thread(void *arg1, void *arg2, void *arg3)
{
  auto &err       = error_handler::get_instance();
  int task_wdt_id = task_wdt_add(CONFIG_WATCHDOG_PERIOD_CAN1, task_wdt_callback,
                                 (void *)k_current_get());
  msg_arbiter    &arb = msg_arbiter::get_instance();
  auto           &pwr = sc::board::get_instance().power();
  struct net_buf *buf;

  pwr.enable(sc::power::subsys::CAN1);
  k_msleep(10);
  int ret = can_start(can1_dev);
  err.assert_error<can_enable_exception>((ret == 0), __FILE__, __LINE__);
  ret = isotp_bind(&can1_recv_ctx, can1_dev, &can1_rx_filter, &can1_rx_conf,
                   &can1_fc_opts, K_FOREVER);
  err.assert_error<isotp_bind_exception>((ret == 0), __FILE__, __LINE__);
  while (1) {
    try {
      task_wdt_feed(task_wdt_id);

      ret = arb.pull(&can1_msg, msg_arbiter::subsys::CAN1, K_NO_WAIT);
      if (ret == 0) {
        ret = isotp_send(&can1_send_ctx, can1_dev, can1_msg.data, can1_msg.len,
                         &can1_tx_conf, &can1_tx_remote_conf, NULL, NULL);
      }
      can1_msg.iface = msg_arbiter::subsys::CAN1;
      can1_msg.len   = 0;
      do {
        ret = isotp_recv_net(&can1_recv_ctx, &buf, K_MSEC(100));
        if (ret == ISOTP_RECV_TIMEOUT) {
          /* This is ok! */
          break;
        }
        if (ret < 0) {
          err.assert_error<isotp_recv_exception>(__FILE__, __LINE__);
        } else {
          /* Clamp to the available MTU */
          size_t ncpy =
              std::min<size_t>(msg_arbiter::mtu - can1_msg.len, buf->len);

          std::memcpy(can1_msg.data + can1_msg.len, buf->data, ncpy);
          can1_msg.len += ncpy;
          net_buf_unref(buf);
        }
      } while (ret > 0);
      if (can1_msg.len > 0) {
        arb.push(&can1_msg);
      }
    } catch (const sc::exception &e) {
      auto &err = error_handler::get_instance();
      err.handle(e);
    } catch (const std::exception &e) {
      auto &err = error_handler::get_instance();
      err.handle(e);
    }
  }
}
#endif

void
io::radio_rx_thread(void *arg1, void *arg2, void *arg3)
{
  int             task_wdt_id = task_wdt_add(CONFIG_WATCHDOG_PERIOD_RADIO_RX,
                                             task_wdt_callback, (void *)k_current_get());
  auto           &radio       = sc::board::get_instance().radio();
  msg_arbiter    &arb         = msg_arbiter::get_instance();
  auto           &log         = logger::get_instance();
  etl::string<32> log_str;
  while (1) {
    try {

      task_wdt_feed(task_wdt_id);
      int ret = radio.recv_msg(radio_rx_msg, 1000);
      if (ret == 0) {
        etl::format_spec fmt;

        log_str = "Frame received with RSSI ";
        etl::to_string(radio_rx_msg.info.rssi, log_str, fmt, true);
        log.log({logger::target::UART, logger::target::RTT}, log_str.c_str());
        radio_msg_to_arb.iface =
            radio_rx_msg.info.iface == sc::radio::interface::UHF
                ? msg_arbiter::subsys::RADIO_UHF
                : msg_arbiter::subsys::RADIO_SBAND;
        std::copy_n(radio_rx_msg.pdu, radio_rx_msg.info.len,
                    radio_msg_to_arb.data);

        radio_msg_to_arb.len = radio_rx_msg.info.len;
        arb.push(&radio_msg_to_arb);
      }
    } catch (const sc::exception &e) {
      auto &err = error_handler::get_instance();
      err.handle(e);
    } catch (const std::exception &e) {
      auto &err = error_handler::get_instance();
      err.handle(e);
    }
  }
}

void
io::uhf_tx_thread(void *arg1, void *arg2, void *arg3)
{
  int          task_wdt_id = task_wdt_add(CONFIG_WATCHDOG_PERIOD_RADIO_TX,
                                          task_wdt_callback, (void *)k_current_get());
  auto        &radio       = sc::board::get_instance().radio();
  msg_arbiter &arb         = msg_arbiter::get_instance();

  static sc::radio::rx_conf rx_conf = {
      .freq = 0.0f,
      .fsk  = {.rate      = AT86RF215_FSK_SRATE_50,
               .mod_idx   = 1.0f,
               .excess_bw = 1.0f},
      .gain = {.gain0_mode = sc::rf_frontend::gain_mode::AUTO,
               .gain0      = {.tgt = -40.0f},
               .gain1_mode = sc::rf_frontend::gain_mode::AUTO,
               .gain1      = {.agc = {.enable = 1,
                                      .freeze = 0,
                                      .reset  = 0,
                                      .avgs   = AT86RF215_AVGS_8,
                                      .input  = 0,
                                      .gcw    = 23,
                                      .tgt    = AT86RF215_TGT_M21}}}};

  while (1) {
    try {
      task_wdt_feed(task_wdt_id);
      int ret = arb.pull(&uhf_msg_from_arb, msg_arbiter::subsys::RADIO_UHF,
                         K_MSEC(100));
      // Nothing received probably. Try again
      if (ret) {
        continue;
      }

      auto &s     = settings::get_instance();
      bool  tx_en = s.get<settings::param::UHF_TX_EN>();

      if (tx_en) {
        radio.enable(sc::radio::interface::UHF, true);

        /* Before any TX set the frequency */
        radio.set_frequency(sc::radio::interface::UHF, sc::rf_frontend::dir::TX,
                            s.get<settings::param::UHF_TX_FREQ>());
        radio.set_test_fsk(sc::radio::interface::UHF);
        radio.set_tx_gain(sc::radio::interface::UHF,
                          s.get<settings::param::UHF_TX_GAIN>());
        radio.tx(sc::radio::interface::UHF, uhf_msg_from_arb.data,
                 uhf_msg_from_arb.len);

        /* Set the RX frequency and start reception again */
        radio.enable(sc::radio::interface::UHF, false);
        radio.enable(sc::radio::interface::UHF, true);

        // FIXME
        radio.uhf().set_filter(sc::rf_frontend::filter::NARROW);
        rx_conf.freq = s.get<settings::param::UHF_RX_FREQ>();
        radio.rx_async(sc::radio::interface::UHF, rx_conf);
      }
    } catch (const sc::exception &e) {
      auto &err = error_handler::get_instance();
      err.handle(e);
    } catch (const std::exception &e) {
      auto &err = error_handler::get_instance();
      err.handle(e);
    }
  }
}

void
io::sband_tx_thread(void *arg1, void *arg2, void *arg3)
{
  int          task_wdt_id = task_wdt_add(CONFIG_WATCHDOG_PERIOD_RADIO_TX,
                                          task_wdt_callback, (void *)k_current_get());
  auto        &radio       = sc::board::get_instance().radio();
  msg_arbiter &arb         = msg_arbiter::get_instance();

  sc::radio::rx_conf rx_conf = {
      .freq = 0.0f,
      .fsk  = {.rate      = AT86RF215_FSK_SRATE_200,
               .mod_idx   = 0.5f,
               .excess_bw = 1.0f},
      .gain = {.gain0_mode = sc::rf_frontend::gain_mode::AUTO,
               .gain0      = {.tgt = -40.0f},
               .gain1_mode = sc::rf_frontend::gain_mode::AUTO,
               .gain1      = {.agc = {.enable = 1,
                                      .freeze = 0,
                                      .reset  = 0,
                                      .avgs   = AT86RF215_AVGS_8,
                                      .input  = 0,
                                      .gcw    = 23,
                                      .tgt    = AT86RF215_TGT_M21}}}};

  while (1) {
    try {
      task_wdt_feed(task_wdt_id);
      int ret = arb.pull(&sband_msg_from_arb, msg_arbiter::subsys::RADIO_SBAND,
                         K_MSEC(100));
      auto &s = settings::get_instance();
      bool  tx_en = s.get<settings::param::SBAND_TX_EN>();
      if (ret == 0 && tx_en) {
        radio.enable(sc::radio::interface::SBAND, true);

        /* Before any TX set the frequency */
        radio.set_frequency(sc::radio::interface::SBAND,
                            sc::rf_frontend::dir::TX,
                            s.get<settings::param::SBAND_TX_FREQ>());
        radio.set_test_fsk(sc::radio::interface::SBAND);
        radio.set_tx_gain(sc::radio::interface::SBAND,
                          s.get<settings::param::SBAND_TX_GAIN>());
        radio.tx(sc::radio::interface::SBAND, sband_msg_from_arb.data,
                 sband_msg_from_arb.len);

        /* Set the RX frequency and start reception again */
        radio.enable(sc::radio::interface::SBAND, false);
        radio.enable(sc::radio::interface::SBAND, true);

        // FIXME
        radio.sband().set_filter(sc::rf_frontend::filter::NARROW);
        rx_conf.freq = s.get<settings::param::SBAND_RX_FREQ>();
        radio.rx_async(sc::radio::interface::SBAND, rx_conf);
      }
    } catch (const sc::exception &e) {
      auto &err = error_handler::get_instance();
      err.handle(e);
    } catch (const std::exception &e) {
      auto &err = error_handler::get_instance();
      err.handle(e);
    }
  }
}

#if DT_NODE_HAS_STATUS(DT_ALIAS(pc104_usart3), okay)
static void
uart_handling_callback(const struct device *dev, struct uart_event *evt,
                       void *user_data)
{
  io::uart *uart = reinterpret_cast<io::uart *>(user_data);

  switch (evt->type) {
  case UART_TX_DONE:
    break;
  case UART_TX_ABORTED:
    break;
  case UART_RX_RDY: {
    /* Ensure no overflow can happen */
    size_t ncopy =
        std::min<size_t>(CONFIG_MAX_MTU - uart->msg_idx, evt->data.rx.len);
    std::copy_n(&evt->data.rx.buf[evt->data.rx.offset], ncopy,
                &uart->msg.data[uart->msg_idx]);
    uart->msg_idx += ncopy;
    msg_arbiter &arb = msg_arbiter::get_instance();
    uart->msg.iface  = msg_arbiter::subsys::UART_PC104;
    uart->msg.len    = uart->msg_idx;
    arb.push(&uart->msg, true);
    uart->msg_idx = 0;
  } break;
  case UART_RX_BUF_RELEASED:
    break;
  case UART_RX_BUF_REQUEST:
    uart->buf_idx = (uart->buf_idx + 1) % 2;
    uart_rx_buf_rsp(dev, uart->buf[uart->buf_idx], io::uart::buf_len);
    break;
  case UART_RX_DISABLED:
    break;
  default:
    break;
  }
}
#endif