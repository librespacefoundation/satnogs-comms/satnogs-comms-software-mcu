/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include "msg_arbiter.hpp"
#include <cstddef>
#include <cstdint>
#include <zephyr/kernel.h>

class io
{
public:
  class uart
  {
  public:
    /* Reduce the memory footprint by using the bounching buffer option of the
     * async API*/
    static constexpr size_t buf_len = CONFIG_MAX_MTU / 2;
    enum class identifier : uint8_t
    {
      PC104
    };

    uart(identifier id) : id(id), buf_idx(0), msg_idx(0){};

    const identifier id;
    uint8_t          buf_idx;
    uint32_t         msg_idx;
    uint8_t          buf[2][buf_len];
    msg_arbiter::msg msg;
  };

  static io &
  get_instance()
  {
    static io instance;
    return instance;
  }

  /* Singleton */
  io(io const &) = delete;

  void
  operator=(io const &) = delete;

  void
  start();

private:
  static void
  can1_thread(void *arg1, void *arg2, void *arg3);

  static void
  radio_rx_thread(void *arg1, void *arg2, void *arg3);

  static void
  uhf_tx_thread(void *arg1, void *arg2, void *arg3);

  static void
  sband_tx_thread(void *arg1, void *arg2, void *arg3);

  io() = default;

  k_tid_t  m_can1_tid;
  k_tid_t  m_radio_rx_tid;
  k_tid_t  m_uhf_tx_tid;
  k_tid_t  m_sband_tx_tid;
  uint32_t m_can1_rx_cnt;
  uint32_t m_can1_tx_cnt;
  uint32_t m_usart3_rx_cnt;
  uint32_t m_usart3_tx_cnt;
};
