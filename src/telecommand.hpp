/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include "msg_arbiter.hpp"
#include "telemetry.hpp"
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <etl/bit_stream.h>

class telecommand
{
public:
  static telecommand &
  get_instance()
  {
    static telecommand instance;
    return instance;
  }

  enum class tc_packet_id : uint16_t
  {
    PING              = 1,
    TELEMETRY_REQ     = 2,
    OTA_REQUEST       = 3,
    OTA_FINISH        = 4,
    OTA_DATA          = 5,
    TEST_TX_SIMPLE    = 6,
    TESTS_STOP        = 7,
    FPGA_ENABLE       = 8,
    RADIO_ENABLE      = 9,
    SBAND_ENABLE      = 10,
    UHF_ENABLE        = 11,
    EMMC_ENABLE       = 12,
    EMMC_DIRECTION    = 13,
    STOP_WDT_UPDATE   = 14,
    FREQ_SET          = 15,
    TX_INHIBIT        = 16,
    TX_GAIN           = 17,
    IO_WDT_PERIOD     = 18,
    RX_GAIN           = 19,
    EMMC_TEST         = 20,
    RESET_RADIO_STATS = 21,
    SET_RFFE_PARAMS   = 22,
    SET_RTC           = 23
  };

  class rx_gain
  {
  public:
    satnogs::comms::rf_frontend::rx_gain_params params;

    void
    deserialize(etl::bit_stream_reader &reader)
    {
    }
  };

  enum class dir_interface : bool
  {
    dir_interface_emmc_to_mcu  = 0,
    dir_interface_emmc_to_fpga = 1
  };

  class test_tx_simple
  {
  public:
    uint32_t iface;
    uint32_t nframes;
    uint32_t delay_us;
    uint32_t len;
    test_tx_simple() : iface(0), nframes(0), delay_us(0), len(0) {}

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      iface    = reader.read<uint32_t>(1).value();
      nframes  = reader.read<uint32_t>(32).value();
      delay_us = reader.read<uint32_t>(32).value();
      len      = reader.read<uint32_t>(32).value();
    }
  };

  class test_stop
  {
  public:
    bool yes;
    test_stop() : yes(true) {}

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      yes = reader.read<uint8_t>(1).value();
    }
  };

  class power_req
  {
  public:
    bool noop;
    power_req() : noop(false) {}

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      noop = reader.read<uint8_t>(1).value();
    }
  };

  class telemetry_req
  {
  public:
    telemetry::telemetry_apid type;
    telemetry_req() : type(telemetry::telemetry_apid::PING_RESP) {}

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      type = static_cast<telemetry::telemetry_apid>(
          reader.read<uint16_t>(16).value());
    }
  };

  class enable_fpga_req
  {
  public:
    bool enable_fpga;
    enable_fpga_req() : enable_fpga(false) {}

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      enable_fpga = reader.read<uint8_t>(1).value();
    }
  };

  class enable_radio_req
  {
  public:
    bool enable_radio;
    enable_radio_req() : enable_radio(false) {}

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      enable_radio = reader.read<uint8_t>(1).value();
    }
  };

  class enable_sband_req
  {
  public:
    bool enable_sband;
    enable_sband_req() : enable_sband(false) {}

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      enable_sband = reader.read<uint8_t>(1).value();
    }
  };

  class enable_uhf_req
  {
  public:
    bool enable_uhf;
    enable_uhf_req() : enable_uhf(false) {}

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      enable_uhf = reader.read<uint8_t>(1).value();
    }
  };

  class enable_emmc_req
  {
  public:
    bool enable_emmc;
    enable_emmc_req() : enable_emmc(false) {}

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      enable_emmc = reader.read<uint8_t>(1).value();
    }
  };

  class set_dir_emmc_req
  {
  public:
    dir_interface dir;
    set_dir_emmc_req() : dir(dir_interface::dir_interface_emmc_to_mcu) {}

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      dir = static_cast<dir_interface>(reader.read<uint8_t>(1).value());
    }
  };

  class stop_wdt_update_req
  {
  public:
    bool stop_wdt_update;
    stop_wdt_update_req() : stop_wdt_update(false) {}

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      stop_wdt_update = reader.read<uint8_t>(1).value();
    }
  };

  class set_frequency
  {
  public:
    sc::rf_frontend::dir direction;
    sc::radio::interface iface;
    float                freq;
    set_frequency()
        : direction(sc::rf_frontend::dir::RX),
          iface(sc::radio::interface::UHF),
          freq(0.0f)
    {
    }

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      direction =
          static_cast<sc::rf_frontend::dir>(reader.read<uint8_t>(2).value());
      iface =
          static_cast<sc::radio::interface>(reader.read<uint8_t>(1).value());
      telecommand::deserialize(reader, freq);
    }
  };

  class set_tx_inhibit
  {
  public:
    sc::radio::interface iface;
    bool                 enable;
    set_tx_inhibit() : iface(sc::radio::interface::UHF), enable(false) {}

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      iface =
          static_cast<sc::radio::interface>(reader.read<uint8_t>(1).value());
      enable = reader.read<uint8_t>(1).value();
    }
  };

  class set_tx_gain
  {
  public:
    sc::radio::interface iface;
    float                gain;
    set_tx_gain() : iface(sc::radio::interface::UHF), gain(0.0f) {}

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      iface =
          static_cast<sc::radio::interface>(reader.read<uint8_t>(1).value());
      telecommand::deserialize(reader, gain);
    }
  };

  class set_io_wdg_period
  {
  public:
    uint32_t period_mins;
    set_io_wdg_period() : period_mins(0) {}

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      period_mins = reader.read<uint32_t>(32).value();
    }
  };

  class test_emmc
  {
  public:
    uint32_t nbytes;
    test_emmc() : nbytes(0) {}

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      nbytes = reader.read<uint32_t>(32).value();
    }
  };

  class reset_radio_stats
  {
  public:
    sc::radio::interface iface;
    reset_radio_stats() : iface(sc::radio::interface::UHF) {}

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      iface =
          static_cast<sc::radio::interface>(reader.read<uint8_t>(1).value());
    }
  };

  class set_rffe_params
  {
  public:
    sc::radio::interface iface;
    float                rx_range_min;
    float                rx_range_max;
    float                tx_range_min;
    float                tx_range_max;
    float                agc0_range_min;
    float                agc0_range_max;
    float                gain0_range_min;
    float                gain0_range_max;
    float                agc0_calib_slope;
    float                agc0_calib_intrcpt;
    float                gain0_calib_slope;
    float                gain0_calib_intrcpt;

    set_rffe_params()
        : iface(sc::radio::interface::UHF),
          rx_range_min(0.0f),
          rx_range_max(0.0f),
          tx_range_min(0.0f),
          tx_range_max(0.0f),
          agc0_range_min(0.0f),
          agc0_range_max(0.0f),
          gain0_range_min(0.0f),
          gain0_range_max(0.0f),
          agc0_calib_slope(0.0f),
          agc0_calib_intrcpt(0.0f),
          gain0_calib_slope(0.0f),
          gain0_calib_intrcpt(0.0f)
    {
    }

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      iface =
          static_cast<sc::radio::interface>(reader.read<uint8_t>(1).value());
      telecommand::deserialize(reader, rx_range_min);
      telecommand::deserialize(reader, rx_range_max);
      telecommand::deserialize(reader, tx_range_min);
      telecommand::deserialize(reader, tx_range_max);
      telecommand::deserialize(reader, agc0_range_min);
      telecommand::deserialize(reader, agc0_range_max);
      telecommand::deserialize(reader, gain0_range_min);
      telecommand::deserialize(reader, gain0_range_max);
      telecommand::deserialize(reader, agc0_calib_slope);
      telecommand::deserialize(reader, agc0_calib_intrcpt);
      telecommand::deserialize(reader, gain0_calib_slope);
      telecommand::deserialize(reader, gain0_calib_intrcpt);
    }
  };

  class set_rtc
  {
  public:
    uint8_t  secs;
    uint8_t  mins;
    uint8_t  hour;
    uint8_t  day;
    uint8_t  month;
    uint16_t year;

    set_rtc() = default;

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      secs  = reader.read<uint8_t>(8).value();
      mins  = reader.read<uint8_t>(8).value();
      hour  = reader.read<uint8_t>(8).value();
      day   = reader.read<uint8_t>(8).value();
      month = reader.read<uint8_t>(8).value();
      year  = reader.read<uint16_t>(16).value();
    }
  };

  class ccsds_tc_header
  {
  public:
    static constexpr size_t size = 8U;
    uint8_t                 version;
    uint8_t                 type;
    uint8_t                 sec_hdr;
    uint16_t                apid;
    uint8_t                 group_flags;
    uint16_t                count;
    uint16_t                length;
    tc_packet_id            packet_id_type;

    ccsds_tc_header()
        : version(0),
          type(0),
          sec_hdr(0),
          apid(0),
          group_flags(0),
          count(0),
          length(0),
          packet_id_type(tc_packet_id::PING)
    {
    }

    void
    deserialize(etl::bit_stream_reader &reader)
    {
      version     = reader.read<uint8_t>(3).value();
      type        = reader.read<uint8_t>(1).value();
      sec_hdr     = reader.read<uint8_t>(1).value();
      apid        = reader.read<uint16_t>(11).value();
      group_flags = reader.read<uint8_t>(2).value();
      count       = reader.read<uint16_t>(14).value();
      length      = reader.read<uint16_t>(16).value();
      packet_id_type =
          static_cast<tc_packet_id>(reader.read<uint16_t>(16).value());
    }
  };

  /* Singleton */
  telecommand(telecommand const &) = delete;

  void
  operator=(telecommand const &) = delete;

  bool
  ccsds_frame_valid(msg_arbiter::msg &m);

  int
  get_frame_count();

  bool
  decode_ccsds_xtce(msg_arbiter::msg &m);

private:
  telecommand();

  uint32_t m_frame_count;

  static void
  deserialize(etl::bit_stream_reader &reader, float &x);
  bool
  parse_reset_radio_stats(msg_arbiter::msg &m, etl::bit_stream_reader &reader);
  bool
  parse_tlm_req(msg_arbiter::msg &m, etl::bit_stream_reader &reader);
  bool
  parse_ota_req(msg_arbiter::msg &m, etl::bit_stream_reader &reader);
  bool
  parse_ota_finish(msg_arbiter::msg &m, etl::bit_stream_reader &reader);
  bool
  parse_ota_data(msg_arbiter::msg &m, etl::bit_stream_reader &reader);
  bool
  parse_test_tx_simple(msg_arbiter::msg &m, etl::bit_stream_reader &reader);
  bool
  parse_emmc_test(msg_arbiter::msg &m, etl::bit_stream_reader &reader);
  bool
  parse_tests_stop(msg_arbiter::msg &m, etl::bit_stream_reader &reader);
  bool
  parse_enable_fpga_req(msg_arbiter::msg &m, etl::bit_stream_reader &reader);
  bool
  parse_enable_radio_req(msg_arbiter::msg &m, etl::bit_stream_reader &reader);
  bool
  parse_enable_sband_req(msg_arbiter::msg &m, etl::bit_stream_reader &reader);
  bool
  parse_enable_uhf_req(msg_arbiter::msg &m, etl::bit_stream_reader &reader);
  bool
  parse_enable_emmc_req(msg_arbiter::msg &m, etl::bit_stream_reader &reader);
  bool
  parse_emmc_dir_req(msg_arbiter::msg &m, etl::bit_stream_reader &reader);
  bool
  parse_stop_wdt_update_req(msg_arbiter::msg       &m,
                            etl::bit_stream_reader &reader);
  bool
  parse_set_freq_req(msg_arbiter::msg &m, etl::bit_stream_reader &reader);
  bool
  parse_set_tx_inhibit_req(msg_arbiter::msg &m, etl::bit_stream_reader &reader);
  bool
  parse_set_tx_gain_req(msg_arbiter::msg &m, etl::bit_stream_reader &reader);
  bool
  parse_set_io_wdg_period_req(msg_arbiter::msg       &m,
                              etl::bit_stream_reader &reader);
  bool
  parse_set_rx_gain_req(msg_arbiter::msg &m, etl::bit_stream_reader &reader);
  bool
  parse_set_rffe_params(msg_arbiter::msg       &m,
                        etl::bit_stream_reader &bit_stream);
  bool
  parse_set_rtc(msg_arbiter::msg &m, etl::bit_stream_reader &bit_stream);
};
