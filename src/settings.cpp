/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024-2025, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "settings.hpp"
#include "error_handler.hpp"
#include <satnogs-comms/exception.hpp>
#include <zephyr/fs/fs.h>
#include <zephyr/fs/littlefs.h>
#include <zephyr/storage/flash_map.h>

namespace sc = satnogs::comms;
using radio  = sc::radio;

FS_FSTAB_DECLARE_ENTRY(DT_NODELABEL(lfs1));
struct fs_mount_t *mountpoint = &FS_FSTAB_ENTRY(DT_NODELABEL(lfs1));

/**
 * @brief Construct a new settings::settings object and mounts the LittleFS
 * partition
 *
 */
settings::settings()
{
  k_mutex_init(&m_mtx);

  /*
   * If the user has selected it, erase the entire settings area. The settings
   * subsystem then will handle the defaults handling
   */
#if CONFIG_ERASE_SETTINGS
  /* If the user has selected it, erase the entire settings area. The settings
   * subsystem then will handle the defaults handling
   */
  const struct flash_area *pfa;
  flash_area_open(FIXED_PARTITION_ID(lfs1_partition), &pfa);
  flash_area_erase(pfa, 0, pfa->fa_size);
  flash_area_close(pfa);
#endif

  /*
   * If mount fails, the sub-sequent read/write operations will fail and the
   * settings subsystem will provide defaults
   */
  fs_mount(mountpoint);
}

/**
 * @brief Reads a 32-bit setting
 *
 * @param fname the filename of the setting
 * @param x pointer to hold the result
 * @return int 0 on success or negative error code
 */
int
settings::read32(const char *fname, uint32_t *x)
{
  uint32_t         val = 0;
  struct fs_file_t file;
  int              ret;

  fs_file_t_init(&file);
  ret = fs_open(&file, fname, FS_O_READ);
  if (ret) {
    return ret;
  }

  ret = fs_read(&file, &val, sizeof(val));
  if (ret < 0) {
    fs_close(&file);
    return ret;
  }

  ret = fs_close(&file);
  if (ret) {
    return ret;
  }
  *x = val;
  return 0;
}

/**
 * @brief Writes a 32-bit value to a specific setting
 *
 * @param fname the filename of the setting
 * @param x the value to write
 * @return int 0 on success or negative error code
 */
int
settings::write32(const char *fname, uint32_t x)
{
  struct fs_file_t file;
  int              ret;

  fs_file_t_init(&file);
  ret = fs_open(&file, fname, FS_O_CREATE | FS_O_RDWR);
  if (ret && ret != -ENOENT) {
    return ret;
  }

  ret = fs_write(&file, &x, sizeof(x));
  if (ret < 0) {
    fs_close(&file);
    return ret;
  }

  return fs_close(&file);
}

/**
 * @brief Writes a floating point setting
 *
 * @param fname the filename of the setting
 * @param x pointer to hold the result
 * @return int 0 on success or negative error code
 */
int
settings::readf(const char *fname, float *x)
{
  uint32_t val = 0;
  int      ret = read32(fname, &val);
  if (ret) {
    return ret;
  }
  memcpy(x, &val, sizeof(float));
  return 0;
}

/**
 * @brief Writes a floating value to a specific setting
 *
 * @param fname the filename of the setting
 * @param x the value to write
 * @return int 0 on success or negative error code
 */
int
settings::writef(const char *fname, float x)
{
  uint32_t val = 0;
  memcpy(&val, &x, sizeof(val));
  return write32(fname, val);
}

bool
settings::param_valid(param id)
{
  return false;
}

/**
 * @brief Increments the current boot count and stores it at the persistent
 * storage
 *
 * @warning this method is not thread-safe
 */
void
settings::incr_boot_cnt()
{
  uint32_t cnt = get<settings::param::BOOT_COUNT>();
  set<settings::param::BOOT_COUNT>(++cnt);
}

/**
 * @brief Retrieves the information regarding if this is the first time that the
 * system deploys
 *
 * @return true has already deployed
 * @return false if this is the first time that the system powers up
 *
 * @note Internally this is implemented by checking the boot counter
 */
bool
settings::deployed()
{
  return get<param::BOOT_COUNT>() > 0;
}
