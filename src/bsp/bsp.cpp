/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */
#include "bsp.hpp"
#include "../error_handler.hpp"
#include <satnogs-comms/board.hpp>

namespace sc = satnogs::comms;

/* spi_bsp Class Method Definitions */

spi_bsp::spi_bsp(const struct device *dev, const struct spi_config &spi_cfg)
    : m_dev(dev), m_spi_cfg(spi_cfg)
{
  k_mutex_init(&m_mtx);
}

void
spi_bsp::read(uint8_t *rx, const uint8_t *tx, size_t tx_len, size_t rx_len)
{
  auto       &err = error_handler::get_instance();
  scoped_lock lock(&m_mtx);

  struct spi_buf tx_buf = {.buf = const_cast<uint8_t *>(tx), .len = tx_len};
  struct spi_buf rx_buf = {.buf = const_cast<uint8_t *>(rx), .len = rx_len};

  struct spi_buf_set tx_bufs = {
      .buffers = &tx_buf,
      .count   = 1,
  };
  struct spi_buf_set rx_bufs = {
      .buffers = &rx_buf,
      .count   = 1,
  };

  int ret = spi_transceive(m_dev, &m_spi_cfg, &tx_bufs, &rx_bufs);
  err.assert_error<spi_bsp_exception>((ret == 0), __FILE__, __LINE__);
}

void
spi_bsp::write(const uint8_t *tx, size_t len)
{
  auto       &err = error_handler::get_instance();
  scoped_lock lock(&m_mtx);

  struct spi_buf     tx_buf  = {.buf = const_cast<uint8_t *>(tx), .len = len};
  struct spi_buf_set tx_bufs = {
      .buffers = &tx_buf,
      .count   = 1,
  };

  int ret = spi_transceive(m_dev, &m_spi_cfg, &tx_bufs, nullptr);
  err.assert_error<spi_bsp_exception>((ret == 0), __FILE__, __LINE__);
}

/* i2c_bsp Class Method Definitions */

i2c_bsp::i2c_bsp(const struct device *i2c_dev) : i2c(), m_i2c(i2c_dev) {}

void
i2c_bsp::read(uint16_t addr, uint8_t *rx, size_t rxlen, const uint8_t *tx,
              size_t txlen)
{
  auto &err = error_handler::get_instance();

  if (CONFIG_ENABLE_I2C_REVOVERY) {
    size_t retries = 0;
    int    ret     = -1;
    while (retries < CONFIG_I2C_RECOVERY_RETRIES && ret < 0) {
      ret = i2c_write_read(m_i2c, addr, tx, txlen, rx, rxlen);
      if (!ret) {
        return;
      }
      i2c_recover_bus(m_i2c);
      retries++;
    }
    err.assert_error<i2c_bsp_exception>((ret == 0), __FILE__, __LINE__);
  } else {
    int ret = i2c_write_read(m_i2c, addr, tx, txlen, rx, rxlen);
    err.assert_error<i2c_bsp_exception>((ret == 0), __FILE__, __LINE__);
  }
}

void
i2c_bsp::read(uint16_t addr, uint8_t start_addr, uint8_t *rx, size_t len)
{
  auto &err = error_handler::get_instance();

  if (CONFIG_ENABLE_I2C_REVOVERY) {
    size_t retries = 0;
    int    ret     = -1;
    while (retries < CONFIG_I2C_RECOVERY_RETRIES && ret < 0) {
      ret = i2c_burst_read(m_i2c, addr, start_addr, rx, len);
      if (!ret) {
        return;
      }
      i2c_recover_bus(m_i2c);
      retries++;
    }
    err.assert_error<i2c_bsp_exception>((ret == 0), __FILE__, __LINE__);
  } else {
    int ret = i2c_burst_read(m_i2c, addr, start_addr, rx, len);
    err.assert_error<i2c_bsp_exception>((ret == 0), __FILE__, __LINE__);
  }
}

void
i2c_bsp::write(uint16_t addr, const uint8_t *tx, size_t len)
{
  auto &err = error_handler::get_instance();
  int   ret = i2c_write(m_i2c, tx, len, addr);
  err.assert_error<i2c_bsp_exception>((ret == 0), __FILE__, __LINE__);
}

void
i2c_bsp::write(uint16_t addr, uint8_t start_addr, const uint8_t *tx, size_t len)
{
  auto &err = error_handler::get_instance();
  int   ret = i2c_burst_write(m_i2c, addr, start_addr, tx, len);
  err.assert_error<i2c_bsp_exception>((ret == 0), __FILE__, __LINE__);
}

/**
 * @brief Get the I2C device
 *
 * This method provides access to the Zephyr I2C device allowing customization
 *
 * @warning Use this method at your own risk!
 *
 * @return const struct device* the I@C device
 */
const struct device *
i2c_bsp::dev()
{
  return m_i2c;
}

/* adc_bsp Class Method Definitions */

adc_bsp::adc_bsp(const struct adc_dt_spec &adc_channel)
    : adc(adc_channel.resolution, adc_ref_internal(adc_channel.dev) / 1000.0f),
      m_adc_channel(adc_channel),
      m_adc_sample(0U),
      m_sequence({.buffer = &m_adc_sample, .buffer_size = sizeof(m_adc_sample)})
{
}

void
adc_bsp::start()
{
  int   err;
  auto &err_hdl = error_handler::get_instance();

  if (!adc_is_ready_dt(&m_adc_channel)) {
    err_hdl.assert_error<adc_initialization_exception>(__FILE__, __LINE__);
  }

  err = adc_channel_setup_dt(&m_adc_channel);
  if (err != 0) {
    err_hdl.assert_error<adc_initialization_exception>(__FILE__, __LINE__);
  }

  err = adc_sequence_init_dt(&m_adc_channel, &m_sequence);
  if (err != 0) {
    err_hdl.assert_error<adc_initialization_exception>(__FILE__, __LINE__);
  }
}

void
adc_bsp::stop()
{
}

uint32_t
adc_bsp::read()
{
  adc_read_dt(&m_adc_channel, &m_sequence);
  return m_adc_sample;
}

float
adc_bsp::voltage()
{
  int32_t val_mv = read();
  adc_raw_to_millivolts_dt(&m_adc_channel, &val_mv);
  return val_mv / 1000.0f;
}

/* sensor_bsp Class Method Definitions */

sensor_bsp::sensor_bsp(const struct device *dev) : sensor(), m_dev(dev)
{
  if (!device_is_ready(dev)) {
    auto &err = error_handler::get_instance();
    err.assert_error<device_not_ready_exception>(__FILE__, __LINE__);
  }
}

float
sensor_bsp::vbat()
{
  auto               &err = error_handler::get_instance();
  struct sensor_value val;
  int                 rc;

  rc = sensor_sample_fetch(m_dev);
  if (rc) {
    err.assert_error<v_bat_exception>(__FILE__, __LINE__);
  }

  rc = sensor_channel_get(m_dev, SENSOR_CHAN_VOLTAGE, &val);
  if (rc) {
    err.assert_error<v_bat_exception>(__FILE__, __LINE__);
  }

  return sensor_value_to_double(&val);
}

float
sensor_bsp::vref()
{
  return 0;
}