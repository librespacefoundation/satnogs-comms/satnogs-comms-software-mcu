/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2022-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <cstdint>
#include <logger.hpp>
#include <satnogs-comms/bsp/adc.hpp>
#include <satnogs-comms/bsp/chrono.hpp>
#include <satnogs-comms/bsp/dac.hpp>
#include <satnogs-comms/bsp/gpio.hpp>
#include <satnogs-comms/bsp/i2c.hpp>
#include <satnogs-comms/bsp/msgq.hpp>
#include <satnogs-comms/bsp/sensor.hpp>
#include <satnogs-comms/bsp/spi.hpp>
#include <scoped_lock.hpp>
#include <settings.hpp>
#include <zephyr/device.h>
#include <zephyr/drivers/adc.h>
#include <zephyr/drivers/dac.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/hwinfo.h>
#include <zephyr/drivers/i2c.h>
#include <zephyr/drivers/sensor.h>
#include <zephyr/drivers/spi.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
#include <zephyr/sys/reboot.h>

class watchdog_exception : public satnogs::comms::exception
{
public:
  watchdog_exception(string_type file_name, numeric_type line)
      : exception(file_name, line,
                  error_msg{satnogs::comms::exception::severity::CRITICAL,
                            "Error in watchdog initialization", "errwdt", EWDT})
  {
  }
};

class device_not_ready_exception : public satnogs::comms::exception
{
public:
  device_not_ready_exception(string_type file_name, numeric_type line)
      : exception(file_name, line,
                  error_msg{satnogs::comms::exception::severity::MAJOR,
                            "Sensor: device  not ready.", "devnotready",
                            EDEVNOTREADY})
  {
  }
};

class device_not_configured_exception : public satnogs::comms::exception
{
public:
  device_not_configured_exception(string_type file_name, numeric_type line)
      : exception(file_name, line,
                  error_msg{satnogs::comms::exception::severity::MAJOR,
                            "Could not configure device", "devnotconf",
                            EDEVNOTCONF})
  {
  }
};

class v_bat_exception : public satnogs::comms::exception
{
public:
  v_bat_exception(string_type file_name, numeric_type line)
      : exception(file_name, line,
                  error_msg{satnogs::comms::exception::severity::MAJOR,
                            "Sensor: device failed to read VBAT.", "vbaterror",
                            EVBAT})
  {
  }
};

class sensor_bsp : public satnogs::comms::bsp::sensor
{
public:
  sensor_bsp(const struct device *dev);

  float
  vbat();

  float
  vref();

private:
  const struct device *m_dev;
};
class gpio_bsp : public satnogs::comms::bsp::gpio
{
public:
  gpio_bsp(const struct gpio_dt_spec *io, bool output = true)
      : gpio(), m_gpio(io)
  {
    if (output) {
      gpio_pin_configure_dt(m_gpio, GPIO_OUTPUT_INACTIVE | m_gpio->dt_flags);
    } else {
      gpio_pin_configure_dt(m_gpio, GPIO_INPUT | m_gpio->dt_flags);
    }
  }

  void
  toggle()
  {
    gpio_pin_toggle_dt(m_gpio);
  }

  bool
  get()
  {
    int ret = gpio_pin_get_dt(m_gpio);
    return ret > 0;
  }

  bool
  get_raw()
  {
    int ret = gpio_pin_get_raw(m_gpio->port, m_gpio->pin);
    return ret > 0;
  }

  void
  set(bool s)
  {
    gpio_pin_set_dt(m_gpio, s);
  }

  void
  set_raw(bool s)
  {
    gpio_pin_set_raw(m_gpio->port, m_gpio->pin, s);
  }

  void
  set_direction(satnogs::comms::bsp::gpio::direction dir)
  {
    if (dir == satnogs::comms::bsp::gpio::direction::INPUT) {
      gpio_dt_flags_t mask = 0xFFFF;
      mask ^= GPIO_OUTPUT_ACTIVE;
      mask ^= GPIO_OUTPUT;
      gpio_pin_configure_dt(m_gpio, GPIO_INPUT | (m_gpio->dt_flags & mask));
    } else {
      gpio_dt_flags_t mask = 0xFFFF;
      mask ^= GPIO_INPUT;
      gpio_pin_configure_dt(m_gpio, GPIO_OUTPUT | GPIO_OUTPUT_INACTIVE |
                                        (m_gpio->dt_flags & mask));
    }
  }

private:
  const struct gpio_dt_spec *m_gpio;
};

class spi_bsp_exception : public satnogs::comms::exception
{
public:
  spi_bsp_exception(string_type file_name, numeric_type line)
      : exception(
            file_name, line,
            error_msg{exception::severity::MAJOR, "SPI error", "spierr", ESPI})
  {
  }
};

class spi_bsp : public satnogs::comms::bsp::spi
{
public:
  spi_bsp(const struct device *dev, const struct spi_config &spi_cfg);

  void
  read(uint8_t *rx, const uint8_t *tx, size_t tx_len, size_t rx_len) override;

  void
  write(const uint8_t *tx, size_t len) override;

private:
  const struct device    *m_dev;
  const struct spi_config m_spi_cfg;
  struct k_mutex          m_mtx;
};

/**
 * @brief Same as the spi_bsp but with manual control of the CS line
 *
 */
class spi_manual_cs_bsp : public spi_bsp
{
public:
  spi_manual_cs_bsp(const struct device *dev, const struct spi_config &spi_cfg,
                    const struct gpio_dt_spec *cs)
      : spi_bsp(dev, spi_cfg), m_cs(cs, true)
  {
  }

  satnogs::comms::bsp::gpio &
  cs() override
  {
    return m_cs;
  }

private:
  gpio_bsp m_cs;
};

class chrono_bsp : public satnogs::comms::bsp::chrono
{

  void
  delay_us(size_t us) override
  {
    k_usleep(us);
  }

  int64_t
  time_ms() override
  {
    return k_uptime_get();
  }
};

class i2c_bsp_exception : public satnogs::comms::exception
{
public:
  i2c_bsp_exception(string_type file_name, numeric_type line)
      : exception(
            file_name, line,
            error_msg{exception::severity::MINOR, "i2c error", "i2cerr", EI2C})
  {
  }
};

class i2c_bsp : public satnogs::comms::bsp::i2c
{
public:
  i2c_bsp(const struct device *i2c_dev);

  void
  read(uint16_t addr, uint8_t *rx, size_t rxlen, const uint8_t *tx,
       size_t txlen);

  void
  read(uint16_t addr, uint8_t start_addr, uint8_t *rx, size_t len);

  void
  write(uint16_t addr, const uint8_t *tx, size_t len);

  void
  write(uint16_t addr, uint8_t start_addr, const uint8_t *tx, size_t len);

  const struct device *
  dev();

private:
  const struct device *m_i2c;
};
class adc_initialization_exception : public satnogs::comms::exception
{
public:
  adc_initialization_exception(string_type file_name, numeric_type line)
      : exception(file_name, line,
                  error_msg{satnogs::comms::exception::severity::MAJOR,
                            "Error in ADC initialization", "adcerror",
                            EADCINIT})
  {
  }
};

class adc_bsp : public satnogs::comms::bsp::adc
{
public:
  adc_bsp(const struct adc_dt_spec &adc_channel);

  void
  start();

  void
  stop();

  uint32_t
  read();

  float
  voltage();

private:
  const struct adc_dt_spec &m_adc_channel;
  uint16_t                  m_adc_sample;
  struct adc_sequence       m_sequence;
};

class dac_bsp : public satnogs::comms::bsp::dac
{
public:
  dac_bsp(const struct device *dac_dev, uint8_t channel, uint8_t resolution)
      : dac(resolution, 3.3f), m_dac(dac_dev), m_channel(channel)
  {
    const struct dac_channel_cfg dac_ch_cfg = {
        .channel_id = channel, .resolution = resolution, .buffered = true};
    dac_channel_setup(m_dac, &dac_ch_cfg);
    set_voltage(0.0f);
  }

  void
  start()
  {
  }
  void
  stop()
  {
    set_voltage(0.0f);
  }

protected:
  void
  write(uint32_t x)
  {
    dac_write_value(m_dac, m_channel, x);
  }

private:
  const struct device *m_dac;
  const uint8_t        m_channel;
};

template <typename T, const size_t LEN>
class msgq : public satnogs::comms::bsp::msgq<T, LEN>
{
public:
  msgq() : satnogs::comms::bsp::msgq<T, LEN>()
  {
    k_msgq_init(&m_mq, m_buffer, sizeof(T), LEN);
  }

  size_t
  size()
  {
    return k_msgq_num_used_get(&m_mq);
  }

  uint32_t
  free_space()
  {
    return k_msgq_num_free_get(&m_mq);
  }

  int
  put(const T &msg, size_t timeout_ms)
  {
    return k_msgq_put(&m_mq, &msg, K_MSEC(timeout_ms));
  }

  int
  put_isr(const T &msg)
  {
    return k_msgq_put(&m_mq, &msg, K_NO_WAIT);
  }

  int
  peek(T *msg)
  {
    return k_msgq_peek(&m_mq, msg);
  }

  void
  purge()
  {
    return k_msgq_purge(&m_mq);
  }

  int
  get(T *msg, size_t timeout_ms)
  {
    return k_msgq_get(&m_mq, msg, K_MSEC(timeout_ms));
  }

  int
  get_isr(T *msg)
  {
    return k_msgq_get(&m_mq, msg, K_NO_WAIT);
  }

private:
  struct k_msgq m_mq;
  char          __aligned(4) m_buffer[LEN * sizeof(T)];
};