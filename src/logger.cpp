/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "logger.hpp"
#include "bsp/bsp.hpp"
#include "callbacks.hpp"
#include "scoped_lock.hpp"
#include "version.hpp"
#include <cstring>
#include <etl/error_handler.h>
#include <etl/string_stream.h>
#include <satnogs-comms/board.hpp>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/retention/retention.h>
#include <zephyr/task_wdt/task_wdt.h>

namespace sc = satnogs::comms;

LOG_MODULE_REGISTER(satnogscomms, CONFIG_LOG_DEFAULT_LEVEL);

static const struct device *retention_memory_dev =
    DEVICE_DT_GET(DT_CHOSEN(zephyr_retention_memory));

etl::string<CONFIG_LOG_MAX_MSG_LEN> retentained_mem_str;

#if DT_NODE_EXISTS(DT_ALIAS(uartlog))
K_SEM_DEFINE(tx_done, 1, 1);
static __aligned(32) uint8_t dma_buf[CONFIG_LOG_MAX_MSG_LEN];

static void
uart_callback(const struct device *dev, struct uart_event *evt, void *user_data)
{
  ARG_UNUSED(dev);
  switch (evt->type) {
  case UART_TX_DONE:
  case UART_TX_ABORTED:
    k_sem_give(&tx_done);
    break;
  case UART_RX_RDY:
    break;
  case UART_RX_BUF_RELEASED:
    break;
  case UART_RX_BUF_REQUEST:
    break;
  case UART_RX_DISABLED:
    break;
  default:
    break;
  }
}
#endif

static auto log_str =
    etl::make_string_with_capacity<CONFIG_LOG_MAX_MSG_LEN>("");

/**
 * @brief Logs system initialization details to multiple targets (RTT, UART, and
 * RING_BUFFER).
 *
 * Gathers firmware, library, and hardware version information, constructs a
 * descriptive message, and logs it. This method is typically called once at
 * system startup.
 */
void
logger::boot()
{
  auto msg = etl::make_string_with_capacity<CONFIG_LOG_MAX_MSG_LEN>("");
  etl::string_stream stream(msg);
  stream << "SatNOGS-COMMS FW: " << satnogs::version::fw_major << "."
         << satnogs::version::fw_minor << "." << satnogs::version::fw_patch
         << ", LIB: " << sc::version::lib_major << "." << sc::version::lib_minor
         << "." << sc::version::lib_patch << ", HW: " << sc::version::hw_major
         << "." << sc::version::hw_minor << "." << sc::version::hw_patch
         << " initialized";
  scoped_lock lock(&m_mtx);
  log_unlocked({target::RTT, target::UART, target::RING_BUFFER}, msg.c_str());
}

/**
 * @brief Logs a SatNOGS-COMMS exception to all default targets (RTT, UART,
 * RING_BUFFER, EMMC). Additionally, the exceptions are saved to BACKUP_SRAM
 * for retention based on the severity level defined by
 * CONFIG_BACKUP_SRAM_EXCEPTION_LOG_LEVEL.
 *
 * @param e The @ref satnogs::comms::exception object containing error details.
 */
void
logger::log(const sc::exception &e)
{
  backup_sram_push(e);
  log({target::RTT, target::RING_BUFFER, target::UART, target::EMMC}, e);
}

/**
 * @brief Logs a std::exception to all default targets (RTT, UART,
 * RING_BUFFER, EMMC). Additionally, the std::exceptions are saved to
 * BACKUP_SRAM for retention.
 *
 * @param e The standard exception object containing error details.
 */
void
logger::log(const std::exception &e)
{
  backup_sram_push(e);
  log({target::RTT, target::RING_BUFFER, target::UART, target::EMMC}, e);
}

/**
 * @brief Logs a C-string message to all default targets (RTT, UART,
 * RING_BUFFER, EMMC).
 *
 * @param msg The null-terminated string to be logged.
 */
void
logger::log(const char *msg)
{
  log({target::RTT, target::RING_BUFFER, target::UART, target::EMMC}, msg);
}

/**
 * @brief Logs an ETL istring message to all default targets.
 *
 * @param msg The etl::istring object whose contents will be logged.
 */
void
logger::log(const etl::istring &msg)
{
  log({target::RTT, target::RING_BUFFER, target::UART, target::EMMC},
      msg.c_str());
}

/**
 * @brief Logs a satnogs::comms::exception to a specified set of targets.
 *
 * This method provides control over which outputs receive the log
 * message.
 *
 * @param list An initializer list specifying the desired logging targets.
 * @param e The @ref satnogs::comms::exception object to be logged.
 */
void
logger::log(std::initializer_list<target> list, const sc::exception &e)
{
  backup_sram_push(e);
  scoped_lock lock(&m_mtx);
  log_unlocked(list, e);
}
/**
 * @brief Logs a std::exception to specified targets.
 *
 * This method provides control over which outputs receive the log
 * message.
 *
 * @param list An initializer list specifying the desired logging targets.
 * @param e The standard exception object containing error details.
 */
void
logger::log(std::initializer_list<target> list, const std::exception &e)
{
  backup_sram_push(e);
  scoped_lock lock(&m_mtx);
  log_unlocked(list, e);
}

/**
 * @brief Logs a C-string message to specified targets.
 *
 * This method provides control over which outputs receive the log
 * message.
 *
 * @param list An initializer list specifying the desired logging targets.
 * @param msg The null-terminated string to be logged.
 */
void
logger::log(std::initializer_list<target> list, const char *msg)
{
  scoped_lock lock(&m_mtx);
  log_unlocked(list, msg);
}

/**
 * @brief Sends a satnogs::comms::exception message to the RTT interface.
 *
 * Uses the Zephyr logging subsystem to output exception details as an error
 * log.
 *
 * @param e The @ref satnogs::comms::exception object containing error details.
 */
void
logger::rtt_push(const satnogs::comms::exception &e)
{
  LOG_ERR("Exception: %s | Severity: %d", e.what_verbose(),
          static_cast<uint8_t>(e.get_severity()));
}

/**
 * @brief Sends a std::exception message to the RTT interface.
 *
 * Outputs the exception details as an error log, utilizing the Zephyr logging
 * system.
 *
 * @param e The standard exception object containing error details.
 */
void
logger::rtt_push(const std::exception &e)
{
  LOG_ERR("Exception: %s", e.what());
}

/**
 * @brief Sends a C-string message to the RTT interface.
 *
 * Uses the Zephyr logging subsystem to print informational messages.
 *
 * @param msg The null-terminated string to be logged.
 */
void
logger::rtt_push(const char *msg)
{
  LOG_INF("%s", msg);
}

/**
 * @brief Stores a satnogs::comms::exception message in an ETL ring buffer.
 *
 * If the buffer is full, the oldest entry is removed.
 *
 * @param e The @ref satnogs::comms::exception object containing error details.
 */
void
logger::ring_buffer_push(const satnogs::comms::exception &e)
{
  log_str.clear();
  log_str.append("EXC: ");
  /* Using the ETL we are safe from a possible buffer overflow */
  log_str.append(e.what_terse());
  if (m_ring_buffer.full() == true) {
    m_ring_buffer.pop();
  }
  m_ring_buffer.push(log_str);
}

/**
 * @brief Stores a std::exception message in an ETL ring buffer.
 *
 * If the buffer is full, the oldest entry is removed.
 *
 * @param e The standard exception object containing error details.
 */
void
logger::ring_buffer_push(const std::exception &e)
{
  log_str.clear();
  log_str.append("EXC: ");
  /* Using the ETL we are safe from a possible buffer overflow */
  log_str.append(e.what());
  if (m_ring_buffer.full() == true) {
    m_ring_buffer.pop();
  }
  m_ring_buffer.push(log_str);
}

/**
 * @brief Stores a C-string message in the ring buffer.
 *
 * Adds a "LOG: " prefix to the message and inserts it into the ring buffer. If
 * the buffer is full, the oldest message is dropped, ensuring that recent
 * messages are always maintained.
 *
 * @param msg The null-terminated string to be logged.
 */
void
logger::ring_buffer_push(const char *msg)
{
  log_str.clear();
  log_str.append("LOG: ");
  /* Using the ETL we are safe from a possible buffer overflow */
  log_str.append(msg);
  if (m_ring_buffer.full() == true) {
    m_ring_buffer.pop();
  }
  m_ring_buffer.push(log_str);
}

/**
 * @brief Sends a @ref satnogs::comms::exception message over UART DMA.
 *
 * The message is prepared with an "EXC: " prefix. If the previous transmission
 * is not yet finished, the method waits for a 400 ms, and then
 * aborts.
 *
 * @warning In order to use this method one must provide a log uart overlay when
 * building the project, otherwise the invokation of this method will cause no
 * effect.
 *
 * @param e The @ref satnogs::comms::exception object containing error details.
 */
void
logger::uart_push(const satnogs::comms::exception &e)
{
#if DT_NODE_EXISTS(DT_ALIAS(uartlog))
  log_str.clear();
  log_str.append("EXC: ");
  log_str.append(e.what());
  log_str.append("\r\n");
  const struct device *uart_log = DEVICE_DT_GET(DT_ALIAS(uartlog));
  /*
   * Wait for the previous transfer to finish. If this is not possible just
   * abort
   */
  if (k_sem_take(&tx_done, K_MSEC(400))) {
    uart_tx_abort(uart_log);
    return;
  }
  memcpy(dma_buf, reinterpret_cast<const uint8_t *>(log_str.c_str()),
         log_str.size());
  uart_tx(uart_log, dma_buf, log_str.size(), SYS_FOREVER_US);
#endif
}

/**
 * @brief Sends a std::exception message over UART DMA.
 *
 * The message is prepared with an "EXC: " prefix.
 *
 * @warning In order to use this method one must provide a log uart overlay when
 * building the project, therwise the invokation of this method will cause no
 * effect.
 *
 * @param e The standard exception object containing error details.
 */
void
logger::uart_push(const std::exception &e)
{
#if DT_NODE_EXISTS(DT_ALIAS(uartlog))
  log_str.clear();
  log_str.append("EXC: ");
  log_str.append(e.what());
  const struct device *uart_log = DEVICE_DT_GET(DT_ALIAS(uartlog));
  uart_tx(uart_log, reinterpret_cast<const uint8_t *>(log_str.c_str()),
          log_str.size(), 100000);
#endif
}

/**
 * @brief Transmits a C-string message over UART DMA.
 *
 * The message is prepared with an "LOG: " prefix.
 *
 * @warning In order to use this method one must provide a log uart overlay when
 * building the project, therwise the invokation of this method will cause no
 * effect.
 *
 * @param msg The null-terminated string to be logged.
 */
void
logger::uart_push(const char *msg)
{
#if DT_NODE_EXISTS(DT_ALIAS(uartlog))
  log_str.clear();
  log_str.append("LOG: ");
  log_str.append(msg);
  log_str.append("\r\n");
  const struct device *uart_log = DEVICE_DT_GET(DT_ALIAS(uartlog));

  /*
   * Wait for the previous transfer to finish. If this is not possible just
   * abort
   */
  if (k_sem_take(&tx_done, K_MSEC(400))) {
    uart_tx_abort(uart_log);
    return;
  }
  memcpy(dma_buf, reinterpret_cast<const uint8_t *>(log_str.c_str()),
         log_str.size());
  uart_tx(uart_log, dma_buf, log_str.size(), SYS_FOREVER_US);
#endif
}

/**
 * @brief Stores a satnogs::comms::exception verbose message C-style string in
 * BACKUP_SRAM
 *
 * The satnogs::comms::exception will be logged based on the severity level
 * defined by CONFIG_BACKUP_SRAM_EXCEPTION_LOG_LEVEL.
 *
 * @param e The @ref satnogs::comms::exception object containing error details.
 * @retval 0		If successful.
 * @retval -errno	Error code code.
 */
int
logger::backup_sram_push(const satnogs::comms::exception &e)
{
  if (static_cast<uint8_t>(e.get_severity()) <=
      CONFIG_BACKUP_SRAM_EXCEPTION_LOG_LEVEL) {
    return retention_write(retention_memory_dev, 0U,
                           reinterpret_cast<const uint8_t *>(e.what_verbose()),
                           CONFIG_LOG_MAX_MSG_LEN);
  }
  return -1;
}

/**
 * @brief Stores a std::exception message C-style string in
 * BACKUP_SRAM.
 *
 * @param e The standard exception object containing error details.
 * @retval 0		If successful.
 * @retval -errno	Error code code.
 */
int
logger::backup_sram_push(const std::exception &e)
{
  return retention_write(retention_memory_dev, 0U,
                         reinterpret_cast<const uint8_t *>(e.what()),
                         CONFIG_LOG_MAX_MSG_LEN);
}

/**
 * @brief Retrieves the latest exception message stored in BACKUP_SRAM.
 *
 * @return A C-style string containing the latest exception message or a
 *         default message if no exception has been logged.
 */
etl::string<CONFIG_LOG_MAX_MSG_LEN>
logger::get_latest_exception() const
{
  if (retention_is_valid(retention_memory_dev) != 1) {
    return "Data in BACKUP SRAM is not valid";
  }
  if (retention_read(retention_memory_dev, 0U,
                     reinterpret_cast<uint8_t *>(retentained_mem_str.data()),
                     CONFIG_LOG_MAX_MSG_LEN) != 0) {
    return "Error while reading data from BACKUP SRAM device";
  }
  return retentained_mem_str.data();
}

/**
 * @brief Constructs the logger instance.
 *
 * Initializes the mutex for thread-safety. If UART logging is available,
 * configures the UART interface, disables RX, aborts any pending TX, and sets a
 * DMA-based UART callback.
 *
 * @warning This constructor is exclusively invoked by @ref
 * logger::get_instance(), as the @ref logger class follows the singleton
 * design pattern. To obtain an instance of the @ref logger class, one
 * must call @ref logger::get_instance().
 */
logger::logger()
{
  k_mutex_init(&m_mtx);
#if DT_NODE_EXISTS(DT_ALIAS(uartlog))
  const struct device *uart_log = DEVICE_DT_GET(DT_ALIAS(uartlog));
  struct uart_config   uart_cfg = {
        .baudrate  = CONFIG_LOG_UART_BAUDRATE,
        .parity    = UART_CFG_PARITY_NONE,
        .stop_bits = UART_CFG_STOP_BITS_1,
        .data_bits = UART_CFG_DATA_BITS_8,
        .flow_ctrl = UART_CFG_FLOW_CTRL_NONE,
  };
  uart_rx_disable(uart_log);
  uart_tx_abort(uart_log);
  uart_configure(uart_log, &uart_cfg);
  uart_callback_set(uart_log, uart_callback, nullptr);
#endif
}
