/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include "scoped_lock.hpp"
#include "tests/test.hpp"
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <zephyr/kernel.h>

/**
 * @class msg_arbiter
 * @brief Incoming/Outgoing Message Arbiter
 *
 * The `msg_arbiter` class is responsible for:
 *
 * - **Receiving messages:** It handles incoming messages from various
 * configured input interfaces.
 * - **Message queuing:** It maintains a thread-safe message queue for received
 * frames and individual queues for each output interface.
 * - **Message processing:** It processes valid telecommands:
 *    - **Local processing:** Handles telecommands specific to the SatNOGS-COMMS
 * subsystem.
 *    - **Forwarding:** Forwards telecommands to the appropriate output
 * interface for delivery to target subsystems within the satellite.
 *
 *
 * **Usage:**
 *
 * To use the `msg_arbiter` class:
 * 1. **Instantiate:** Create an instance of the class. The class provides
 * singleton access, via the get_instance() method.
 * 2. **Start:** Begin the message processing loop by calling the start()
 * method.
 * 3. **Push messages:** Use the `push()` method to enqueue received messages
 * from any of the available IO interfaces.
 * 4. **Pull messages:** Messages for specific interfaces can be pulled using
 * the pull() method.
 *
 * @note Each output interface has a dedicated message queue. A pull() call that
 * may block for a specific interface, does not block the other available.
 *
 * @warning The start() method should be called **once** as early as possible in
 * the firmware. Multiple calls of start() may result to undefined behavior.
 *
 * @mermaid{msg_arbiter}
 */
class msg_arbiter
{
public:
  static constexpr size_t mtu                = CONFIG_MAX_MTU;
  static constexpr size_t rx_msgq_size       = CONFIG_RX_MSGQ_SIZE;
  static constexpr size_t can1_tx_msgq_size  = CONFIG_CAN1_TX_MSGQ_SIZE;
  static constexpr size_t uhf_tx_msgq_size   = CONFIG_UHF_TX_MSGQ_SIZE;
  static constexpr size_t sband_tx_msgq_size = CONFIG_SBAND_TX_MSGQ_SIZE;

  /**
   * @brief Singleton access to a unique and global \ref msg_arbiter instance.
   *
   * @note The start() method should be called **once** for the full
   * functionality of this subsystem
   *
   * @return msg_arbiter& a unique and global \ref msg_arbiter instance
   */
  static msg_arbiter &
  get_instance()
  {
    static msg_arbiter instance;
    return instance;
  }

  /**
   * @brief Peripheral identifier
   *
   * SatNOGS-COMMS has several peripherals that can be configured as
   * input/output interfaces. Each \ref msg contains the IO interface that was
   * received.
   */
  enum class subsys : uint8_t
  {
    CAN1        = 0,
    CAN2        = 1,
    RADIO_UHF   = 2,
    RADIO_SBAND = 3,
    SPI         = 4,
    UART_PC104  = 5
  };

  /**
   * Message format that are used for internal routing
   *
   */
  class msg
  {
  public:
    subsys  iface;     //!< The interface from which the msg was received
    uint8_t data[mtu]; //!< Buffer to hold the data
    size_t  len;       //!< Data size in bytes
  };

  /* Singleton */
  msg_arbiter(msg_arbiter const &) = delete;

  void
  operator=(msg_arbiter const &) = delete;

  void
  push(const msg *m, bool block = false);

  int
  pull(msg *m, subsys s, k_timeout_t wait);

  int
  fwd(msg *m, subsys s, k_timeout_t wait);

  void
  start();

private:
  static void
  parse_thread(void *arg1, void *arg2, void *arg3);

  msg_arbiter();

  void
  send_tx_msgq(subsys iface, const msg_arbiter::msg &m);

  msg_arbiter::msg m_msg;
  k_tid_t          m_tid;
  struct k_mutex   m_mtx;
};
