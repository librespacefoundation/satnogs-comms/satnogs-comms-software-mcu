/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2025, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include "bsp/bsp.hpp"
#include "callbacks.hpp"
#include "error_handler.hpp"
#include "io.hpp"
#include "io_wdg.hpp"
#include "mission.hpp"
#include "msg_arbiter.hpp"
#include "overlays.hpp"
#include "settings.hpp"
#include "telemetry.hpp"
#include "tests/test.hpp"
#include "time.hpp"

#include "etl_profile.h"
#include "logger.hpp"

#include <errno.h>
#include <satnogs-comms/board.hpp>
#include <zephyr/devicetree.h>
#include <zephyr/kernel.h>
#include <zephyr/stats/stats.h>
#include <zephyr/sys/reboot.h>
#include <zephyr/sys/util.h>
#include <zephyr/task_wdt/task_wdt.h>

/**
 * @brief Firmware initialization helper class
 *
 * This class provides a safe and convenient way to perform the initial
 * configuration and initialization of all the underlying subsystems of the
 * transceiver. The class provides two different stages, the prepare() and the
 * start() respectively. Between them, users may apply any kind of mission
 * specific operations that may be required.
 *
 * @attention instantiating an object of the class using the singleton
 * get_instance() engages the hardware watchdog and initiates the Task Watchdog
 * of the Zephyr-RTOS. It is crucial that the instantiation of this class is
 * performed as early as possible to ensure reliable operation.
 */
class startup
{
public:
  /**
   * @brief Get a singleton access to the startup subsystem.
   *
   * At the first instantiation of the class the hardware watchdog is engaged
   * and the Task Watchdog of the Zephyr-RTOS is initiated.
   *
   * @return startup& singleton access to the startup subsystem
   */
  static startup &
  get_instance()
  {
    static startup instance;
    return instance;
  }
  /* Singleton */
  startup(startup const &) = delete;
  void
  operator=(startup const &) = delete;

  void
  prepare();

  void
  start();

  int
  wdg_id() const;

private:
  int m_wdgid;

  startup();
};
