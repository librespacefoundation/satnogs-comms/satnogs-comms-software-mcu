/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "msg_arbiter.hpp"
#include "callbacks.hpp"
#include "error_handler.hpp"
#include "io_wdg.hpp"
#include "settings.hpp"
#include "telecommand.hpp"
#include "telemetry.hpp"
#include "tests/test.hpp"
#include "time.hpp"
#include <satnogs-comms/version.hpp>
#include <string>
#include <tinycrypt/sha256.h>
#include <zephyr/task_wdt/task_wdt.h>

static constexpr std::array<uint8_t, 32>
sha256_key_hex2bytes()
{
  std::array<uint8_t, 32> key = {0};
  static_assert(std::char_traits<char>::length(CONFIG_TLC_SHA256_KEY) == 64,
                "Hash should be exactly 64 hex annotated chars");
  for (unsigned int i = 0; i < 64; i += 2) {
    char s[2]  = {CONFIG_TLC_SHA256_KEY[i], CONFIG_TLC_SHA256_KEY[i + 1]};
    char byte  = (char)strtol(s, NULL, 16);
    key[i / 2] = byte;
  }
  return key;
}

static struct k_thread msg_arbiter_thread_data;
K_THREAD_STACK_DEFINE(msg_arbiter_thread_stack, CONFIG_MSG_ARBITER_STACK_SIZE);

static char rx_msgq_buffer[CONFIG_RX_MSGQ_SIZE * sizeof(msg_arbiter::msg)];
static struct k_msgq rx_msgq;

static char
    can1_tx_msgq_buffer[CONFIG_CAN1_TX_MSGQ_SIZE * sizeof(msg_arbiter::msg)];
static struct k_msgq can1_tx_msgq;

static char
    uhf_tx_msgq_buffer[CONFIG_UHF_TX_MSGQ_SIZE * sizeof(msg_arbiter::msg)];
static struct k_msgq uhf_tx_msgq;

static char
    sband_tx_msgq_buffer[CONFIG_SBAND_TX_MSGQ_SIZE * sizeof(msg_arbiter::msg)];
static struct k_msgq sband_tx_msgq;

namespace sc = satnogs::comms;

static const auto                   &tlc_key = sha256_key_hex2bytes();
static struct tc_sha256_state_struct sha256_state;
static std::array<uint8_t, 32>       sha256_comp;

/**
 * @brief Push a received message for processing
 *
 * @param m the received message
 * @param block if set to true, this call may block until there is space in the
 * message queue. If set to false, the message queue is purged so the new
 * message can be pushed
 *
 * @todo Probably a better policy can be applied instead of purging
 */
void
msg_arbiter::push(const msg *m, bool block)
{
  if (block) {
    k_msgq_put(&rx_msgq, m, K_FOREVER);
  } else {
    while (k_msgq_put(&rx_msgq, m, K_NO_WAIT) != 0) {
      k_msgq_purge(&rx_msgq);
    }
  }
}

/**
 * @brief Pulls a message for a specific IO interface
 *
 * @param m pointer to hold the message. It is responsibility of the caller to
 * allocate memory before hand
 * @param s the target IO interface
 * @param wait time to wait if no messages are available
 * @return int 0 on success, negative error code on timeout of failure
 */
int
msg_arbiter::pull(msg *m, subsys s, k_timeout_t wait)
{
  switch (s) {
  case subsys::CAN1:
    return k_msgq_get(&can1_tx_msgq, m, wait);
  case subsys::RADIO_UHF:
    return k_msgq_get(&uhf_tx_msgq, m, wait);
  case subsys::RADIO_SBAND:
    return k_msgq_get(&sband_tx_msgq, m, wait);
  default:
    return -EINVAL;
  }
}

int
msg_arbiter::fwd(msg *m, subsys s, k_timeout_t wait)
{
  switch (s) {
  case subsys::CAN1:
    return k_msgq_put(&can1_tx_msgq, m, wait);
  case subsys::RADIO_UHF:
    return k_msgq_put(&uhf_tx_msgq, m, wait);
  case subsys::RADIO_SBAND:
    return k_msgq_put(&sband_tx_msgq, m, wait);
  default:
    return -EINVAL;
  }
}

/**
 * @brief Starts the processing task for incoming messages handling
 *
 * @warning This method should be called only once
 */
void
msg_arbiter::start()
{
  m_tid = k_thread_create(&msg_arbiter_thread_data, msg_arbiter_thread_stack,
                          K_THREAD_STACK_SIZEOF(msg_arbiter_thread_stack),
                          parse_thread, NULL, NULL, NULL,
                          CONFIG_MSG_ARBITER_PRIO, 0, K_NO_WAIT);
  if (!m_tid) {
    k_oops();
  }
  k_thread_name_set(m_tid, "msg_arbiter");
}

void
msg_arbiter::parse_thread(void *arg1, void *arg2, void *arg3)
{
  int          task_wdt_id = task_wdt_add(CONFIG_WATCHDOG_PERIOD_MSG_ARBITER,
                                          task_wdt_callback, (void *)k_current_get());
  msg_arbiter &arb         = msg_arbiter::get_instance();
  telecommand &tc          = telecommand::get_instance();

  while (1) {
    try {
      task_wdt_feed(task_wdt_id);
      if (k_msgq_get(&rx_msgq, &arb.m_msg, K_MSEC(20)) == 0) {
        if (tc.ccsds_frame_valid(arb.m_msg)) {
          /* Decoding with protobuf failed, decode with CCSDS XTCE */
          if (tc.decode_ccsds_xtce(arb.m_msg)) {
            arb.send_tx_msgq(arb.m_msg.iface, arb.m_msg);
          }
        } else {
          continue;
        }
      }
    } catch (const sc::exception &e) {
      auto &err = error_handler::get_instance();
      err.handle(e);
    } catch (const std::exception &e) {
      auto &err = error_handler::get_instance();
      err.handle(e);
    }
  }
}

msg_arbiter::msg_arbiter()
{
  k_msgq_init(&rx_msgq, rx_msgq_buffer, sizeof(msg_arbiter::msg), rx_msgq_size);
  k_msgq_init(&can1_tx_msgq, can1_tx_msgq_buffer, sizeof(msg_arbiter::msg),
              can1_tx_msgq_size);
  k_msgq_init(&uhf_tx_msgq, uhf_tx_msgq_buffer, sizeof(msg_arbiter::msg),
              uhf_tx_msgq_size);
  k_msgq_init(&sband_tx_msgq, sband_tx_msgq_buffer, sizeof(msg_arbiter::msg),
              sband_tx_msgq_size);
  k_mutex_init(&m_mtx);
}

void
msg_arbiter::send_tx_msgq(subsys iface, const msg_arbiter::msg &m)
{
  // FIXME interface handling needs revising
  switch (iface) {
  case subsys::CAN1:
    k_msgq_put(&can1_tx_msgq, &m, K_FOREVER);
    break;
  case subsys::RADIO_UHF:
    k_msgq_put(&uhf_tx_msgq, &m, K_FOREVER);
    break;
  case subsys::RADIO_SBAND:
    k_msgq_put(&sband_tx_msgq, &m, K_FOREVER);
    break;
  default:
    return;
  }
}
