#include "mission.hpp"

/**
 * @brief This method is called before the satnogs::comms::board::init()
 *
 */
void
mission::pre_init() const
{
  m_pre_init.call_if();
}

/**
 * @brief Registers a callback for the pre_init()
 *
 * @param callback a valid callback
 */
void
mission::register_pre_init(etl::delegate<void(void)> callback)
{
  m_pre_init = callback;
}

/**
 * @brief This method is called after the satnogs::comms::board::init()
 *
 */
void
mission::post_init() const
{
  m_post_init.call_if();
}

/**
 * @brief Registers a callback for the post_init()
 *
 * @param callback a valid callback
 */
void
mission::register_post_init(etl::delegate<void(void)> callback)
{
  m_post_init = callback;
}

/**
 * @brief This method is called at the end of the startup::start()
 *
 */
void
mission::start() const
{
  m_start.call_if();
}

/**
 * @brief Registers a callback for the start()
 *
 * @param callback a valid callback
 */
void
mission::register_start(etl::delegate<void(void)> callback)
{
  m_start = callback;
}
