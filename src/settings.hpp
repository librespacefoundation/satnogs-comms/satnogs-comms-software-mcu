/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024-2025, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once
#include <type_traits>
#include <variant>

#include "scoped_lock.hpp"
#include <satnogs-comms/radio.hpp>
#include <zephyr/kernel.h>

/**
 * @brief Store and retrieve reliably persistent settings
 *
 * The \ref settings subsystem utilizes LittleFS to store reliably all the
 * settings that should be persistent across power cycles.
 * It uses the internal flash of the MCU.
 *
 * Access to the subsystem itself is achieved using the singleton design
 * pattern. All read or write operations are thread-safe.
 *
 * To add a new setting, first a new file should be defined.
 * All files should defined under the `/lfs1` directory.
 * In addition, a default setting value should be set, so it can be applied in
 * case the flash area is formatted or has been previously erased.
 *
 * @note Using the `CONFIG_ERASE_SETTINGS` KConfig option, users can wipe the
 * entire flash region used by the \ref settings subsystem. This can be useful
 * for testing and preparation of the flight software.
 */
class settings
{
public:
  /**
   * @brief Specific setting identifier
   *
   * To efficiently discriminate between the available settings parameters,
   * indexing is performed via this enumeration
   */
  enum class param : uint16_t
  {
    BOOT_COUNT,
    UHF_TX_FREQ,
    UHF_RX_FREQ,
    SBAND_TX_FREQ,
    SBAND_RX_FREQ,
    UHF_TX_EN,
    SBAND_TX_EN,
    UHF_TX_GAIN,
    SBAND_TX_GAIN,
    IO_WDG_PERIOD_MINS,
    SBAND_RX_RANGE_MIN,
    SBAND_RX_RANGE_MAX,
    SBAND_TX_RANGE_MIN,
    SBAND_TX_RANGE_MAX,
    SBAND_AGC0_RANGE_MIN,
    SBAND_AGC0_RANGE_MAX,
    SBAND_GAIN0_RANGE_MIN,
    SBAND_GAIN0_RANGE_MAX,
    SBAND_AGC0_CALIB_SLOPE,
    SBAND_AGC0_CALIB_INTRCPT,
    SBAND_GAIN0_CALIB_SLOPE,
    SBAND_GAIN0_CALIB_INTRCPT,
    UHF_RX_RANGE_MIN,
    UHF_RX_RANGE_MAX,
    UHF_TX_RANGE_MIN,
    UHF_TX_RANGE_MAX,
    UHF_AGC0_RANGE_MIN,
    UHF_AGC0_RANGE_MAX,
    UHF_GAIN0_RANGE_MIN,
    UHF_GAIN0_RANGE_MAX,
    UHF_AGC0_CALIB_SLOPE,
    UHF_AGC0_CALIB_INTRCPT,
    UHF_GAIN0_CALIB_SLOPE,
    UHF_GAIN0_CALIB_INTRCPT
  };

  /**
   * @brief Get a singleton access to the settings subsystem
   *
   * @return settings& singleton access to the settings subsystem
   */
  static settings &
  get_instance()
  {
    static settings instance;
    return instance;
  }
  /* Singleton */
  settings(settings const &) = delete;
  void
  operator=(settings const &) = delete;

  void
  incr_boot_cnt();
  bool
  deployed();

  template <param P>
  static constexpr auto
  defaults()
  {
    static_assert(static_cast<uint32_t>(P) <
                      (sizeof(params) / sizeof(param_info)),
                  "Invalid enumeration");

    /* Ensure that the specified param matches the table  */
    static_assert(P == params[static_cast<uint32_t>(P)].assoc_param,
                  "Params configuration contains error");

    if constexpr (std::holds_alternative<uint32_t>(
                      params[static_cast<uint32_t>(P)].default_value)) {
      return std::get<uint32_t>(params[static_cast<uint32_t>(P)].default_value);
    } else if constexpr (std::holds_alternative<float>(
                             params[static_cast<uint32_t>(P)].default_value)) {
      return std::get<float>(params[static_cast<uint32_t>(P)].default_value);
    } else if constexpr (std::holds_alternative<bool>(
                             params[static_cast<uint32_t>(P)].default_value)) {
      return std::get<bool>(params[static_cast<uint32_t>(P)].default_value);
    } else {
      static_assert("Invalid std::variant evaluation. Please ask for support "
                    "and raise an issue at "
                    "https://gitlab.com/librespacefoundation/satnogs-comms/"
                    "satnogs-comms-software-mcu/-/issues");
    }
  }

  template <param P, typename T>
  void
  set(T x)
  {
    static_assert(std::holds_alternative<T>(
                      params[static_cast<uint32_t>(P)].default_value),
                  "Parameter value type and specified type do not match");
    scoped_lock lock(&m_mtx);
    if constexpr (std::holds_alternative<float>(
                      params[static_cast<uint32_t>(P)].default_value)) {
      writef(params[static_cast<uint32_t>(P)].path, x);
    } else if constexpr (std::holds_alternative<uint32_t>(
                             params[static_cast<uint32_t>(P)].default_value)) {
      write32(params[static_cast<uint32_t>(P)].path, x);
    } else if constexpr (std::holds_alternative<bool>(
                             params[static_cast<uint32_t>(P)].default_value)) {
      write32(params[static_cast<uint32_t>(P)].path, x);
    } else {
      static_assert("Invalid std::variant evaluation. Please ask for support "
                    "and raise an issue at "
                    "https://gitlab.com/librespacefoundation/satnogs-comms/"
                    "satnogs-comms-software-mcu/-/issues");
    }
  }

  template <param P>
  void
  reset()
  {
    set<P>(defaults<P>());
  }

  template <param P>
  auto
  get()
  {
    static_assert(static_cast<uint32_t>(P) <
                      (sizeof(params) / sizeof(param_info)),
                  "Invalid enumeration");
    /* Ensure that the specified param matches the table  */
    static_assert(P == params[static_cast<uint32_t>(P)].assoc_param,
                  "Params configuration contains error");

    scoped_lock           lock(&m_mtx);
    constexpr const auto &info = params[static_cast<uint32_t>(P)];
    if constexpr (std::holds_alternative<float>(
                      params[static_cast<uint32_t>(P)].default_value)) {
      float val;
      if (readf(info.path, &val) == -ENOENT) {
        reset<P>();
        return defaults<P>();
      }
      return val;
    } else if constexpr (std::holds_alternative<uint32_t>(
                             params[static_cast<uint32_t>(P)].default_value)) {
      uint32_t val;
      if (read32(info.path, &val) == -ENOENT) {
        reset<P>();
        return defaults<P>();
      }
      return val;
    } else if constexpr (std::holds_alternative<bool>(
                             params[static_cast<uint32_t>(P)].default_value)) {
      uint32_t val;
      if (read32(info.path, &val) == -ENOENT) {
        reset<P>();
        return defaults<P>();
      }
      return val == 1;
    } else {
      static_assert("Invalid std::variant evaluation. Please ask for support "
                    "and raise an issue at "
                    "https://gitlab.com/librespacefoundation/satnogs-comms/"
                    "satnogs-comms-software-mcu/-/issues");
    }
  }

protected:
  settings();

private:
  mutable struct k_mutex m_mtx;

  struct param_info
  {
    const char *path; //!< LitleFS path for the setting
    std::variant<float, uint32_t, bool>
        default_value; //!< Default value applied when no stored information is
                       //!< available
    param assoc_param; //!< The associated \ref param. Thus us needed to perform
                       //!< compile time checks ensuring 1-to-1 matching between
                       //!< the settings_param array indexes and the \ref param
                       //!< enumeration
  };

  static constexpr const param_info params[] = {
      // BOOT_COUNT
      {"/lfs1/boot_cnt", 0U, param::BOOT_COUNT},
      // Frequencies
      {"/lfs1/uhf-tx-freq", static_cast<float>(CONFIG_UHF_TX_FREQ_HZ),
       param::UHF_TX_FREQ},
      {"/lfs1/uhf-rx-freq", static_cast<float>(CONFIG_UHF_RX_FREQ_HZ),
       param::UHF_RX_FREQ},
      {"/lfs1/sband-tx-freq", static_cast<float>(CONFIG_SBAND_TX_FREQ_HZ),
       param::SBAND_TX_FREQ},
      {"/lfs1/sband-rx-freq", static_cast<float>(CONFIG_SBAND_RX_FREQ_HZ),
       param::SBAND_RX_FREQ},
      // TX enable flags
      {"/lfs1/uhf-tx-en", true, param::UHF_TX_EN},
      {"/lfs1/sband-tx-en", true, param::SBAND_TX_EN},
      // TX gains
      {"/lfs1/uhf-tx-gain", static_cast<float>(CONFIG_UHF_TX_GAIN),
       param::UHF_TX_GAIN},
      {"/lfs1/sband-tx-gain", static_cast<float>(CONFIG_SBAND_TX_GAIN),
       param::SBAND_TX_GAIN},
      // IO watchdog period
      {"/lfs1/io-wdg-period-min",
       static_cast<uint32_t>(CONFIG_IO_WDG_PERIOD_MINS),
       param::IO_WDG_PERIOD_MINS},

      // S-band RX range
      {"/lfs1/sband-rx_range_min",
       static_cast<float>(CONFIG_SBAND_RX_RANGE_MIN_HZ),
       param::SBAND_RX_RANGE_MIN},
      {"/lfs1/sband-rx_range_max",
       static_cast<float>(CONFIG_SBAND_RX_RANGE_MAX_HZ),
       param::SBAND_RX_RANGE_MAX},
      // S-band TX range
      {"/lfs1/sband-tx_range_min",
       static_cast<float>(CONFIG_SBAND_TX_RANGE_MIN_HZ),
       param::SBAND_TX_RANGE_MIN},
      {"/lfs1/sband-tx_range_max",
       static_cast<float>(CONFIG_SBAND_TX_RANGE_MAX_HZ),
       param::SBAND_TX_RANGE_MAX},
      // S-band AGC0 range
      {"/lfs1/sband-agc0_range_min", -57.0f, param::SBAND_AGC0_RANGE_MIN},
      {"/lfs1/sband-agc0_range_max", -31.0f, param::SBAND_AGC0_RANGE_MAX},
      // S-band gain0 range
      {"/lfs1/sband-gain0_range_min", -22.0f, param::SBAND_GAIN0_RANGE_MIN},
      {"/lfs1/sband-gain0_range_max", 19.0f, param::SBAND_GAIN0_RANGE_MAX},
      // S-band AGC0 calib
      {"/lfs1/sband-agc0_calib_slope", -0.022f, param::SBAND_AGC0_CALIB_SLOPE},
      {"/lfs1/sband-agc0_calib_intrcpt", 0.1049f,
       param::SBAND_AGC0_CALIB_INTRCPT},
      // S-band gain0 calib
      {"/lfs1/sband-gain0_calib_slope", 0.01871f,
       param::SBAND_GAIN0_CALIB_SLOPE},
      {"/lfs1/sband-gain0_calib_intrcpt", 1.016f,
       param::SBAND_GAIN0_CALIB_INTRCPT},

      // UHF RX range
      {"/lfs1/uhf-rx_range_min", static_cast<float>(CONFIG_UHF_RX_RANGE_MIN_HZ),
       param::UHF_RX_RANGE_MIN},
      {"/lfs1/uhf-rx_range_max", static_cast<float>(CONFIG_UHF_RX_RANGE_MAX_HZ),
       param::UHF_RX_RANGE_MAX},
      // UHF TX range
      {"/lfs1/uhf-tx_range_min", static_cast<float>(CONFIG_UHF_TX_RANGE_MIN_HZ),
       param::UHF_TX_RANGE_MIN},
      {"/lfs1/uhf-tx_range_max", static_cast<float>(CONFIG_UHF_TX_RANGE_MAX_HZ),
       param::UHF_TX_RANGE_MAX},
      // UHF AGC0 range
      {"/lfs1/uhf-agc0_range_min", -60.0f, param::UHF_AGC0_RANGE_MIN},
      {"/lfs1/uhf-agc0_range_max", -35.0f, param::UHF_AGC0_RANGE_MAX},
      // UHF gain0 range
      {"/lfs1/uhf-gain0_range_min", -6.0f, param::UHF_GAIN0_RANGE_MIN},
      {"/lfs1/uhf-gain0_range_max", 30.0f, param::UHF_GAIN0_RANGE_MAX},
      // UHF AGC0 calib
      {"/lfs1/uhf-agc0_calib_slope", -0.02141f, param::UHF_AGC0_CALIB_SLOPE},
      {"/lfs1/uhf-agc0_calib_intrcpt", 0.663199f,
       param::UHF_AGC0_CALIB_INTRCPT},
      // UHF gain0 calib
      {"/lfs1/uhf-gain0_calib_slope", 0.021854f, param::UHF_GAIN0_CALIB_SLOPE},
      {"/lfs1/uhf-gain0_calib_intrcpt", 0.7547525f,
       param::UHF_GAIN0_CALIB_INTRCPT}};

  int
  read32(const char *fname, uint32_t *x);

  int
  write32(const char *fname, uint32_t x);

  int
  readf(const char *fname, float *x);

  int
  writef(const char *fname, float x);

  static bool
  param_valid(param id);
};
