#include "error_handler.hpp"
#include <zephyr/drivers/hwinfo.h>
#include <zephyr/sys/reboot.h>

namespace sc = satnogs::comms;

/**
 * @brief Logs error details and throws an exception with ETL_ASSERT().
 *
 * @param e The exception object containing error info.
 */
void
error_handler::assert_error(const sc::exception &e)
{
  assert_error(0, e);
}

/**
 * @brief Logs error details and throws an exception with ETL_ASSERT() based
 * on the condition.
 *
 * Triggers if the condition is false.
 *
 * @param condition The condition to evaluate.
 * @param e The exception object containing error info.
 */
void
error_handler::assert_error(bool condition, const sc::exception &e)
{
  ETL_ASSERT(condition, e);
}

/**
 * @brief Retrieves the hardware reset cause from the MCU.
 *
 * @return uint32_t The internal hardware reset cause value.
 */
uint32_t
error_handler::hwinfo_reset_cause() const
{
  return m_hw_reset_cause;
}

/**
 * @brief A private method that obtains and clears the reset cause flags of the
 * MCU
 *
 * This method is executed once during the @ref error_handler class
 * construction. It should be used during board initialization before proceeding
 * with other operations. The method reads the MCU's reset cause register and
 * clears the associated flags. The retrieved reset cause is stored in a
 * private variable and can be accessed later using @ref
 * error_handler::hwinfo_reset_cause().
 *
 * @warning Clearing MCU's reset cause register flags is essential as the MCU
 * does not perform this step automatically, which would lead to inaccurate
 * readings on subsequent boots.
 */
void
error_handler::get_reset_cause()
{
  uint32_t cause;
  hwinfo_get_reset_cause(&cause);
  hwinfo_clear_reset_cause();
  m_hw_reset_cause = cause;
  return;
}

/**
 * @brief Instructs a system reboot.
 *
 * This method instructs the system to reboot. It first sleeps for a short
 * period to allow any buffered logs to be flushed, then requests a cold
 * software reboot. If the reboot fails, a watchdog is expected to take over.
 */
void
error_handler::system_reboot() const
{
  /* Wait a bit for any log buffers to be flushed */
  k_sleep(K_SECONDS(2));
  sys_reboot(SYS_REBOOT_COLD);

  /* If this routine fails, we rely on the WDT to do its job */
  volatile uint32_t x = 0;
  while (1) {
    x++;
  }
}

/**
 * @brief Handles a @ref satnogs::comms::exception based on its severity.
 *
 * This method logs the given exception and responds to it according to its
 * severity level:
 * - \ref satnogs::comms::exception::severity::CATASTROPHIC or
 * satnogs::comms::exception::severity::CRITICAL: triggers a system reboot
 * immediately.
 * - satnogs::comms::exception::severity::MAJOR: increments the error counter.
 * If the same error repeats more than a configured threshold ( @ref
 * CONFIG_MAX_MAJOR_ERRORS), the system reboots.
 * - Other severity levels: currently do nothing.
 *
 * @param e The exception to handle.
 */
void
error_handler::handle(const satnogs::comms::exception &e)
{
  log(e);
  switch (e.get_severity()) {
  case sc::exception::severity::CATASTROPHIC:
  case sc::exception::severity::CRITICAL:
    system_reboot();
    break;
  case sc::exception::severity::MAJOR:
    if (m_last_errno == e.get_errno()) {
      m_errno_cnt++;
    } else {
      m_last_errno = e.get_errno();
    }
    if (m_errno_cnt > CONFIG_MAX_MAJOR_ERRORS) {
      system_reboot();
    }
    break;
  default:
    break;
  }
}

/**
 * @brief Handles a generic exception.
 *
 * @note All exceptions that do not have as base the satnogs::comms::exception
 * are handled with sc::exception::severity::CATASTROPHIC severity level
 *
 * @param e the exception to handle
 */
void
error_handler::handle(const std::exception &e)
{
  log(e);
  system_reboot();
}

/**
 * @brief Constructs an @ref error_handler instance.
 *
 * @warning This constructor is exclusively invoked by @ref
 * error_handler::get_instance(), as @ref error_handler follows the singleton
 * design pattern. To obtain an instance of the @ref error_handler class, one
 * must use @ref error_handler::get_instance().
 *
 * The constructor invokes the private method @ref get_reset_cause() to read and
 * store the hardware reset cause from the MCU. This ensures the reset cause is
 * captured and cleared upon system initialization.
 */
error_handler::error_handler()
    : m_hw_reset_cause(0), m_last_errno(0), m_errno_cnt(0)
{
  get_reset_cause();
}
