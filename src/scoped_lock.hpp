/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2022-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <zephyr/kernel.h>

/**
 * @brief Implements a scoped lock utilizing the Zephyr mutex
 *
 */
class scoped_lock
{
public:
  /**
   * @brief Creates a scoped lock
   *
   * @param mutex a valid and initialized mutex
   */
  scoped_lock(struct k_mutex *mutex) : m_mtx(mutex)
  {
    k_mutex_lock(m_mtx, K_FOREVER);
  }

  /**
   * @brief Upon destruction, the mutex is released
   *
   */
  ~scoped_lock() { k_mutex_unlock(m_mtx); }

private:
  struct k_mutex *m_mtx;
};
