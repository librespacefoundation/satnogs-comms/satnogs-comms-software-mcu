options:
  parameters:
    author: ''
    catch_exceptions: 'True'
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: ''
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: no_gui
    hier_block_src_path: '.:'
    id: fsk_ber_leo
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: run
    sizing_mode: fixed
    thread_safe_setters: ''
    title: SatNOGS-COMMS FSK BER test with LEO channel emulation
    window_size: (1000,1000)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 8]
    rotation: 0
    state: enabled

blocks:
- name: antenna_gs
  id: variable_antenna
  parameters:
    aperture_efficiency: '0'
    beamwidth: '0'
    boom_length: '1'
    circumference: '0'
    comment: ''
    diameter: '0'
    frequency: 435e6
    gain: '0'
    lp: '0.5'
    pointing_error: '0'
    polarization: '0'
    rolloff_gain: '0'
    turn_spacing: '0'
    turns: '0'
    type: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1584, 20.0]
    rotation: 0
    state: enabled
- name: antenna_sat
  id: variable_antenna
  parameters:
    aperture_efficiency: '0'
    beamwidth: '0'
    boom_length: '1'
    circumference: '0'
    comment: ''
    diameter: '0'
    frequency: 435e6
    gain: '0'
    lp: '0.5'
    pointing_error: '0'
    polarization: '2'
    rolloff_gain: '0'
    turn_spacing: '0'
    turns: '0'
    type: '5'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1728, 28.0]
    rotation: 0
    state: enabled
- name: gaussian_taps
  id: variable
  parameters:
    comment: ''
    value: filter.firdes.gaussian(1.0, sps_tx, 1.0, 4*sps_tx)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [200, 372.0]
    rotation: 0
    state: enabled
- name: interp_taps
  id: variable
  parameters:
    comment: ''
    value: numpy.convolve(numpy.array(gaussian_taps), numpy.array(sq_wave))
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [200, 500.0]
    rotation: 0
    state: enabled
- name: modulation_index
  id: variable
  parameters:
    comment: ''
    value: deviation / (baudrate / 2.0)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [200, 308.0]
    rotation: 0
    state: enabled
- name: sat
  id: variable_satellite
  parameters:
    comm_freq_rx: 435e6
    comm_freq_tx: 435e6
    comment: ''
    noise_figure: '1'
    noise_temp: '2000'
    rx_antenna: antenna_sat
    rx_bw: 20e3
    tle_1: 1 25544U 98067A   24337.11671824  .00018999  00000+0  33833-3 0  9997
    tle_2: 2 25544  51.6395 203.4167 0006964 291.2449 168.6768 15.50127726484598
    tle_title: ISS
    tx_antenna: antenna_sat
    tx_power_dbm: '30'
    value: '''ok'''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1360, 20.0]
    rotation: 0
    state: enabled
- name: sq_wave
  id: variable
  parameters:
    comment: ''
    value: (1.0, ) * sps_tx
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [200, 436.0]
    rotation: 0
    state: enabled
- name: variable_ieee802_15_4_encoder_0
  id: variable_ieee802_15_4_encoder
  parameters:
    comment: ''
    crc_type: CRC32_C
    custom_crc: satnogs.crc.crc16(0x1021, 0xFFFF, 0xFFFF, True, True)
    preamble: '0b01010101'
    preamble_len: '16'
    rs: 'False'
    sync_word: '[0x1A, 0xCF, 0xFC, 0x1D]'
    var_len: 'False'
    whitening: satnogs.whitening.make_none()
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 308.0]
    rotation: 0
    state: enabled
- name: variable_ieee802_15_4_variant_decoder_0
  id: variable_ieee802_15_4_variant_decoder
  parameters:
    comment: ''
    crc_type: CRC32_C
    custom_crc: satnogs.crc.crc16(0x1021, 0xFFFF, 0xFFFF, True, True)
    drop_invalid: 'True'
    frame_len: 448-4
    preamble: '[0b01010101]*12'
    preamble_thrsh: '0'
    rs: 'False'
    sync_thrsh: '4'
    sync_word: '[0x1A, 0xCF, 0xFC, 0x1D]'
    var_len: 'False'
    whitening: satnogs.whitening.make_ccsds(True)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [0, 668.0]
    rotation: 0
    state: enabled
- name: variable_leo_model_def_0
  id: variable_leo_model_def
  parameters:
    atmo_gases_attenuation: '0'
    comment: ''
    doppler_shift_enum: '7'
    enable_link_margin: 'False'
    fspl_attenuation_enum: '0'
    mode: '0'
    pointing_attenuation_enum: '0'
    precipitation_attenuation: '0'
    rainfall_rate: '25'
    surface_watervap_density: '7.5'
    temperature: '0'
    tracker: variable_tracker_0
    value: '''fine'''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1168, 20.0]
    rotation: 0
    state: enabled
- name: variable_tracker_0
  id: variable_tracker
  parameters:
    comm_freq_rx: 435e6
    comm_freq_tx: 435e6
    comment: ''
    gs_alt: '0.01'
    gs_lat: '35.501'
    gs_lon: '24.011'
    noise_figure: '1'
    noise_temp: '2300'
    obs_end: '2024-12-02T10:36:06Z'
    obs_start: '2024-12-02T10:16:26Z'
    rx_antenna: antenna_gs
    rx_bw: 50e3
    satellite_info: sat
    time_resolution_us: '100'
    tx_antenna: antenna_gs
    tx_power_dbm: '30'
    value: '''ok'''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [848, 20.0]
    rotation: 0
    state: enabled
- name: analog_frequency_modulator_fc_0
  id: analog_frequency_modulator_fc
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    sensitivity: (math.pi*modulation_index) / sps_tx - (((math.pi*modulation_index)
      / sps_tx) * 0.1)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [2312, 556.0]
    rotation: 0
    state: enabled
- name: baudrate
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 50e3
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [288, 20.0]
    rotation: 0
    state: enabled
- name: blocks_and_const_xx_0
  id: blocks_and_const_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    const: '0'
    maxoutbuf: '0'
    minoutbuf: '0'
    type: byte
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [376, 660.0]
    rotation: 0
    state: enabled
- name: blocks_file_sink_0
  id: blocks_file_sink
  parameters:
    affinity: ''
    alias: ''
    append: 'False'
    comment: ''
    file: /spare2/leo_fsk_50000.dat
    type: complex
    unbuffered: 'False'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1336, 1012.0]
    rotation: 0
    state: disabled
- name: blocks_message_debug_0_0
  id: blocks_message_debug
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    en_uvec: 'True'
    log_level: info
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1344, 384.0]
    rotation: 0
    state: enabled
- name: blocks_message_strobe_0
  id: blocks_message_strobe
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    msg: pmt.cons(pmt.PMT_NIL,pmt.intern("TEST"))
    period: delay_ms
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [856, 412.0]
    rotation: 180
    state: enabled
- name: blocks_packed_to_unpacked_xx_0
  id: blocks_packed_to_unpacked_xx
  parameters:
    affinity: ''
    alias: ''
    bits_per_chunk: '1'
    comment: ''
    endianness: gr.GR_MSB_FIRST
    maxoutbuf: '0'
    minoutbuf: '0'
    num_ports: '1'
    type: byte
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1656, 532.0]
    rotation: 0
    state: enabled
- name: blocks_tagged_stream_align_0
  id: blocks_tagged_stream_align
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    lengthtagname: packet_len
    maxoutbuf: '0'
    minoutbuf: '0'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [824, 804.0]
    rotation: 180
    state: enabled
- name: blocks_tagged_stream_multiply_length_0_0
  id: blocks_tagged_stream_multiply_length
  parameters:
    affinity: ''
    alias: ''
    c: (samp_rate/(baudrate*sps_tx))
    comment: ''
    lengthtagname: packet_len
    maxoutbuf: '0'
    minoutbuf: '0'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1040, 792.0]
    rotation: 180
    state: enabled
- name: blocks_tagged_stream_multiply_length_0_0_0
  id: blocks_tagged_stream_multiply_length
  parameters:
    affinity: ''
    alias: ''
    c: '(sps_tx * 8) '
    comment: Length tag should be fixed for the burst tagger to work properly
    lengthtagname: packet_len
    maxoutbuf: '0'
    minoutbuf: '0'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [2064, 792.0]
    rotation: 180
    state: enabled
- name: delay_ms
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '100'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [440, 20.0]
    rotation: 0
    state: enabled
- name: deviation
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Frequency deviation from the center frequency
    short_id: ''
    type: eng_float
    value: 25e3
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [688, 116.0]
    rotation: 0
    state: enabled
- name: digital_burst_shaper_xx_0
  id: digital_burst_shaper_xx
  parameters:
    affinity: ''
    alias: ''
    comment: 'gr-leo uses relative time based on the number of samples it consumes.

      This is an issue for busrt transmission like this one. To deal with this issue,

      we properly use the burst shaper, to append a number of zero samples

      at the end of each frame, thta matches the inter-arrival of the burst.

      With this way, we produce an almost continues stream so gr-leo can operate

      as ittended.'
    insert_phasing: 'False'
    length_tag_name: '"packet_len"'
    maxoutbuf: '0'
    minoutbuf: '0'
    post_padding: int( (baudrate * sps_tx) * (delay_ms * 0.001)) - 200 - (frame_size
      + 32 + 4 + 4) * 8 * sps_tx - 1000
    pre_padding: '200'
    type: complex
    window: ([])
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1864, 772.0]
    rotation: 180
    state: enabled
- name: digital_chunks_to_symbols_xx_0
  id: digital_chunks_to_symbols_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    dimension: '1'
    in_type: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    num_ports: '1'
    out_type: float
    symbol_table: '[-1, 1]'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1936, 544.0]
    rotation: 0
    state: enabled
- name: frame_size
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: The payload frame size
    short_id: ''
    type: intx
    value: '252'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [688, 20.0]
    rotation: 0
    state: enabled
- name: import_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import math
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [360, 20.0]
    rotation: 0
    state: enabled
- name: import_0_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import numpy
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [200, 564.0]
    rotation: 0
    state: true
- name: interp_fir_filter_xxx_0
  id: interp_fir_filter_xxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    interp: sps_tx
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_delay: '0'
    taps: interp_taps
    type: fff
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [2128, 548.0]
    rotation: 0
    state: enabled
- name: leo_channel_model_0
  id: leo_channel_model
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    filename: test.csv
    maxoutbuf: '0'
    minoutbuf: '0'
    model: variable_leo_model_def_0
    noise_type: '0'
    sample_rate: baudrate * sps_tx
    store_csv: '0'
    value: '''fine'''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1624, 772.0]
    rotation: 180
    state: enabled
- name: nframes
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: The number of frames to send
    short_id: ''
    type: intx
    value: '1000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [520, 20.0]
    rotation: 0
    state: enabled
- name: pdu_pdu_to_tagged_stream_0
  id: pdu_pdu_to_tagged_stream
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    tag: packet_len
    type: byte
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1440, 540.0]
    rotation: 0
    state: enabled
- name: pfb_arb_resampler_xxx_0_0_0
  id: pfb_arb_resampler_xxx
  parameters:
    affinity: ''
    alias: ''
    atten: '100'
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    nfilts: '32'
    rrate: samp_rate/(baudrate*sps_tx)
    samp_delay: '0'
    taps: ''
    type: ccf
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1376, 764.0]
    rotation: 180
    state: enabled
- name: qtgui_freq_sink_x_0_0
  id: qtgui_freq_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    average: '1.0'
    axislabels: 'True'
    bw: samp_rate
    color1: '"blue"'
    color10: '"dark blue"'
    color2: '"red"'
    color3: '"green"'
    color4: '"black"'
    color5: '"cyan"'
    color6: '"magenta"'
    color7: '"yellow"'
    color8: '"dark red"'
    color9: '"dark green"'
    comment: ''
    ctrlpanel: 'True'
    fc: tx_freq
    fftsize: '8192'
    freqhalf: 'True'
    grid: 'True'
    gui_hint: ''
    label: Relative Gain
    label1: ''
    label10: ''''''
    label2: ''''''
    label3: ''''''
    label4: ''''''
    label5: ''''''
    label6: ''''''
    label7: ''''''
    label8: ''''''
    label9: ''''''
    legend: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    name: '"TX"'
    nconnections: '1'
    norm_window: 'False'
    showports: 'False'
    tr_chan: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_tag: '""'
    type: complex
    units: dB
    update_time: '0.01'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    wintype: window.WIN_BLACKMAN_hARRIS
    ymax: '0'
    ymin: '-120'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [824, 988.0]
    rotation: 0
    state: disabled
- name: qtgui_time_sink_x_1
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: blue
    color10: dark blue
    color2: red
    color3: green
    color4: black
    color5: cyan
    color6: magenta
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'False'
    entags: 'True'
    grid: 'False'
    gui_hint: ''
    label1: Signal 1
    label10: Signal 10
    label2: Signal 2
    label3: Signal 3
    label4: Signal 4
    label5: Signal 5
    label6: Signal 6
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '-1'
    marker10: '-1'
    marker2: '-1'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '""'
    nconnections: '1'
    size: '1024'
    srate: samp_rate
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: complex
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Amplitude
    ymax: '1'
    ymin: '-1'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [576, 884.0]
    rotation: 180
    state: disabled
- name: samp_rate
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 2e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [200, 20.0]
    rotation: 0
    state: enabled
- name: satnogs_ber_calculator_0
  id: satnogs_ber_calculator
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    frame_size: frame_size
    maxoutbuf: '0'
    minoutbuf: '0'
    nframes: nframes
    skip: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [856, 524.0]
    rotation: 0
    state: enabled
- name: satnogs_crc_async_0
  id: satnogs_crc_async
  parameters:
    affinity: ''
    alias: ''
    check: 'False'
    comment: "This block adds some extra bytes at the \nend of the frame that have\
      \ no actual\ncontribution to the original frame. They act\nas a trailing burst\
      \ shaping. We have identified that\nthere is a problem at the last byte received\
      \ by the\nSatNOGS-COMMS and it is related to this TX chain.\nBurst shaper seems\
      \ that does not solve the issue\nso probably the problem is earlier at the TX\
      \ chain."
    custom_crc: satnogs.crc.crc16(0x1021, 0xFFFF, 0xFFFF, True, True)
    maxoutbuf: '0'
    minoutbuf: '0'
    nbo: 'False'
    type: CRC32_C
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1272, 524.0]
    rotation: 0
    state: enabled
- name: satnogs_frame_decoder_0_0
  id: satnogs_frame_decoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decoder_object: variable_ieee802_15_4_variant_decoder_0
    itype: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [544, 528.0]
    rotation: 0
    state: enabled
- name: satnogs_frame_encoder_0
  id: satnogs_frame_encoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    encoder_object: variable_ieee802_15_4_encoder_0
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1080, 540.0]
    rotation: 0
    state: enabled
- name: sps_tx
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '16'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 564.0]
    rotation: 0
    state: enabled
- name: tx_freq
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 435e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 196.0]
    rotation: 0
    state: enabled
- name: tx_gain
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: '60.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [96, 196.0]
    rotation: 0
    state: enabled
- name: uhd_usrp_sink_0
  id: uhd_usrp_sink
  parameters:
    affinity: ''
    alias: ''
    ant0: '"TX/RX"'
    ant1: '"TX/RX"'
    ant10: '"TX/RX"'
    ant11: '"TX/RX"'
    ant12: '"TX/RX"'
    ant13: '"TX/RX"'
    ant14: '"TX/RX"'
    ant15: '"TX/RX"'
    ant16: '"TX/RX"'
    ant17: '"TX/RX"'
    ant18: '"TX/RX"'
    ant19: '"TX/RX"'
    ant2: '"TX/RX"'
    ant20: '"TX/RX"'
    ant21: '"TX/RX"'
    ant22: '"TX/RX"'
    ant23: '"TX/RX"'
    ant24: '"TX/RX"'
    ant25: '"TX/RX"'
    ant26: '"TX/RX"'
    ant27: '"TX/RX"'
    ant28: '"TX/RX"'
    ant29: '"TX/RX"'
    ant3: '"TX/RX"'
    ant30: '"TX/RX"'
    ant31: '"TX/RX"'
    ant4: '"TX/RX"'
    ant5: '"TX/RX"'
    ant6: '"TX/RX"'
    ant7: '"TX/RX"'
    ant8: '"TX/RX"'
    ant9: '"TX/RX"'
    bw0: '0'
    bw1: '0'
    bw10: '0'
    bw11: '0'
    bw12: '0'
    bw13: '0'
    bw14: '0'
    bw15: '0'
    bw16: '0'
    bw17: '0'
    bw18: '0'
    bw19: '0'
    bw2: '0'
    bw20: '0'
    bw21: '0'
    bw22: '0'
    bw23: '0'
    bw24: '0'
    bw25: '0'
    bw26: '0'
    bw27: '0'
    bw28: '0'
    bw29: '0'
    bw3: '0'
    bw30: '0'
    bw31: '0'
    bw4: '0'
    bw5: '0'
    bw6: '0'
    bw7: '0'
    bw8: '0'
    bw9: '0'
    center_freq0: tx_freq
    center_freq1: '0'
    center_freq10: '0'
    center_freq11: '0'
    center_freq12: '0'
    center_freq13: '0'
    center_freq14: '0'
    center_freq15: '0'
    center_freq16: '0'
    center_freq17: '0'
    center_freq18: '0'
    center_freq19: '0'
    center_freq2: '0'
    center_freq20: '0'
    center_freq21: '0'
    center_freq22: '0'
    center_freq23: '0'
    center_freq24: '0'
    center_freq25: '0'
    center_freq26: '0'
    center_freq27: '0'
    center_freq28: '0'
    center_freq29: '0'
    center_freq3: '0'
    center_freq30: '0'
    center_freq31: '0'
    center_freq4: '0'
    center_freq5: '0'
    center_freq6: '0'
    center_freq7: '0'
    center_freq8: '0'
    center_freq9: '0'
    clock_rate: 0e0
    clock_source0: ''
    clock_source1: ''
    clock_source2: ''
    clock_source3: ''
    clock_source4: ''
    clock_source5: ''
    clock_source6: ''
    clock_source7: ''
    comment: ''
    dev_addr: '""'
    dev_args: ''
    gain0: tx_gain
    gain1: '0'
    gain10: '0'
    gain11: '0'
    gain12: '0'
    gain13: '0'
    gain14: '0'
    gain15: '0'
    gain16: '0'
    gain17: '0'
    gain18: '0'
    gain19: '0'
    gain2: '0'
    gain20: '0'
    gain21: '0'
    gain22: '0'
    gain23: '0'
    gain24: '0'
    gain25: '0'
    gain26: '0'
    gain27: '0'
    gain28: '0'
    gain29: '0'
    gain3: '0'
    gain30: '0'
    gain31: '0'
    gain4: '0'
    gain5: '0'
    gain6: '0'
    gain7: '0'
    gain8: '0'
    gain9: '0'
    gain_type0: default
    gain_type1: default
    gain_type10: default
    gain_type11: default
    gain_type12: default
    gain_type13: default
    gain_type14: default
    gain_type15: default
    gain_type16: default
    gain_type17: default
    gain_type18: default
    gain_type19: default
    gain_type2: default
    gain_type20: default
    gain_type21: default
    gain_type22: default
    gain_type23: default
    gain_type24: default
    gain_type25: default
    gain_type26: default
    gain_type27: default
    gain_type28: default
    gain_type29: default
    gain_type3: default
    gain_type30: default
    gain_type31: default
    gain_type4: default
    gain_type5: default
    gain_type6: default
    gain_type7: default
    gain_type8: default
    gain_type9: default
    len_tag_name: '""'
    lo_export0: 'False'
    lo_export1: 'False'
    lo_export10: 'False'
    lo_export11: 'False'
    lo_export12: 'False'
    lo_export13: 'False'
    lo_export14: 'False'
    lo_export15: 'False'
    lo_export16: 'False'
    lo_export17: 'False'
    lo_export18: 'False'
    lo_export19: 'False'
    lo_export2: 'False'
    lo_export20: 'False'
    lo_export21: 'False'
    lo_export22: 'False'
    lo_export23: 'False'
    lo_export24: 'False'
    lo_export25: 'False'
    lo_export26: 'False'
    lo_export27: 'False'
    lo_export28: 'False'
    lo_export29: 'False'
    lo_export3: 'False'
    lo_export30: 'False'
    lo_export31: 'False'
    lo_export4: 'False'
    lo_export5: 'False'
    lo_export6: 'False'
    lo_export7: 'False'
    lo_export8: 'False'
    lo_export9: 'False'
    lo_source0: internal
    lo_source1: internal
    lo_source10: internal
    lo_source11: internal
    lo_source12: internal
    lo_source13: internal
    lo_source14: internal
    lo_source15: internal
    lo_source16: internal
    lo_source17: internal
    lo_source18: internal
    lo_source19: internal
    lo_source2: internal
    lo_source20: internal
    lo_source21: internal
    lo_source22: internal
    lo_source23: internal
    lo_source24: internal
    lo_source25: internal
    lo_source26: internal
    lo_source27: internal
    lo_source28: internal
    lo_source29: internal
    lo_source3: internal
    lo_source30: internal
    lo_source31: internal
    lo_source4: internal
    lo_source5: internal
    lo_source6: internal
    lo_source7: internal
    lo_source8: internal
    lo_source9: internal
    maxoutbuf: '0'
    minoutbuf: '0'
    nchan: '1'
    num_mboards: '1'
    otw: ''
    samp_rate: samp_rate
    sd_spec0: ''
    sd_spec1: ''
    sd_spec2: ''
    sd_spec3: ''
    sd_spec4: ''
    sd_spec5: ''
    sd_spec6: ''
    sd_spec7: ''
    show_lo_controls: 'False'
    start_time: '-1.0'
    stream_args: ''
    stream_chans: '[]'
    sync: sync
    time_source0: ''
    time_source1: ''
    time_source2: ''
    time_source3: ''
    time_source4: ''
    time_source5: ''
    time_source6: ''
    time_source7: ''
    type: fc32
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [560, 756.0]
    rotation: 180
    state: enabled
- name: uhd_usrp_source_0
  id: uhd_usrp_source
  parameters:
    affinity: ''
    alias: ''
    ant0: '"RX2"'
    ant1: '"RX2"'
    ant10: '"RX2"'
    ant11: '"RX2"'
    ant12: '"RX2"'
    ant13: '"RX2"'
    ant14: '"RX2"'
    ant15: '"RX2"'
    ant16: '"RX2"'
    ant17: '"RX2"'
    ant18: '"RX2"'
    ant19: '"RX2"'
    ant2: '"RX2"'
    ant20: '"RX2"'
    ant21: '"RX2"'
    ant22: '"RX2"'
    ant23: '"RX2"'
    ant24: '"RX2"'
    ant25: '"RX2"'
    ant26: '"RX2"'
    ant27: '"RX2"'
    ant28: '"RX2"'
    ant29: '"RX2"'
    ant3: '"RX2"'
    ant30: '"RX2"'
    ant31: '"RX2"'
    ant4: '"RX2"'
    ant5: '"RX2"'
    ant6: '"RX2"'
    ant7: '"RX2"'
    ant8: '"RX2"'
    ant9: '"RX2"'
    bw0: '0'
    bw1: '0'
    bw10: '0'
    bw11: '0'
    bw12: '0'
    bw13: '0'
    bw14: '0'
    bw15: '0'
    bw16: '0'
    bw17: '0'
    bw18: '0'
    bw19: '0'
    bw2: '0'
    bw20: '0'
    bw21: '0'
    bw22: '0'
    bw23: '0'
    bw24: '0'
    bw25: '0'
    bw26: '0'
    bw27: '0'
    bw28: '0'
    bw29: '0'
    bw3: '0'
    bw30: '0'
    bw31: '0'
    bw4: '0'
    bw5: '0'
    bw6: '0'
    bw7: '0'
    bw8: '0'
    bw9: '0'
    center_freq0: tx_freq
    center_freq1: '0'
    center_freq10: '0'
    center_freq11: '0'
    center_freq12: '0'
    center_freq13: '0'
    center_freq14: '0'
    center_freq15: '0'
    center_freq16: '0'
    center_freq17: '0'
    center_freq18: '0'
    center_freq19: '0'
    center_freq2: '0'
    center_freq20: '0'
    center_freq21: '0'
    center_freq22: '0'
    center_freq23: '0'
    center_freq24: '0'
    center_freq25: '0'
    center_freq26: '0'
    center_freq27: '0'
    center_freq28: '0'
    center_freq29: '0'
    center_freq3: '0'
    center_freq30: '0'
    center_freq31: '0'
    center_freq4: '0'
    center_freq5: '0'
    center_freq6: '0'
    center_freq7: '0'
    center_freq8: '0'
    center_freq9: '0'
    clock_rate: 0e0
    clock_source0: ''
    clock_source1: ''
    clock_source2: ''
    clock_source3: ''
    clock_source4: ''
    clock_source5: ''
    clock_source6: ''
    clock_source7: ''
    comment: ''
    dc_offs0: 0+0j
    dc_offs1: 0+0j
    dc_offs10: 0+0j
    dc_offs11: 0+0j
    dc_offs12: 0+0j
    dc_offs13: 0+0j
    dc_offs14: 0+0j
    dc_offs15: 0+0j
    dc_offs16: 0+0j
    dc_offs17: 0+0j
    dc_offs18: 0+0j
    dc_offs19: 0+0j
    dc_offs2: 0+0j
    dc_offs20: 0+0j
    dc_offs21: 0+0j
    dc_offs22: 0+0j
    dc_offs23: 0+0j
    dc_offs24: 0+0j
    dc_offs25: 0+0j
    dc_offs26: 0+0j
    dc_offs27: 0+0j
    dc_offs28: 0+0j
    dc_offs29: 0+0j
    dc_offs3: 0+0j
    dc_offs30: 0+0j
    dc_offs31: 0+0j
    dc_offs4: 0+0j
    dc_offs5: 0+0j
    dc_offs6: 0+0j
    dc_offs7: 0+0j
    dc_offs8: 0+0j
    dc_offs9: 0+0j
    dc_offs_enb0: default
    dc_offs_enb1: default
    dc_offs_enb10: default
    dc_offs_enb11: default
    dc_offs_enb12: default
    dc_offs_enb13: default
    dc_offs_enb14: default
    dc_offs_enb15: default
    dc_offs_enb16: default
    dc_offs_enb17: default
    dc_offs_enb18: default
    dc_offs_enb19: default
    dc_offs_enb2: default
    dc_offs_enb20: default
    dc_offs_enb21: default
    dc_offs_enb22: default
    dc_offs_enb23: default
    dc_offs_enb24: default
    dc_offs_enb25: default
    dc_offs_enb26: default
    dc_offs_enb27: default
    dc_offs_enb28: default
    dc_offs_enb29: default
    dc_offs_enb3: default
    dc_offs_enb30: default
    dc_offs_enb31: default
    dc_offs_enb4: default
    dc_offs_enb5: default
    dc_offs_enb6: default
    dc_offs_enb7: default
    dc_offs_enb8: default
    dc_offs_enb9: default
    dev_addr: '""'
    dev_args: ''
    gain0: '32'
    gain1: '0'
    gain10: '0'
    gain11: '0'
    gain12: '0'
    gain13: '0'
    gain14: '0'
    gain15: '0'
    gain16: '0'
    gain17: '0'
    gain18: '0'
    gain19: '0'
    gain2: '0'
    gain20: '0'
    gain21: '0'
    gain22: '0'
    gain23: '0'
    gain24: '0'
    gain25: '0'
    gain26: '0'
    gain27: '0'
    gain28: '0'
    gain29: '0'
    gain3: '0'
    gain30: '0'
    gain31: '0'
    gain4: '0'
    gain5: '0'
    gain6: '0'
    gain7: '0'
    gain8: '0'
    gain9: '0'
    gain_type0: default
    gain_type1: default
    gain_type10: default
    gain_type11: default
    gain_type12: default
    gain_type13: default
    gain_type14: default
    gain_type15: default
    gain_type16: default
    gain_type17: default
    gain_type18: default
    gain_type19: default
    gain_type2: default
    gain_type20: default
    gain_type21: default
    gain_type22: default
    gain_type23: default
    gain_type24: default
    gain_type25: default
    gain_type26: default
    gain_type27: default
    gain_type28: default
    gain_type29: default
    gain_type3: default
    gain_type30: default
    gain_type31: default
    gain_type4: default
    gain_type5: default
    gain_type6: default
    gain_type7: default
    gain_type8: default
    gain_type9: default
    iq_imbal0: 0+0j
    iq_imbal1: 0+0j
    iq_imbal10: 0+0j
    iq_imbal11: 0+0j
    iq_imbal12: 0+0j
    iq_imbal13: 0+0j
    iq_imbal14: 0+0j
    iq_imbal15: 0+0j
    iq_imbal16: 0+0j
    iq_imbal17: 0+0j
    iq_imbal18: 0+0j
    iq_imbal19: 0+0j
    iq_imbal2: 0+0j
    iq_imbal20: 0+0j
    iq_imbal21: 0+0j
    iq_imbal22: 0+0j
    iq_imbal23: 0+0j
    iq_imbal24: 0+0j
    iq_imbal25: 0+0j
    iq_imbal26: 0+0j
    iq_imbal27: 0+0j
    iq_imbal28: 0+0j
    iq_imbal29: 0+0j
    iq_imbal3: 0+0j
    iq_imbal30: 0+0j
    iq_imbal31: 0+0j
    iq_imbal4: 0+0j
    iq_imbal5: 0+0j
    iq_imbal6: 0+0j
    iq_imbal7: 0+0j
    iq_imbal8: 0+0j
    iq_imbal9: 0+0j
    iq_imbal_enb0: default
    iq_imbal_enb1: default
    iq_imbal_enb10: default
    iq_imbal_enb11: default
    iq_imbal_enb12: default
    iq_imbal_enb13: default
    iq_imbal_enb14: default
    iq_imbal_enb15: default
    iq_imbal_enb16: default
    iq_imbal_enb17: default
    iq_imbal_enb18: default
    iq_imbal_enb19: default
    iq_imbal_enb2: default
    iq_imbal_enb20: default
    iq_imbal_enb21: default
    iq_imbal_enb22: default
    iq_imbal_enb23: default
    iq_imbal_enb24: default
    iq_imbal_enb25: default
    iq_imbal_enb26: default
    iq_imbal_enb27: default
    iq_imbal_enb28: default
    iq_imbal_enb29: default
    iq_imbal_enb3: default
    iq_imbal_enb30: default
    iq_imbal_enb31: default
    iq_imbal_enb4: default
    iq_imbal_enb5: default
    iq_imbal_enb6: default
    iq_imbal_enb7: default
    iq_imbal_enb8: default
    iq_imbal_enb9: default
    lo_export0: 'False'
    lo_export1: 'False'
    lo_export10: 'False'
    lo_export11: 'False'
    lo_export12: 'False'
    lo_export13: 'False'
    lo_export14: 'False'
    lo_export15: 'False'
    lo_export16: 'False'
    lo_export17: 'False'
    lo_export18: 'False'
    lo_export19: 'False'
    lo_export2: 'False'
    lo_export20: 'False'
    lo_export21: 'False'
    lo_export22: 'False'
    lo_export23: 'False'
    lo_export24: 'False'
    lo_export25: 'False'
    lo_export26: 'False'
    lo_export27: 'False'
    lo_export28: 'False'
    lo_export29: 'False'
    lo_export3: 'False'
    lo_export30: 'False'
    lo_export31: 'False'
    lo_export4: 'False'
    lo_export5: 'False'
    lo_export6: 'False'
    lo_export7: 'False'
    lo_export8: 'False'
    lo_export9: 'False'
    lo_source0: internal
    lo_source1: internal
    lo_source10: internal
    lo_source11: internal
    lo_source12: internal
    lo_source13: internal
    lo_source14: internal
    lo_source15: internal
    lo_source16: internal
    lo_source17: internal
    lo_source18: internal
    lo_source19: internal
    lo_source2: internal
    lo_source20: internal
    lo_source21: internal
    lo_source22: internal
    lo_source23: internal
    lo_source24: internal
    lo_source25: internal
    lo_source26: internal
    lo_source27: internal
    lo_source28: internal
    lo_source29: internal
    lo_source3: internal
    lo_source30: internal
    lo_source31: internal
    lo_source4: internal
    lo_source5: internal
    lo_source6: internal
    lo_source7: internal
    lo_source8: internal
    lo_source9: internal
    maxoutbuf: '0'
    minoutbuf: '0'
    nchan: '1'
    num_mboards: '1'
    otw: ''
    rx_agc0: Default
    rx_agc1: Default
    rx_agc10: Default
    rx_agc11: Default
    rx_agc12: Default
    rx_agc13: Default
    rx_agc14: Default
    rx_agc15: Default
    rx_agc16: Default
    rx_agc17: Default
    rx_agc18: Default
    rx_agc19: Default
    rx_agc2: Default
    rx_agc20: Default
    rx_agc21: Default
    rx_agc22: Default
    rx_agc23: Default
    rx_agc24: Default
    rx_agc25: Default
    rx_agc26: Default
    rx_agc27: Default
    rx_agc28: Default
    rx_agc29: Default
    rx_agc3: Default
    rx_agc30: Default
    rx_agc31: Default
    rx_agc4: Default
    rx_agc5: Default
    rx_agc6: Default
    rx_agc7: Default
    rx_agc8: Default
    rx_agc9: Default
    samp_rate: samp_rate
    sd_spec0: ''
    sd_spec1: ''
    sd_spec2: ''
    sd_spec3: ''
    sd_spec4: ''
    sd_spec5: ''
    sd_spec6: ''
    sd_spec7: ''
    show_lo_controls: 'False'
    start_time: '-1.0'
    stream_args: ''
    stream_chans: '[]'
    sync: sync
    time_source0: ''
    time_source1: ''
    time_source2: ''
    time_source3: ''
    time_source4: ''
    time_source5: ''
    time_source6: ''
    time_source7: ''
    type: fc32
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [432, 1036.0]
    rotation: 0
    state: disabled
- name: virtual_sink_0
  id: virtual_sink
  parameters:
    alias: ''
    comment: ''
    stream_id: dummy
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1856, 468.0]
    rotation: 0
    state: enabled
- name: virtual_source_0
  id: virtual_source
  parameters:
    alias: ''
    comment: ''
    stream_id: dummy
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [216, 660.0]
    rotation: 0
    state: enabled

connections:
- [analog_frequency_modulator_fc_0, '0', blocks_tagged_stream_multiply_length_0_0_0,
  '0']
- [blocks_and_const_xx_0, '0', satnogs_frame_decoder_0_0, '0']
- [blocks_message_strobe_0, strobe, satnogs_ber_calculator_0, trigger]
- [blocks_packed_to_unpacked_xx_0, '0', digital_chunks_to_symbols_xx_0, '0']
- [blocks_packed_to_unpacked_xx_0, '0', virtual_sink_0, '0']
- [blocks_tagged_stream_align_0, '0', qtgui_time_sink_x_1, '0']
- [blocks_tagged_stream_align_0, '0', uhd_usrp_sink_0, '0']
- [blocks_tagged_stream_multiply_length_0_0, '0', blocks_tagged_stream_align_0, '0']
- [blocks_tagged_stream_multiply_length_0_0_0, '0', digital_burst_shaper_xx_0, '0']
- [digital_burst_shaper_xx_0, '0', leo_channel_model_0, '0']
- [digital_chunks_to_symbols_xx_0, '0', interp_fir_filter_xxx_0, '0']
- [interp_fir_filter_xxx_0, '0', analog_frequency_modulator_fc_0, '0']
- [leo_channel_model_0, '0', pfb_arb_resampler_xxx_0_0_0, '0']
- [pdu_pdu_to_tagged_stream_0, '0', blocks_packed_to_unpacked_xx_0, '0']
- [pfb_arb_resampler_xxx_0_0_0, '0', blocks_file_sink_0, '0']
- [pfb_arb_resampler_xxx_0_0_0, '0', blocks_tagged_stream_multiply_length_0_0, '0']
- [satnogs_ber_calculator_0, pdu, satnogs_frame_encoder_0, pdu]
- [satnogs_crc_async_0, out, pdu_pdu_to_tagged_stream_0, pdus]
- [satnogs_frame_decoder_0_0, out, satnogs_ber_calculator_0, received]
- [satnogs_frame_encoder_0, pdu, blocks_message_debug_0_0, print]
- [satnogs_frame_encoder_0, pdu, satnogs_crc_async_0, in]
- [uhd_usrp_source_0, '0', qtgui_freq_sink_x_0_0, '0']
- [virtual_source_0, '0', blocks_and_const_xx_0, '0']

metadata:
  file_format: 1
  grc_version: 3.10.11.0
